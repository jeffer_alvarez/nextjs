import "firebase/auth";

import { Drawer, Typography } from "@material-ui/core";

import AppBar from "@material-ui/core/AppBar";
import ContenedorPrincipal from "./main/ContenedorPrincipal";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import React from "react";
import ToolbarPrimario from "./tollbars/ToolbarPrimario";
import firebase from "firebase/app";
import themes from "../utils/themeCustom";

const drawerWidth = 200;

function PersistentDrawerLeft(props) {
  const [open, setOpen] = React.useState(true);
  const [title, setTitle] = React.useState('Caja');
  const [
    vistaContenedorPrincipal,
    setVistaContenedorPrincipal
  ] = React.useState(0);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const [mobileMoreAnchorEl, setOpenMobileMoreAnchorEl] = React.useState();

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  
  const handleSesionClose = () => {
    firebase.auth().signOut()
  };


  const handleMobileMenuClose = () => {
    setOpenMobileMoreAnchorEl(null);
  };


  return (
    <div style={{ height: "100vh", width: "100%" }}>
      {/* <CssBaseline /> */}
      <AppBar position="static" style={{
        marginLeft: open ? drawerWidth : '0px'
      }}>
        <ToolbarPrimario
          title={title}
          handleDrawerOpen={handleDrawerOpen}
          handleDrawerClose={handleDrawerClose}
          open={open}
        />
      </AppBar>
      <Drawer
        variant="persistent"
        anchor="left"
        open={open}
      >
        <div
          style={{
            height: "50px",
            background: themes.base,
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Typography variant="h6" noWrap style={{ paddingLeft: 24, color: 'white' }}>
            Menu
          </Typography>
        </div>
        <List style={{
          width: drawerWidth
        }}>
          <ListItem
            button
            onClick={() => { setVistaContenedorPrincipal(0); setTitle('Caja') }}
            style={{
              margin:0,
              paddingLeft:16,
              paddingRight:16,
            }}
          >
            <span style={{
              color:  vistaContenedorPrincipal===0? themes.dark: themes.base,
              fontWeight:  vistaContenedorPrincipal===0? 700 : 400,
            }}>Caja</span>
          </ListItem>
          <ListItem
            button
            onClick={() => { setVistaContenedorPrincipal(1); setTitle('Punto de venta') }}
            style={{
              margin:0,
              paddingLeft:16,
              paddingRight:16,
            }}
          >
            <span style={{
              color:  vistaContenedorPrincipal===1? themes.dark: themes.base,
              fontWeight:  vistaContenedorPrincipal===1? 700 : 400,
            }}>Punto de venta</span>
          </ListItem>
          <ListItem
            button
            onClick={() => { setVistaContenedorPrincipal(2); setTitle('Lista de ventas') }}
            style={{
              margin:0,
              paddingLeft:16,
              paddingRight:16,
            }}
          >
            <span style={{
              color:  vistaContenedorPrincipal===2? themes.dark: themes.base,
              fontWeight:  vistaContenedorPrincipal===2? 700 : 400,
            }}>Lista de ventas</span>
          </ListItem>



        {/*   <ListItem
            button
            onClick={() => { setVistaContenedorPrincipal(3); setTitle('Lista de productos') }}
            style={{
              margin:0,
              paddingLeft:16,
              paddingRight:16,
            }}
          >
            <span style={{
              color:  vistaContenedorPrincipal===3? themes.dark: themes.base,
              fontWeight:  vistaContenedorPrincipal===3? 700 : 400,
            }}>Lista Productos</span>
          </ListItem> */}
          <Divider />
          <ListItem
            button
            onClick={() => { handleSesionClose() }}
            style={{
              margin:0,
              paddingLeft:16,
              paddingRight:16,
            }}
          >
            <span style={{
              color:   themes.base,
              fontWeight:   400,
            }}>Cerrar Sesión</span>
          </ListItem>
        </List>
      </Drawer>
      <main style={{
        marginLeft: open ? drawerWidth : '0px'
      }}>
        <ContenedorPrincipal
          vistaContenedorPrincipal={vistaContenedorPrincipal}
          firebaseClientesConection={props.firebaseClientesConection}
        />
      </main>
    </div>
  );
}

export default PersistentDrawerLeft;
