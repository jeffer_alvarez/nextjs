import { Button, CircularProgress } from "@material-ui/core";
import React, { useEffect, useState } from "react";

import AdvancedTable from "./caja/Components/TablaHistorialTransaccionesCaja";
import Caja from "./caja/Caja";
import ListaFacturas from "./punto_venta/components_lista_content/ListaFacturas";
import PuntoVenta1 from "./punto_venta/PuntoVenta1";
import { Spinner } from "evergreen-ui";
import TablaProductos from "./caja/Components/TablaProductos";
import firebase from "firebase/app";
import funtions from "../../utils/funtions";
import { teal } from "@material-ui/core/colors";

function ContenedorPrincipal(props) {

  const [usuarioSeleccionado, setUsuarioSeleccionado] = useState([])
  const [cajaSeleccionada, setCajaSeleccionada] = useState([])
  const [usuario, setUsuario] = useState("");
  const [caja, setCaja] = useState('cargando');
  const [codigo, setCodigo] = useState("");
  const [saldoInicial, setSaldoInicial] = useState(0);

  const obteberCajaSeleccionada = () => {
    var db = firebase.database();
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        var db = firebase.database();
        var operacionVentaRefCaja = db.ref(
          "users/" +
          firebase.auth().currentUser.uid +
          "/caja/cajas_abiertas_usuario"
        );
        var ususarios = db.ref(
          "users/" + firebase.auth().currentUser.uid + "/usuarios"
        );
        var usuarios = {};
        ususarios.on("value", snapUsu => {
          if (snapUsu.val()) {
            var lista = funtions.snapshotToArray(snapUsu);
            lista.map(item => {
              if (item.estado) {
                usuarios = item;
                var code = item.code
                setUsuario(code)
              }
            });
          }
        });
        operacionVentaRefCaja.on("value", snap => {
          if (snap.val()) {
            var caja = funtions.snapshotToArray(snap);
            if (caja != null) {
              var estado = caja[0].estado;
              var codigoCaja = caja[0].codigo;
              var saldo = caja[0].saldo_inicial;
              setCodigo(codigoCaja)
              setSaldoInicial(saldo)
              setCaja(estado)
              setUsuarioSeleccionado(usuarios)
              setCajaSeleccionada(caja)
            } else {
              var estado = false;
              var codigoCaja = "";
              var saldo = 0;
              setCodigo(codigoCaja)
              setSaldoInicial(saldo)
              setCaja(estado)
              setCajaSeleccionada([])
              setUsuarioSeleccionado(null)
            }

          } else {
            var estado = false;
            var codigoCaja = "";
            var saldo = 0;
            setCodigo(codigoCaja)
            setSaldoInicial(saldo)
            setCaja(false)
            setCajaSeleccionada([])
            setUsuarioSeleccionado(null)
            setTimeout(() => {
              console.log(estado);
            }, 100)
            console.log('no allo nada')
          }
        });
      }
    });
  };

  const [listaPro, setLista] = useState([])

  useEffect(() => {
    obteberCajaSeleccionada()


  }, [])


  const cargarLista = (lista) => {
    setLista(lista)
  }

  const style = {


  }

  return (
    <div>
      {
        props.vistaContenedorPrincipal == 0 &&
        <>
          {
            caja != 'cargando' ?
              <Caja caja={caja} codigo={codigo} saldo_inicial={saldoInicial} code={usuario} />
              :
              <div style={{ marginTop: 75, marginLeft: 75 }}>
                <CircularProgress size={24} style={{ color: teal[500] }} />
              </div>
          }
        </>
      }
      {
        props.vistaContenedorPrincipal == 1 &&
        <>
          {
            caja != 'cargando' ?
              <PuntoVenta1 estado={caja} firebaseClientesConection={props.firebaseClientesConection} cargarLista={(lista) => cargarLista(lista)} />
              :
              <div style={{ marginTop: 75, marginLeft: 75 }}>
                <CircularProgress size={24} style={{ color: teal[500] }} />
              </div>
          }
        </>
      }
      {props.vistaContenedorPrincipal == 2 &&
        <>
          {
            caja != 'cargando' ?
              <ListaFacturas
                cajaSeleccionada={cajaSeleccionada}
                usuario={usuarioSeleccionado}
              />
              :
              <div style={{ marginTop: 75, marginLeft: 75 }}>
                <CircularProgress size={24} style={{ color: teal[500] }} />
              </div>
          }
        </>
      }
      {props.vistaContenedorPrincipal == 3 && <div style={{ width: '100%', height: '100vh' }}>
        <Button onClick={() => {
          event.preventDefault()
          $("#div_print").print();
        }} value="Imprimir" style={{ position: 'fixed' }}>Imprimir</Button>
        <div className='vertical' style={{ width: '100%', marginTop: 50 }} >

          <div id="div_print" style={{ backgroundColor: 'brown', width: '100 %', height: '100vh' }}>
            
            <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', border: '1px dashed' }}>
              <div style={{ width: '5%' }}> <span class="spacio">#</span></div>
              <div style={{ width: '12%' }}> <span class="spacio">Codigo Barras</span></div>
              <div style={{ width: '8%' }}> <span class="spacio">Cod Aux</span></div>
              <div style={{ width: '36%' }}> <span class="spacio">Descripcion</span></div>
              <div style={{ width: '15%' }}> <span class="spacio">Categoria Producto</span></div>
              <div style={{ width: '12%' }}> <span class="spacio">Marca Producto</span></div>
              <div style={{ width: '12%' }}> <span class="spacio">Fecha Registro</span></div>
            </div> 
           {/*  <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', border: '1px dashed' }}>
              <div style={{ width: '10%' }}> <span class="spacio">Fecha Venci</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Stock</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Precio Compra</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Pre Venta A</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Pre Venta B</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Pre Venta C</span></div>
              <div style={{ width: '30%' }}> <span class="spacio">Proveedor</span></div>
              <div style={{ width: '10%' }}> <span class="spacio">Iva</span></div>
            </div> */}


            {
              listaPro.map((item, i) => {
                return <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', border: '1px solid #545454' }}>
                  <div style={{ width: '5%' }}> <span class="spacio" style={{ color: 'red' }}>{i + 1}</span></div>
                  <div style={{ width: '12%' }}> <span class="spacio">{item.codigo_barras}</span>  </div>
                  <div style={{ width: '8%' }}> <span class="spacio">{item.codigo_referencia}</span></div>
                  <div style={{ width: '36%' }}> <span class="spacio">{item.descripcion_producto}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.categoria_producto}</span></div>
                  <div style={{ width: '12%' }}> <span class="spacio">{item.marca_producto}</span></div>
                  <div style={{ width: '12%' }}> <span class="spacio">{item.fecha_registro}</span></div>
                </div>
              })
            }

            {/*    
             <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', border: '1px solid #545454' }}>
                  <div style={{ width: '15%' }}> <span class="spacio" style={{ color: 'red' }}>{i + 1}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.codigo_barras}</span>  </div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.codigo_referencia}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.descripcion_producto}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.categoria_producto}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.marca_producto}</span></div>
                  <div style={{ width: '15%' }}> <span class="spacio">{item.fecha_registro}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.fecha_vencimiento}</span></div>

                </div>
             <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', border: '1px solid #545454' }}>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.fecha_vencimiento}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.stock_actual}</span>  </div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.precio_costo}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.precio_venta_a}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.precio_venta_b}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.precio_venta_c}</span></div>
                  <div style={{ width: '30%' }}> <span class="spacio">{item.proveedor}</span></div>
                  <div style={{ width: '10%' }}> <span class="spacio">{item.tiene_iva === true ? 'SI' : 'NO'}</span></div>

                </div> */}



          </div>

          {/* <TablaProductos lista={listaPro} /> */}
        </div>
        <style jsx global>{`
      
          @media print{
    @page {
      size: landscape;
    }
    
        
  }
        `}</style>

      </div>}
      {props.vistaContenedorPrincipal == 4 && <div>5</div>}
    </div>
  );
}

export default ContenedorPrincipal;
