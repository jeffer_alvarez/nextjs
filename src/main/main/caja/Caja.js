import "firebase/database";
import "firebase/auth";

import { Icon, Spinner } from "evergreen-ui";
import React, { useEffect, useState } from "react";

import ListaContent from "../punto_venta/ListaContent";
import MenuDerecho from "../punto_venta/MenuDerecho";
import TablaHistorialTransaccionesCaja from "./Components/TablaHistorialTransaccionesCaja";
import ToolbarsAccionesCaja from "./Components/ToolbarsAccionesCaja";
import firebase from "firebase/app";
import funtions from "../../../utils/funtions";
import themes from "../../../utils/themeCustom";

function Caja(props) {
  const [caja, setCaja] = useState("cargando");
  const [valorActualCaja, setValorActualCaja] = useState(0.0);
  const [usuario, setUsuario] = useState("");
  const [codigo, setCodigo] = useState("");
  const [saldoInicial, setSaldoInicial] = useState(0);
  const [listaTransacciones, setListaTransacciones] = useState([]);
  const [listaTransaccionesVentas, setListaTransaccionesVentas] = useState([]);
  const [
    listaTransaccionesIngresoDinero,
    setListaTransaccionesIngresoDinero
  ] = useState([]);
  const [
    listaTransaccionesDevolucionProveedores,
    setListaTransaccionesDevolucionProveedores
  ] = useState([]);
  const [
    listaTransaccionesDineroRetirado,
    setListaTransaccionesDineroRetirado
  ] = useState([]);
  const [
    listaTransaccionesVentasDevueltas,
    setListaTransaccionesVentasDevueltas
  ] = useState([]);
  const [
    listaTransaccionesDevolucionClientes,
    setListaTransaccionesDevolucionClientes
  ] = useState([]);
  const [
    listaTransaccionesCompraProductos,
    setListaTransaccionesCompraProductos
  ] = useState([]);
  const [
    listaTransaccionesDineroAcreditado,
    setListaTransaccionesDineroAcreditado
  ] = useState([]);

  useEffect(() => {
    setListaTransacciones([]);
    setListaTransaccionesVentas([]);
    setListaTransaccionesIngresoDinero([]);
    setListaTransaccionesDevolucionProveedores([]);
    setListaTransaccionesDineroRetirado([]);
    setListaTransaccionesVentasDevueltas([]);
    setListaTransaccionesDevolucionClientes([]);
    setListaTransaccionesCompraProductos([]);
    setListaTransaccionesDineroAcreditado([]);
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        var codigoCaja = "";

        // Estado caja y codigo
        var db = firebase
          .database()
          .ref("users")
          .child(user.uid)
          .child("caja")
          .child("cajas_abiertas_usuario");
        db.on("value", snap => {
          if (snap.val()) {
            setCaja("cargando");
            var caja = Object.values(snap.val());
            var estado = caja[0].estado;
            var codigoc = caja[0].codigo;
            var saldoInicio = caja[0].saldo_inicial;
            console.log(codigoc);
            setTimeout(() => {
              setSaldoInicial(saldoInicio);
              setCaja(caja);
              setUsuario(props.code);
              setCaja(estado);
              setCodigo(codigoc);
              obtenerDatos(codigoc);
            }, 100);
          } else {
            setCaja(false);
          }
        });
      }
    });
  }, []);

  const obtenerDatos = codigoCaja => {
    setListaTransacciones([]);
    setListaTransaccionesVentas([]);
    setListaTransaccionesIngresoDinero([]);
    setListaTransaccionesDevolucionProveedores([]);
    setListaTransaccionesDineroRetirado([]);
    setListaTransaccionesVentasDevueltas([]);
    setListaTransaccionesDevolucionClientes([]);
    setListaTransaccionesCompraProductos([]);
    setListaTransaccionesDineroAcreditado([]);
    // registro ventas diarias
    setTimeout(() => {
      firebase.auth().onAuthStateChanged(async user => {
        if (user) {
          var db = firebase.database();

          var cajaHistorial = db.ref(
            "users/" + user.uid + "/caja/cajas_normales/" + codigoCaja
          );

          cajaHistorial.on("value", snap => {
            if (snap.val()) {
              setValorActualCaja(snap.val().valor_caja)
              listaTransacciones.length = 0;
              setListaTransacciones(listaTransacciones);
              var listaVentas = snap.val().ventas ? snap.val().ventas : [];
              var listaVentasDevueltas = snap.val().ventas_devueltas
                ? snap.val().ventas_devueltas
                : [];
              var listaIngresoDinero = snap.val().ingreso_dinero
                ? snap.val().ingreso_dinero
                : [];
              var listaRetiroDinero = snap.val().retiro_dinero
                ? snap.val().retiro_dinero
                : [];
              var listaDineroAcreditado = snap.val()
                .lista_dinero_acreditado_venta_credito
                ? snap.val().lista_dinero_acreditado_venta_credito
                : [];
              var listaDevolucionProveedores = snap.val()
                .devoluciones_proveedores
                ? snap.val().devoluciones_proveedores
                : [];
              var listaDevolucionClientes = snap.val().devoluciones_clientes
                ? snap.val().devoluciones_clientes
                : [];
              var listaCompraProductos = snap.val().compras_productos
                ? snap.val().compras_productos
                : [];
              var listaStockSalida = snap.val().ajustes_stock_salidas
                ? snap.val().ajustes_stock_salidas
                : [];
              var listaStockEntrada = snap.val().ajustes_stock_entradas
                ? snap.val().ajustes_stock_entradas
                : [];

              /*      console.log(snap.val());
              console.log(Object.values(listaVentas));
              console.log(Object.values(listaVentasDevueltas));
              console.log(Object.values(listaIngresoDinero));
              console.log(Object.values(listaRetiroDinero));
              console.log(Object.values(listaDineroAcreditado));
              console.log(Object.values(listaDevolucionProveedores));
              console.log(Object.values(listaDevolucionClientes));
              console.log(Object.values(listaCompraProductos));
              console.log(Object.values(listaStockSalida));
              console.log(Object.values(listaStockEntrada)); */

              //lista ventas
              if (Object.values(listaVentas).length > 0) {
                setTimeout(() => {
                  setListaTransacciones("cargando");
                  var listaVentasM = Object.values(listaVentas);

                  listaVentasM.map(item => {
                    item.tipo_transaccion = "Venta";
                  });
                  listaTransacciones.push(...listaVentasM);
                  setListaTransaccionesVentas(listaVentasM);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista dinero ingresado
              if (Object.values(listaIngresoDinero).length > 0) {
                var listaIngresoDineroS = [];
                setTimeout(() => {
                  listaIngresoDineroS = Object.values(listaIngresoDinero);
                  listaIngresoDineroS.map(item => {
                    (item.tipo_transaccion = "Ingreso dinero"),
                      (item.total = item.valor);
                  });
                  listaTransacciones.push(...listaIngresoDineroS);
                  setListaTransaccionesIngresoDinero(listaIngresoDineroS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista dinero retirado
              if (Object.values(listaRetiroDinero).length > 0) {
                setTimeout(() => {
                  var listaRetiroDineroS = [];
                  listaRetiroDineroS = Object.values(listaRetiroDinero);
                  listaRetiroDineroS.map(item => {
                    (item.tipo_transaccion = "Retiro dinero"),
                      (item.total = item.valor);
                  });
                  listaTransacciones.push(...listaRetiroDineroS);
                  setListaTransaccionesDineroRetirado(listaRetiroDineroS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista ventas devueltas
              if (Object.values(listaVentasDevueltas).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaVentasDevueltasS = [];
                  listaVentasDevueltasS = Object.values(listaVentasDevueltas);
                  listaVentasDevueltasS.map(item => {
                    item.tipo_transaccion = "Ventas Devueltas";
                  });
                  listaTransacciones.push(...listaVentasDevueltasS);
                  setListaTransaccionesVentasDevueltas(listaVentasDevueltasS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista de dinero acreditado
              if (Object.values(listaDineroAcreditado).length > 0) {
                var listaDineroAcreditadoS = [];
                setTimeout(() => {
                  listaDineroAcreditadoS = Object.values(listaDineroAcreditado);
                  listaDineroAcreditadoS.map(item => {
                    (item.tipo_transaccion = "Pago venta a credito"),
                      (item.total = item.valor);
                  });
                  listaTransacciones.push(...listaDineroAcreditadoS);
                  setListaTransaccionesDineroAcreditado(listaDineroAcreditadoS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista de devoluciones a proveedores

              if (Object.values(listaDevolucionProveedores).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaDevolucionProveedoresS = [];
                  listaDevolucionProveedoresS = Object.values(snapshot.val());
                  listaDevolucionProveedoresS.map(item => {
                    item.tipo_transaccion = "Devolucion de Proveedores";
                    item.total = item.total_final;
                  });
                  setListaTransaccionesDevolucionProveedores(
                    listaDevolucionProveedoresS
                  );
                  listaTransacciones.push(...listaDevolucionProveedoresS);
                  setListaTransacciones(listaDevolucionProveedoresS);
                }, 100);
              } else {
              }

              //lista de devoluciones del cliente
              if (Object.values(listaDevolucionClientes).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaDevolucionClientesS = [];
                  listaDevolucionClientesS = Object.values(
                    listaDevolucionClientes
                  );
                  listaDevolucionClientesS.map(item => {
                    item.tipo_transaccion = "Devolucion de Clientes";
                    item.total = item.total_final;
                  });
                  listaTransacciones.push(...listaDevolucionClientesS);
                  setListaTransaccionesDevolucionClientes(
                    listaDevolucionClientesS
                  );
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista de compra de productos
              if (Object.values(listaCompraProductos).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaCompraProductosS = [];
                  listaCompraProductosS = Object.values(listaCompraProductos);
                  listaCompraProductosS.map(item => {
                    item.tipo_transaccion = "Compra de Productos";
                    item.total = item.total_final;
                  });
                  listaTransacciones.push(...listaCompraProductosS);
                  setListaTransaccionesCompraProductos(listaCompraProductosS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //lista de ajustes_stock_salidas
              if (Object.values(listaStockSalida).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaStockSalidaS = [];
                  listaStockSalidaS = Object.values(listaStockSalida);
                  listaStockSalidaS.map(item => {
                    item.tipo_transaccion = "Ajuste stock salida";
                    item.total = item.total_final;
                  });
                  listaTransacciones.push(...listaStockSalidaS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }

              //historial ajustes_stock_entrada
              if (Object.values(listaStockEntrada).length > 0) {
                setListaTransacciones("cargando");
                setTimeout(() => {
                  var listaStockEntradaS = [];
                  listaStockEntradaS = Object.values(listaStockEntrada);
                  listaStockEntradaS.map(item => {
                    item.tipo_transaccion = "Ajuste stock entradas";
                    item.total = item.total_final;
                  });
                  listaTransacciones.push(...listaStockEntradaS);
                  setListaTransacciones(listaTransacciones);
                }, 100);
              } else {
              }
            }
          });
          var fechaActual = funtions.obtenerFechaActual();

          // historial ingreso de dinero

          // dinero acreditado

          //historial devoluciones de proveedores

          //EGRESOS

          //historial retirar dinero
        }
      });
    }, 100);
  };

  return (
    <div style={{ background: themes.lightest }}>
      <ToolbarsAccionesCaja
        valorActualCaja={valorActualCaja}
        userUid={usuario}
        estadoCaja={caja}
        codigoCaja={codigo}
        listaTransacciones={listaTransacciones}
        listaTransaccionesVentas={listaTransaccionesVentas}
        listaTransaccionesIngresoDinero={listaTransaccionesIngresoDinero}
        listaTransaccionesDevolucionProveedores={
          listaTransaccionesDevolucionProveedores
        }
        listaTransaccionesDineroRetirado={listaTransaccionesDineroRetirado}
        listaTransaccionesVentasDevueltas={listaTransaccionesVentasDevueltas}
        listaTransaccionesDevolucionClientes={
          listaTransaccionesDevolucionClientes
        }
        listaTransaccionesCompraProductos={listaTransaccionesCompraProductos}
        listaTransaccionesDineroAcreditado={listaTransaccionesDineroAcreditado}
        saldo={saldoInicial}
      />
      {caja === false ? (
        <>
          <div
            style={{
              width: "100%",
              justifyContent: "center",
              textAlign: "center",
              alignItems: "center",
              height: "450px"
            }}
          >
            {/* <h4>Debe abrir caja para empezar a <br/>registrar todas las acciones el sistema</h4> */}
          </div>
        </>
      ) : (
        <>
          {listaTransacciones.length > 0 &&
            listaTransacciones != "cargando" && (
              <TablaHistorialTransaccionesCaja lista={listaTransacciones} />
            )}
        </>
      )}
    </div>
  );
}

export default Caja;
