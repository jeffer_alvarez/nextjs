import "firebase/database";
import "firebase/auth";

import React, { useEffect, useState } from "react";

import { Button } from "evergreen-ui/commonjs/buttons";
import { Dialog } from "evergreen-ui/commonjs/dialog";
import { Pane, TextInputField } from "evergreen-ui";
import { TextInput } from "evergreen-ui/commonjs/text-input";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";
import setSnackBars from "../../../../utils/setSnackBars";

function AbrirCaja(props) {
  const [estadoAbrirCaja, setSEstadoAbrirCaja] = useState(false);
  const [valorInicial, setValorInicial] = useState(props.saldo_anterior);
  const [sugerencia, setSugerencia] = useState("");

  /*  useEffect(()=>{
    setValorInicial(props.saldoAnterior)
  },[]) */

  const OpenCaja = async () => {
    setSEstadoAbrirCaja(true);
    var userUid = firebase.auth().currentUser.uid;
    var codigo = await funtions.guidGenerator();
    var fecha = await funtions.obtenerFechaActual();
    var hora = await funtions.obtenerHoraActual();
    var refCaja = firebase
      .database()
      .ref("users")
      .child(userUid)
      .child("caja")
      .child("cajas_abiertas_usuario")
      .child(codigo);

    refCaja
      .update({
        codigo: codigo,
        estado: true,
        fecha_abrir: fecha,
        fecha_cerrar: "",
        hora_abrir: hora,
        hora_cerrrar: "",
        observacion: sugerencia,
        order: new Date() + "",
        saldo_final: "0",
        saldo_inicial: valorInicial,
        usuario: props.userUid,
        usuario_cerrar: "",
        valor_caja: valorInicial
      })
      .then(() => {
        var refCajaNormales = firebase
          .database()
          .ref("users")
          .child(userUid)
          .child("caja")
          .child("cajas_normales")
          .child(codigo);
        refCajaNormales
          .update({
            codigo: codigo,
            estado: true,
            fecha_abrir: fecha,
            fecha_cerrar: "",
            hora_abrir: hora,
            hora_cerrrar: "",
            observacion: sugerencia,
            order: new Date() + "",
            saldo_final: "0",
            saldo_inicial: valorInicial,
            usuario: props.userUid,
            usuario_cerrar: "",
            valor_caja: valorInicial
          })
          .then(() => {
            setSEstadoAbrirCaja(false);
            props.close();
          });
      });
  };

  const ComprobarValor = e => {
    if (!isNaN(e.target.value)) {
      setValorInicial(e.target.value);
    } else {
      setSnackBars.openSnack(
        "error",
        "rootSnackBar",
        "Debe ingresar un valor numerico",
        2500
      );
    }
  };

  return (
    <Pane>
      {console.log(props.saldo_anterior)}
      <Dialog
        isShown={props.isShown}
        title={
          <div>
            Abrir Caja{" "}
            <span
              style={{
                float: "right",
                color: "#333",
                fontSize: 16,
                background: "#d4eee2",
                padding: 5,
                borderRadius: 25,
                fontFamily: "cursive",
                maxHeight: 650
              }}
            >
              Saldo anterior: $ {props.saldo_anterior}
            </span>
          </div>
        }
        shouldCloseOnOverlayClick={false}
        shouldCloseOnEscapePress={false}
        hasClose={false}
        confirmLabel="Abrir"
        hasFooter={false}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <TextInputField
            marginRight={16}
            height={40}
            value={valorInicial}
            width="49%"
            label="Saldo incial"
            onChange={e => ComprobarValor(e)}
            name="text-input-name"
            placeholder="Dinero inicial..."
          />
          <TextInputField
            height={40}
            value={sugerencia}
            width="49%"
            label="Sugerencia"
            onChange={e => setSugerencia(e.target.value)}
            name="text-input-name"
            placeholder="Sugerencia..."
          />
        </div>
        <hr />

        <Button
          onClick={() => OpenCaja()}
          appearance="primary"
          height={32}
          isLoading={estadoAbrirCaja}
          style={{ float: "right" }}
        >
          Abrir Caja
        </Button>
        <Button
          //  onClick={() => OpenCaja()}
          appearance="primary"
          iconBefore="delete"
          height={32}
          intent="danger"
          style={{ float: "right", marginRight: 15 }}
          onClick={() => props.close()}
        >
          Cancelar
        </Button>
      </Dialog>
    </Pane>
  );
}

export default AbrirCaja;
