import "firebase/database";
import "firebase/auth";

import { Alert, Pane, toaster } from "evergreen-ui";
import React, { useEffect, useState } from "react";

import { Button } from "evergreen-ui/commonjs/buttons";
import { Dialog } from "evergreen-ui/commonjs/dialog";
import { TextInput } from "evergreen-ui/commonjs/text-input";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";

function AgregarDinero(props) {
  const [cantidad, setCantidad] = useState("");
  const [motivo, setMotivo] = useState("");

  useEffect(() => {
    setCantidad("");
    setMotivo("");
  }, []);

  const [estadoCerrarCaja, setEstadoCerrarCaja] = useState(false);

  const addDollar = () => {
    setEstadoCerrarCaja(true);
    var uid = firebase.auth().currentUser.uid;
    if (cantidad > 0 && motivo.length > 0) {
      var uid = firebase.auth().currentUser.uid;
      var db = firebase.database();
      var codigo = funtions.guidGenerator();
      var fecha = funtions.obtenerFechaActual();
      var hora = funtions.obtenerHoraActual();
      var AunmentarDimero = db.ref(
        `users/${uid}/caja/cajas_normales/${props.codigoCaja}`
      );
      AunmentarDimero.once("value", snap => {
        if (snap.val()) {
          var dinero = snap.val().valor_caja;
          var refIngresoDinero = db.ref(
            `users/${uid}/caja/cajas_normales/${props.codigoCaja}/ingreso_dinero/${codigo}`
          );
          refIngresoDinero
            .update({
              estado: false,
              fecha: fecha,
              hora: hora,
              observacion: motivo,
              usuario: "hdsjkad789d7as9",
              valor: cantidad
            })
            .then(() => {
              AunmentarDimero.update({
                valor_caja: Number(dinero) + Number(cantidad)
              });
              setCantidad("");
              setMotivo("");
              setEstadoCerrarCaja(false);
              props.close();
            });
        }
      });
    } else {
      setEstadoCerrarCaja(false);
      toaster.warning("Debe de llenar todos los campos");
    }
  };

  return (
    <Pane>
      <Dialog
        isShown={props.isShown}
        title="  Transacción - Ingreso de dinero"
        shouldCloseOnOverlayClick={false}
        hasCancel={false}
        hasClose={false}
        confirmLabel="Agregar"
        shouldCloseOnEscapePress={false}
        onCloseComplete={() => props.close()}
        hasFooter={false}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <TextInput
            height={40}
            name="text-input-name"
            placeholder="Cantidad..."
            value={cantidad}
            onChange={e => {
              var dato = e.target.value;
              if (isNaN(dato)) {
              } else {
                setCantidad(e.target.value);
              }
            }}
            marginRight={16}
          />
          <TextInput
            height={40}
            value={motivo}
            onChange={e => {
              setMotivo(e.target.value);
            }}
            name="text-input-name"
            placeholder="Motivo..."
          />
        </div>
        <hr />
        <Button
          onClick={() => addDollar()}
          appearance="primary"
          iconBefore="dollar"
          height={32}
          isLoading={estadoCerrarCaja}
          disabled={estadoCerrarCaja === true ? true : false}
          style={{ float: "right", marginLeft: 15 }}
        >
          Ingresar dinero
        </Button>
        <Button
          //  onClick={() => OpenCaja()}
          appearance="primary"
          iconBefore="delete"
          height={32}
          intent="danger"
          disabled={estadoCerrarCaja === true ? true : false}
          style={{ float: "right" }}
          onClick={() => {
            setCantidad(0);
            setMotivo("");
            props.close();
          }}
        >
          Cancelar
        </Button>
      </Dialog>
    </Pane>
  );
}

export default AgregarDinero;
