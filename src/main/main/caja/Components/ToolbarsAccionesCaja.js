import "firebase/database";
import "firebase/auth";

import {
  Button,
  Icon,
  RadioGroup,
  SearchInput,
  Spinner,
  TextInput
} from "evergreen-ui";
import React, { useState } from "react";

import AbrirCaja from "./AbrirCaja";
import AgregarDinero from "./AgregarDinero";
import CerrarCaja from "./CerrarCaja";
import RetirarDinero from "./RetirarDinero";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";
import themes from "../../../../utils/themeCustom";

function ToolbarsAccionesCaja(props) {
  const [showAdd, setShowAdd] = useState(false);
  const [estadoAbrirCaja, setSEstadoAbrirCaja] = useState(false);
  const [showRemove, setShowRemove] = useState(false);
  const [value, setValue] = React.useState("nota-venta");
  const [showAbrirCaja, setAbrirCaja] = useState(false);
  const [showCerrarCaja, setCerrarCaja] = useState(false);
  const [fecha, setFecha] = useState("");
  const [saldoAnterior, setSaldoAnterior] = useState("");

  const OpenCaja = async () => {
    setSEstadoAbrirCaja(true);
    var db = firebase.database();
    var referernciaSaldo = await db
      .ref(`users/${firebase.auth().currentUser.uid}/caja/saldo_caja/saldo`)
      .once("value");
    if (referernciaSaldo.val()) {
      setSaldoAnterior(referernciaSaldo.val());
      setSEstadoAbrirCaja(false);
      setAbrirCaja(true);
    }
  };

  const cerrarCaja = () => {
    event.preventDefault();
    var fecha = funtions.obtenerFechaActual();
    setFecha(fecha);
    setCerrarCaja(true);
  };

  return (
    <div
      style={{
        width: "100%",
        height: "50px",
        background: themes.light,
        borderBottomWidth: "1px",
        borderBottomStyle: "solid",
        borderBottomColor: "rgba(0,0,0,0.08)",
        display: "flex",
        paddingLeft: 16,
        flexDirection: "row",
        alignItems: "center",
        position: "fixed",
        top: "50px",
        zIndex: "10"
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          with: "content-max",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        {props.estadoCaja === "cargando" && (
          <>
            <Spinner size={24} />
          </>
        )}
        {props.estadoCaja === false && (
          <>
            <span style={{ color: "#bf0e08", fontWeight: "800", fontSize: 24 }}>
              CAJA CERRADA
            </span>
          </>
        )}
        {props.estadoCaja != false && props.estadoCaja != "cargando" && (
          <>
            <span style={{ color: "#399d6c", fontWeight: "800", fontSize: 24 }}>
              CAJA ABIERTA
            </span>
          </>
        )}
        <div>
          <span style={{ fontSize: 15, fontWeight: 350, marginLeft: 16 }}>
            Zumbi, {funtions.obtenerFechaActual()}
          </span>
        </div>
      </div>
      <div style={{ flex: 0.6 }} />
      <div>
        {props.estadoCaja === "cargando" && (
          <>
            <Spinner size={24} />
          </>
        )}

        {props.estadoCaja === false && (
          <Button
            height={32}
            marginRight={16}
            appearance="primary"
            isLoading={estadoAbrirCaja}
            intent="success"
            iconBefore="bank-account"
            onClick={() => OpenCaja()}
          >
            ABRIR CAJA
          </Button>
        )}
        {props.estadoCaja === true && (
          <>
            <span
              style={{
                color: "#000000",
                fontWeight: "800",
                fontSize: 24,
                marginRight: 25
              }}
            >
              {Number(props.valorActualCaja).toFixed(2)}
            </span>
          </>
        )}
        {props.estadoCaja === true && (
          <>
            <Button
              height={32}
              marginRight={16}
              appearance="primary"
              intent="danger"
              isLoading={false}
              iconBefore="bank-account"
              onClick={() => cerrarCaja()}
            >
              CERRAR CAJA
            </Button>
          </>
        )}
        {props.estadoCaja === true && (
          <>
            <Button
              height={32}
              marginRight={16}
              intent="success"
              iconBefore="add"
              onClick={() => setShowAdd(true)}
            >
              INGRESAR DINERO
            </Button>
            <Button
              height={32}
              marginRight={16}
              intent="danger"
              iconBefore="minus"
              onClick={() => setShowRemove(true)}
            >
              RETIRAR DINERO
            </Button>
          </>
        )}

        <AgregarDinero
          isShown={showAdd}
          close={() => {
            setShowAdd(false);
          }}
          codigoCaja={props.codigoCaja}
        />
        <RetirarDinero
          isShown={showRemove}
          close={() => setShowRemove(false)}
          codigoCaja={props.codigoCaja}
        />
        {showCerrarCaja && (
          <CerrarCaja
            fecha={fecha}
            codigoCaja={props.codigoCaja}
            listaTransacciones={props.listaTransacciones}
            listaTransaccionesVentas={props.listaTransaccionesVentas}
            listaTransaccionesIngresoDinero={
              props.listaTransaccionesIngresoDinero
            }
            listaTransaccionesDevolucionProveedores={
              props.listaTransaccionesDevolucionProveedores
            }
            listaTransaccionesDineroRetirado={
              props.listaTransaccionesDineroRetirado
            }
            listaTransaccionesVentasDevueltas={
              props.listaTransaccionesVentasDevueltas
            }
            listaTransaccionesDevolucionClientes={
              props.listaTransaccionesDevolucionClientes
            }
            listaTransaccionesCompraProductos={
              props.listaTransaccionesCompraProductos
            }
            listaTransaccionesDineroAcreditado={
              props.listaTransaccionesDineroAcreditado
            }
            isShown={showCerrarCaja}
            close={() =>{ setCerrarCaja(false)
              location.reload()}}
            saldo={props.saldo}
          />
        )}

        {showAbrirCaja === true && (
          <AbrirCaja
            saldo_anterior={saldoAnterior}
            isShown={showAbrirCaja}
            userUid={props.userUid}
            close={() => setAbrirCaja(false)}
          />
        )}
      </div>
    </div>
  );
}

export default ToolbarsAccionesCaja;
