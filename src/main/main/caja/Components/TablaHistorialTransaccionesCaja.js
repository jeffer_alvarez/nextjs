import {
  Avatar,
  IconButton,
  Menu,
  Popover,
  Position,
  Table,
  Text,
  TextDropdownButton
} from "evergreen-ui";

import React from "react";
import { filter } from "fuzzaldrin-plus";
import profiles from "./profiles.json"; // eslint-disable-line import/extensions

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

const Order = {
  NONE: "NONE",
  ASC: "ASC",
  DESC: "DESC"
};

export default class AdvancedTable extends React.Component {
  state = {
    searchQuery: "",
    lista: [],
    orderedColumn: 1,
    ordering: Order.NONE,
    column2Show: "tipo_transaccion"
  };

  sort = profiles => {
    const { ordering, orderedColumn } = this.state;
    // Return if there's no ordering.
    if (ordering === Order.NONE) return profiles;

    // Get the property to sort each profile on.
    // By default use the `name` property.
    let propKey = "name";
    // The second column is dynamic.
    if (orderedColumn === 2) propKey = this.state.column2Show;
    // The third column is fixed to the `ltv` property.
    if (orderedColumn === 3) propKey = "ltv";

    return profiles.sort((a, b) => {
      let aValue = a[propKey];
      let bValue = b[propKey];

      // Parse money as a number.
      const isMoney = aValue.indexOf("$") === 0;

      if (isMoney) {
        aValue = Number(aValue.substr(1));
        bValue = Number(bValue.substr(1));
      }

      // Support string comparison
      const sortTable = { true: 1, false: -1 };

      // Order ascending (Order.ASC)
      if (this.state.ordering === Order.ASC) {
        return aValue === bValue ? 0 : sortTable[aValue > bValue];
      }

      // Order descending (Order.DESC)
      return bValue === aValue ? 0 : sortTable[bValue > aValue];
    });
  };

  // Filter the profiles based on the name property.
  filter = profiles => {
    const searchQuery = this.state.searchQuery.trim();

    // If the searchQuery is empty, return the profiles as is.
    if (searchQuery.length === 0) return profiles;

    return profiles.filter(profile => {
      // Use the filter from fuzzaldrin-plus to filter by name.
      const result = filter([profile.name], searchQuery);
      return result.length === 1;
    });
  };

  getIconForOrder = order => {
    switch (order) {
      case Order.ASC:
        return "arrow-up";
      case Order.DESC:
        return "arrow-down";
      default:
        return "caret-down";
    }
  };

  handleFilterChange = value => {
    this.setState({ searchQuery: value });
  };

  renderValueTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <TextDropdownButton>Tipo transacción</TextDropdownButton>
      </Table.TextHeaderCell>
    );
  };

  renderLTVTableHeaderCell = () => {
    return (
      <Table.TextHeaderCell>
        <Popover
          position={Position.BOTTOM_LEFT}
          content={({ close }) => (
            <Menu>
              <Menu.OptionsGroup
                title="Order"
                options={[
                  { label: "Ascending", value: Order.ASC },
                  { label: "Descending", value: Order.DESC }
                ]}
                selected={
                  this.state.orderedColumn === 3 ? this.state.ordering : null
                }
                onChange={value => {
                  this.setState({
                    orderedColumn: 3,
                    ordering: value
                  });
                  // Close the popover when you select a value.
                  close();
                }}
              />
            </Menu>
          )}
        >
          <TextDropdownButton
            icon={
              this.state.orderedColumn === 3
                ? this.getIconForOrder(this.state.ordering)
                : "caret-down"
            }
          >
            Monto
          </TextDropdownButton>
        </Popover>
      </Table.TextHeaderCell>
    );
  };

  renderRowMenu = () => {
    return (
      <Menu>
        <Menu.Group>
          <Menu.Item>Eliminar...</Menu.Item>
        </Menu.Group>
      </Menu>
    );
  };

  renderRow = ({ profile }) => {
    return (
      <Table.Row key={profile.codigo}>
        {/* <Table.Cell width={48} flex="none">
          <Popover
            content={this.renderRowMenu}
            position={Position.BOTTOM_RIGHT}
          >
            <IconButton icon="more" height={24} appearance="minimal" />
          </Popover>
        </Table.Cell> */}
        <Table.Cell display="flex" alignItems="center">
          <Text marginLeft={8} size={300} fontWeight={500}>
            {profile.cliente ? (
              <>
                {profile.cliente === "Consumidor Final" && "Consumidor Final"}
                {profile.cliente.nombre && <>{profile.cliente.nombre}</>}
              </>
            ) : (
              "Usuario del sistema"
            )}
          </Text>
        </Table.Cell>

        <Table.TextCell>
          {profile[this.state.column2Show]}{" "}
          <strong>
            {" "}
            (
            {profile.hora
              ? profile.hora
              : profile.hora_venta
              ? profile.hora_venta
              : "0:00"}
            )
          </strong>
        </Table.TextCell>
        <Table.TextCell isNumber>$ {profile.total}</Table.TextCell>
      </Table.Row>
    );
  };

  componentDidMount() {
    this.setState({
      lista: this.props.lista
    });
  }

  componentWillReceiveProps(props) {
    this.setState({
      lista: props.lista
    });
  }

  render() {
    const items = this.filter(this.sort(profiles));
    return (
      <Table border style={{ marginTop: 50 }}>
        <div
          style={{
            display: "flex",
            width: "100%",
            backgroundColor: "#f5f6f7",
            paddingTop: 18,
            paddingLeft: 15
          }}
        >
          <h6>Historial de transacciones</h6>
        </div>
        <Table.Head>
          <Table.HeaderCell width={0} flex="none" />
          <Table.SearchHeaderCell
            onChange={this.handleFilterChange}
            value={this.state.searchQuery}
          />
          {this.renderValueTableHeaderCell()}
          {this.renderLTVTableHeaderCell()}
        </Table.Head>
        <Table.VirtualBody height={600}>
          {this.state.lista.map(item => this.renderRow({ profile: item }))}
        </Table.VirtualBody>
      </Table>
    );
  }
}
