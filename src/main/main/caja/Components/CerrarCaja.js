import "firebase/database";
import "firebase/auth";

import { Pane, TextInputField } from "evergreen-ui";
import React, { useEffect, useState, Component } from "react";

import { Button } from "evergreen-ui/commonjs/buttons";
import { Dialog } from "evergreen-ui/commonjs/dialog";
import { TextInput } from "evergreen-ui/commonjs/text-input";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";

class CerrarCaja extends Component {
  state = {
    estadoCerrarCaja: false,
    valorInicial: 0,
    saldoABanco: "",
    saldoRestante: 0,
    totalVentas: 0,
    totalIngresoDinero: 0,
    totalIngresoDineroAcreditado: 0,
    totalDevolucionProve: 0,
    totalDineroRetirado: 0,
    totalVentasDevueltas: 0,
    totalDevolucionClientes: 0,
    totalCompraProductos: 0,
    sumaTotal: 0,
    sumaTotalTemporal: ""
  };

  componentDidMount() {
    this.obtenerValores();
  }

  componentWillReceiveProps() {
    this.obtenerValores();
  }

  obtenerValores() {
    $(".max-h_1duvey0").css({
      "max-height": "800px !important"
    });

    if (this.props.listaTransaccionesVentas.length > 0) {
      var total = 0;
      this.props.listaTransaccionesVentas.map(item => {
        total += Number(item.total);
      });
      this.setState({
        totalVentas: total
      });
    } else {
    }

    if (this.props.listaTransaccionesIngresoDinero.length > 0) {
      var totalIngresoDi = 0;
      this.props.listaTransaccionesIngresoDinero.map(item => {
        totalIngresoDi += Number(item.total);
      });
      this.setState({ totalIngresoDinero: totalIngresoDi });
    } else {
    }
    if (this.props.listaTransaccionesDineroAcreditado.length > 0) {
      var totalIngresoDA = 0;
      this.props.listaTransaccionesDineroAcreditado.map(item => {
        totalIngresoDA += Number(item.total);
      });
      this.setState({ totalIngresoDineroAcreditado: totalIngresoDA });
    } else {
    }

    if (this.props.listaTransaccionesDevolucionProveedores.length > 0) {
      var totalDevolucionProveedores = 0;
      this.props.listaTransaccionesDevolucionProveedores.map(item => {
        totalDevolucionProve += Number(item.total);
      });
      this.setState({ totalDevolucionProve: totalDevolucionProveedores });
    } else {
    }

    if (this.props.listaTransaccionesDineroRetirado.length > 0) {
      var DineroRetirado = 0;
      this.props.listaTransaccionesDineroRetirado.map(item => {
        DineroRetirado += Number(item.total);
      });
      this.setState({ totalDineroRetirado: DineroRetirado });
    } else {
      //setTotalDineroRetirado(0);
    }

    if (this.props.listaTransaccionesVentasDevueltas.length > 0) {
      var TotalVentasDevueltasA = 0;
      this.props.listaTransaccionesVentasDevueltas.map(item => {
        TotalVentasDevueltasA += Number(item.total);
      });
      this.setState({ totalVentasDevueltas: TotalVentasDevueltasA });
    } else {
      // setTotalVentasDevueltas(0);
    }

    if (this.props.listaTransaccionesDevolucionClientes.length > 0) {
      var TotalDevolucionClie = 0;
      this.props.listaTransaccionesDevolucionClientes.map(item => {
        TotalDevolucionClie += Number(item.total);
      });
      this.setState({ totalDevolucionClientes: TotalDevolucionClie });
    } else {
    }

    if (this.props.listaTransaccionesCompraProductos.length > 0) {
      console.log("paso com");
      var CompraProductos = 0;
      this.props.listaTransaccionesCompraProductos.map(item => {
        CompraProductos += Number(item.total);
      });
      this.setState({ totalCompraProductos: CompraProductos });
    } else {
    }

    this.calcularValores();
  }

  calcularValores() {
    setTimeout(() => {
      var totalIngresos = Number(
        this.state.totalVentas +
          this.state.totalIngresoDinero +
          this.state.totalDevolucionProve +
          this.state.totalIngresoDineroAcreditado +
          Number(this.props.saldo)
      );
      var totalEgresos = Number(
        this.state.totalDineroRetirado +
          this.state.totalDevolucionClientes +
          this.state.totalCompraProductos
      );
      if (isNaN(totalEgresos)) {
      } else {
        var sumaTotal = Number(totalIngresos) - Number(totalEgresos);
        this.setState({
          sumaTotal: Number(sumaTotal),
          sumaTotalTemporal: Number(sumaTotal)
        });
        // setSumaTotal();
      }
    }, 500);
  }

  CloseCaja = () => {
    this.setState({
      estadoCerrarCaja: true
    });
    var db = firebase.database();
    var uid = firebase.auth().currentUser.uid;
    var codigo = funtions.guidGenerator();
    var fecha = funtions.obtenerFechaActual();
    var hora = funtions.obtenerHoraActual();
    var refTransaccionesCaja = db.ref(
      `users/${uid}/caja/transacciones_caja/${this.props.codigoCaja}`
    );
    refTransaccionesCaja
      .update({
        fecha: fecha,
        hora: hora,
        order: new Date() + "",
        transacciones: {
          listaTransaccionesVentas: this.props.listaTransaccionesVentas,
          listaTransaccionesIngresoDinero: this.props
            .listaTransaccionesIngresoDinero,
          listaTransaccionesDevolucionProveedores: this.props
            .listaTransaccionesDevolucionProveedores,
          listaTransaccionesDineroRetirado: this.props
            .listaTransaccionesDineroRetirado,
          listaTransaccionesVentasDevueltas: this.props
            .listaTransaccionesVentasDevueltas,
          listaTransaccionesDevolucionClientes: this.props
            .listaTransaccionesDevolucionClientes,
          listaTransaccionesCompraProductos: this.props
            .listaTransaccionesCompraProductos
        }
      })
      .then(() => {
        var refCaja = db.ref(
          `users/${uid}/caja/cajas_abiertas_usuario/${this.props.codigoCaja}`
        );
        var refCajaaNormales = db.ref(
          `users/${uid}/caja/cajas_normales/${this.props.codigoCaja}`
        );
        var refSaldoProximaCaja = db.ref(`users/${uid}/caja/saldo_caja`);
        refCaja.remove();
        refCajaaNormales.update({
          estado: false,
          depositoBancario: this.state.saldoABanco
        });
        refSaldoProximaCaja.update({
          saldo: Number(this.state.sumaTotal).toFixed(2)
        });
        this.setState({
          estadoCerrarCaja: false
        });
       
        this.props.close();
      });
  };

  depositoBancario = e => {
    if (!isNaN(e.target.value)) {
      if (Number(e.target.value) > 0) {
        this.setState({
          saldoABanco: e.target.value
        });
        setTimeout(() => {
          var saldo =
            Number(this.state.sumaTotalTemporal) -
            Number(this.state.saldoABanco);
          this.setState({
            sumaTotal: saldo
          });
        }, 100);
      } else {
        this.setState({
          saldoABanco: e.target.value,
          sumaTotal: this.state.sumaTotalTemporal
        });
      }
    }
  };

  render() {
    return (
      <Pane height={650} maxHeight={650}>
        {console.log(this.state.sumaTotal)}
        <Dialog
          minHeightContent={800}
          maxHeight={650}
          height={650}
          isShown={this.props.isShown}
          title={
            <div>
              Cerrar Caja{" "}
              <span
                style={{
                  float: "right",
                  color: "#333",
                  fontSize: 16,
                  background: "#d4eee2",
                  padding: 5,
                  borderRadius: 25,
                  fontFamily: "cursive",
                  maxHeight: 650
                }}
              >
                Saldo inicial: $ {this.props.saldo}
              </span>
            </div>
          }
          shouldCloseOnOverlayClick={false}
          shouldCloseOnEscapePress={false}
          hasClose={false}
          hasFooter={false}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              textAlign: "center",
              marginTop: -9,
              marginBottom: 7
            }}
          >
            <span
              style={{ fontFamily: "cursive", fontWeight: 500, width: "50%" }}
            >
              Ingresos
            </span>

            <span
              style={{
                float: "right",
                fontFamily: "cursive",
                fontWeight: 500,
                textAlign: "center",
                width: "50%"
              }}
            >
              Egresos
            </span>
          </div>

          <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
            <div style={{ width: "50%", marginRight: 10 }}>
              <TextInputField
                width="100%"
                height={34}
                description="Total de ventas"
                value={this.state.totalVentas.toFixed(2)}
                name="text-input-name"
                disabled
              />

              <TextInputField
                width="100%"
                height={34}
                description="Total de ingresos de dinero"
                value={this.state.totalIngresoDinero}
                // onChange={e => ComprobarValor(e)}
                name="text-input-name"
                disabled
              />
              <TextInputField
                width="100%"
                height={34}
                description="Total de devolucion a proveedores"
                value={this.state.totalDevolucionProve}
                // onChange={e => ComprobarValor(e)}
                name="text-input-name"
                disabled
              />
              <TextInputField
                width="100%"
                height={34}
                description="Total dinero acreditado deudas"
                value={this.state.totalIngresoDineroAcreditado}
                // onChange={e => ComprobarValor(e)}
                name="text-input-name"
                disabled
              />
            </div>

            <div style={{ width: "50%" }}>
              <TextInputField
                height={34}
                value={this.state.totalDineroRetirado}
                width="100%"
                description="Total de dinero retirado"
                disabled
              />
              <TextInputField
                height={34}
                value={this.state.totalVentasDevueltas.toFixed(2)}
                width="100%"
                disabled
                description="Total de ventas devueltas"
              />
              <TextInputField
                height={34}
                disabled
                value={this.state.totalDevolucionClientes}
                width="100%"
                description="Total de devoluciones de clientes"
              />
              <TextInputField
                height={34}
                value={this.state.totalCompraProductos}
                width="100%"
                description="Total de compras de productos"
                disabled
              />
            </div>
          </div>
          <hr style={{ marginBottom: 6, marginTop: 4 }} />
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "100%",
              padding: ".1rem"
            }}
          >
            <span
              style={{
                fontFamily: "cursive",
                fontWeight: 500,
                width: "50%",
                fontSize: 14
              }}
            >
              Total Ingresos:{" "}
              <span
                style={{
                  fontSize: 12,
                  background: "#d4eee2",
                  padding: 7,
                  borderRadius: 25,
                  fontFamily: "cursive"
                }}
              >
                ${" "}
                {Number(
                  this.state.totalVentas +
                    this.state.totalIngresoDinero +
                    this.state.totalDevolucionProve +
                    this.state.totalIngresoDineroAcreditado
                ).toFixed(2)}
              </span>
            </span>

            <span
              style={{
                float: "right",
                fontFamily: "cursive",
                fontWeight: 500,
                width: "50%",
                fontSize: 14
              }}
            >
              Total Egresos: {"  "}
              <span
                style={{
                  fontSize: 12,
                  background: "#ef5350",
                  padding: 7,
                  borderRadius: 25,
                  fontFamily: "cursive"
                }}
              >
                $ {"  "}
                {Number(
                  this.state.totalDineroRetirado +
                    this.state.totalDevolucionClientes +
                    this.state.totalCompraProductos
                ).toFixed(2)}
              </span>
            </span>
          </div>
          <hr style={{ marginBottom: 4 }} />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <TextInputField
              height={34}
              value={this.state.saldoABanco}
              width="100%"
              style={{ marginInlineEnd: 5 }}
              label="Cierre de caja - Depósito bancario"
              onChange={e => this.depositoBancario(e)}
            />
          </div>
          <hr style={{ marginTop: 4 }} />
          <span style={{ fontFamily: "cursive", fontWeight: 500 }}>
            Saldo Final:{" "}
            <span
              style={{
                fontSize: 17,
                background: "#66bb6a",
                padding: 7,
                borderRadius: 25,
                fontFamily: "cursive"
              }}
            >
              {" "}
              ${" "}
              {this.state.sumaTotal === NaN
                ? 0.0
                : Number(this.state.sumaTotal).toFixed(2)}
            </span>
          </span>
          <Button
              onClick={() => this.CloseCaja()}
            appearance="primary"
            height={32}
            isLoading={this.state.estadoCerrarCaja}
            disabled={this.state.estadoCerrarCaja === true ? true : false}
            style={{ float: "right", marginLeft: 15 }}
          >
            Cerrar Caja
          </Button>
          <Button
            appearance="primary"
            iconBefore="delete"
            height={32}
            intent="danger"
            disabled={this.state.estadoCerrarCaja === true ? true : false}
            style={{ float: "right" }}
            onClick={() => this.props.close()}
          >
            Cancelar
          </Button>
        </Dialog>
      </Pane>
    );
  }
}

export default CerrarCaja;
