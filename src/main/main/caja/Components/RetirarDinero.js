import "firebase/database";
import "firebase/auth";

import React, { useEffect, useState } from "react";

import { Button } from "evergreen-ui/commonjs/buttons";
import { Dialog } from "evergreen-ui/commonjs/dialog";
import { Pane } from "evergreen-ui";
import { TextInput } from "evergreen-ui/commonjs/text-input";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";
import { toaster } from "evergreen-ui/commonjs/toaster";

function RetirarDinero(props) {
  const [cantidad, setCantidad] = useState("");
  const [motivo, setMotivo] = useState("");
  useEffect(() => {
    setCantidad("");
    setMotivo("");
  }, "");

  const [estadoCerrarCaja, setEstadoCerrarCaja] = useState(false);

  const removeDollar = () => {
    setEstadoCerrarCaja(true);
    var uid = firebase.auth().currentUser.uid;

    if (cantidad > 0 && motivo.length > 0) {
      var uid = firebase.auth().currentUser.uid;
      var db = firebase.database();
      var codigo = funtions.guidGenerator();
      var fecha = funtions.obtenerFechaActual();
      var hora = funtions.obtenerHoraActual();
      var disminuirMontoCaja = db.ref(
        `users/${uid}/caja/cajas_normales/${props.codigoCaja}`
      );
      disminuirMontoCaja.once("value", snap => {
        if (snap.val()) {
          var totalDinero = snap.val().valor_caja;
          if(Number(totalDinero)>=Number(cantidad)){
            var refRetirarDinero = db.ref(
              `users/${uid}/caja/cajas_normales/${props.codigoCaja}/retiro_dinero/${codigo}`
            );
            refRetirarDinero
              .update({
                estado: false,
                fecha: fecha,
                hora: hora,
                observacion: motivo,
                usuario: "hdsjkad789d7as9",
                valor: cantidad
              })
              .then(() => {
                disminuirMontoCaja.update({
                  valor_caja: Number(totalDinero) - Number(cantidad)
                });
                setCantidad("");
                setMotivo("");
                setEstadoCerrarCaja(false);
                props.close();
              });
          }else{
            setEstadoCerrarCaja(false);
            toaster.warning("No hay dinero suficiente en caja");

          }
        }
      });
    } else {
    }
  };

  return (
    <Pane>
      <Dialog
        isShown={props.isShown}
        title="  Transacción - Retiro de dinero"
        shouldCloseOnOverlayClick={false}
        hasCancel={false}
        hasClose={false}
        shouldCloseOnEscapePress={false}
        hasFooter={false}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <TextInput
            height={40}
            name="text-input-name"
            marginRight={16}
            value={cantidad}
            onChange={e => {
              var dato = e.target.value;
              if (isNaN(dato)) {
              } else {
                setCantidad(e.target.value);
              }
            }}
            placeholder="Cantidad..."
          />
          <TextInput
            height={40}
            name="text-input-name"
            value={motivo}
            onChange={e => {
              setMotivo(e.target.value);
            }}
            placeholder="Motivo..."
          />
        </div>
        <hr />
        <Button
          onClick={() => removeDollar()}
          appearance="primary"
          iconBefore="dollar"
          height={32}
          isLoading={estadoCerrarCaja}
          disabled={estadoCerrarCaja === true ? true : false}
          style={{ float: "right", marginLeft: 15 }}
        >
          Retirar dinero
        </Button>
        <Button
          //  onClick={() => OpenCaja()}
          appearance="primary"
          iconBefore="delete"
          height={32}
          intent="danger"
          disabled={estadoCerrarCaja === true ? true : false}
          style={{ float: "right" }}
          onClick={() => props.close()}
        >
          Cancelar
        </Button>
      </Dialog>
    </Pane>
  );
}

export default RetirarDinero;
