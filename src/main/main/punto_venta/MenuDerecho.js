import React from "react";
import themes from "../../../utils/themeCustom";
import NumeroVenta from "./components_menu_derecho/NumeroVenta";
import Cliente from "./components_menu_derecho/Cliente";
import Totales from "./components_menu_derecho/Totales";
import Cambio from "./components_menu_derecho/Cambio";
import TipoPago from "./components_menu_derecho/TipoPago";

function MenuDerecho(props) {
  return (
    <div
      style={{
        width: "300px",
        height: "85vh",
        borderLeftColor: "#95989A",
        borderLeftStyle: "solid",
        borderLeftWidth: "0.1px",
        position: "fixed",
        right: "0px",
        background: themes.lightest,
        marginTop: "90px"
      }}
    >
      <NumeroVenta />
      <Cliente />
      <Totales />
      <TipoPago />
      <Cambio />
    </div>
  );
}

export default MenuDerecho;
