import "firebase/database";
import "firebase/auth";

import React, { Component } from "react";

import { Pane } from "evergreen-ui/commonjs/layers";
import { Spinner } from "evergreen-ui";
import ToolbarSelectItem from "../../tollbars/ToolbarSelectItem";
import firebase from "firebase/app";

class PuntoVenta1 extends Component {

  state = {
    caja: 'cargando',
    lista: []
  }

  componentDidMount() {
    this.setState({
      caja: this.props.estado
    })
  }

  componentWillReceiveProps(props) {
    this.setState({
      caja: props.estado
    })
  }

  

  render() {
    return (
      <div>
        {
          this.state.caja === true &&
          <>
            <ToolbarSelectItem
              cliente={this.props.cliente}
              firebaseClientesConection={this.props.firebaseClientesConection}
              cargarLista={(lista) => this.props.cargarLista(lista)}
            />
          </>
        }
        <div>
          {
            this.state.lista.length > 0 &&
            <></>
          }
        </div>
        {
          this.state.caja === false &&
          <Pane
            display="flex"
            flexDirection="column"
            alignItems="center"
            fontFamily="cursive"
            justifyContent="center"
            height={600}
          >

            Debe abrir caja para empezar a trabajar
          </Pane>
        }
      </div>
    );
  }
}

export default PuntoVenta1;
