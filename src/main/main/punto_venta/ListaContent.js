import React, { useEffect, useState } from "react";

import ItemProducto from "./components_lista_content/ItemProducto";
import TablaProducto1 from "./components_lista_content/TablaProductos1";
import TablaProductos from "./components_lista_content/TablaProductos";
import Titles from "./components_lista_content/Titles";
import ToolBarAcciones from "../../tollbars/ToolBarAcciones";
import ToolbarSelectItem from "../../tollbars/ToolbarSelectItem";
import themes from "../../../utils/themeCustom";

function ListaContent(props) {
  const [lista, setLista] = useState([]);

 
  const cargarLista = listaProducto => {
    setLista(listaProducto);   
  };
  

  return (
    <div
      style={{
        width: "100%",
        marginTop: "130px",
        zIndex: "9"
      }}
    >   
      {/* <Titles /> */}
      {/* <ItemProducto /> */}

      <ToolbarSelectItem cliente={props.cliente} totales={(totales,totalIva,subTotal,estado)=>props.totales(totales,totalIva,subTotal,estado)}     
      />
      <ToolBarAcciones />
    </div>
  );
}

export default ListaContent;
