import React from "react";
import { Pane, TextInput, Heading, Radio } from "evergreen-ui";
import themes from "../../../../utils/themeCustom";

function TipoPago(props) {
  const [value, setValue] = React.useState("efectivo");

  return (
    <div
      style={{
        borderBottomColor: "#95989A",
        borderBottomStyle: "solid",
        borderBottomWidth: "0.1px",
        padding: 8
      }}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Radio
          checked={value == "efectivo" ? true : false}
          onClick={() => setValue("efectivo")}
          name="group"
          label="En Efectivo"
          margin={0}
        />
        <div style={{ flex: "1" }} />
        <Radio
          checked={value == "credito" ? true : false}
          onClick={() => setValue("credito")}
          name="group"
          label="A Credito"
          margin={0}
        />
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Radio
          checked={value == "tarjeta-credito" ? true : false}
          onClick={() => setValue("tarjeta-credito")}
          name="group"
          label="Tarjeta credito"
          margin={0}
        />
        <div style={{ flex: "1" }} />
        <Radio
          checked={value == "tarjeta-debito" ? true : false}
          onClick={() => setValue("tarjeta-debito")}
          name="group"
          label="Tarjeta debito"
          margin={0}
        />
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Radio
          checked={value == "cheque" ? true : false}
          onClick={() => setValue("cheque")}
          name="group"
          label="Cheque"
          margin={0}
        />
        <div style={{ flex: "1" }} />
      </div>
    </div>
  );
}

export default TipoPago;
