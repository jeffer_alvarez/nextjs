import React from "react";
import { Pane, TextInput, Heading, Radio } from "evergreen-ui";
import themes from "../../../../utils/themeCustom";

function Cambio(props) {
  const [value, setValue] = React.useState("efectivo");

  return (
    <div
      style={{
        borderBottomColor: "#95989A",
        borderBottomStyle: "solid",
        borderBottomWidth: "0.1px",
        padding: 8
      }}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <TextInput height={24} width={135} placeHolder="Dinero resibido" />
        <div style={{ flex: "1" }} />
        <Heading size={100}>Vuelto:</Heading>
        <Heading size={300}>0,00</Heading>
      </div>
    </div>
  );
}

export default Cambio;
