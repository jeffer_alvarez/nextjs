import React from "react";
import { Pane, TextInput, Heading } from "evergreen-ui";

function Cliente(props) {
  return (
    <div
      style={{
        borderBottomColor: "#95989A",
        borderBottomStyle: "solid",
        borderBottomWidth: "0.1px",
        padding: 8
      }}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <TextInput height={24} width={195} value="DARWIN ALVAREZ" />
        <div style={{ flex: "1" }} />
        <TextInput height={24} width={80} value="1900677533" />
      </div>
    </div>
  );
}

export default Cliente;
