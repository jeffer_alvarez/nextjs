import React from "react";

function NumeroVenta(props) {
  return (
    <div
      style={{
        borderBottomColor: "#95989A",
        borderBottomStyle: "solid",
        borderBottomWidth: "0.1px",
        paddingLeft: 8
      }}
    >
      N° 000056473
    </div>
  );
}

export default NumeroVenta;
