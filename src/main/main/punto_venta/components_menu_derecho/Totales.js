import React from "react";
import { Pane, TextInput, Heading } from "evergreen-ui";
import themes from "../../../../utils/themeCustom";

function Totales(props) {
  return (
    <div
      style={{
        borderBottomColor: "#95989A",
        borderBottomStyle: "solid",
        borderBottomWidth: "0.1px",
        padding: 8
      }}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Heading size={300}>Sub Total:</Heading>
        <div style={{ flex: "1" }} />
        <Heading size={300}>$12,00</Heading>
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Heading size={300}>Iva (12%):</Heading>
        <div style={{ flex: "1" }} />
        <Heading size={300}>$12,00</Heading>
      </div>
      <div style={{ display: "flex", flexDirection: "row" }}>
        <Heading size={300}>Total:</Heading>
        <div style={{ flex: "1" }} />
        <div
          style={{
            background: themes.base,
            paddingLeft: 8,
            paddingRight: 4,
            paddingTop: 2,
            paddingBottom: 2,
            borderRadius: 2
          }}
        >
          <Heading size={300} style={{ color: themes.lightest }}>
            $12,00
          </Heading>
        </div>
      </div>
    </div>
  );
}

export default Totales;
