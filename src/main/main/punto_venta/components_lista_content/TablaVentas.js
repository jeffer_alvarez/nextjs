import "firebase/database";
import "firebase/auth";

import { Button, Table, toaster } from "evergreen-ui";
import { CircularProgress, IconButton, Tooltip } from "@material-ui/core";
import React, { Component } from 'react';

import CloseIcon from '@material-ui/icons/Close';
import ContainerPlantillas from "../../../../utils/container_plantillas";
import DoneAllIcon from '@material-ui/icons/DoneAll';
import DoneIcon from '@material-ui/icons/Done';
import EditarIcon from '@material-ui/icons/Edit';
import EmitirFacturaModal from "./EmitirFacturaModal";
import ErrorEstado from "./ErrorEstado";
import GenerarPDF from "./GenerarPDF";
import GenerarResivo from "./GenerarResivo";
import InputIcon from '@material-ui/icons/Input';
import LocalPrintshopIcon from '@material-ui/icons/LocalPrintshop';
import ModalCancelarVenta from "./ModalCancelarVenta";
import ModalContainerNormal from "./ModalContainerNormal";
import MostrarProductosVenta from "./MostrarProductosVenta";
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ReactToPrint from "react-to-print";
import ResivoVenta from "../../../../utils/resivo_venta";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";
import { teal } from "@material-ui/core/colors";

class TablaVentas extends Component {

    state = {
        codigoEmitirFactura: '',
        estadoModalCancelarVenta: false,
        itemFormateadoImprimir: [],
        eliminando: false,
        item: [],
        emisor: [],
        imprimir: false,
        mes: '',
        claveAcceso: '',
        imprimirResivo: false

    }


    componentDidMount() {

    }

    updateDataProductos = codigoVenta => {
        this.setState({
            eliminando: true
        })
        var db = firebase.database();
        var ventaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas/' + codigoVenta);
        var operacionVentaRefCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_abiertas_usuario')
        //cambiar numero factura
        //    this.restarNumeroFactura()
        ventaRef.on('value', (snapshot) => {
            if (snapshot.val()) {

                operacionVentaRefCaja.once('value', (snap) => {
                    if (snap.val()) {
                        var caja = funtions.snapshotToArray(snap).filter(it => it.usuario === this.props.usuario.code)[0]

                        var cajaRefValorActual = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo)

                        cajaRefValorActual.once('value', (snap) => {
                            if (snap.val()) {
                                if (Number(snap.val().valor_caja) < Number(snapshot.val().total)) {
                                    setSnackBars.openSnack('error', 'rootSnackBar', 'Dinero insuficiente en caja', 2000)
                                } else {
                                    snapshot.val().productos.forEach(element => {
                                        var productoRef = db.ref('users/' + firebase.auth().currentUser.uid + '/productos/' + element.codigo)
                                        productoRef.once('value', (snapshot) => {
                                            if (snapshot.val()) {
                                                productoRef.update({
                                                    stock_actual: Number(snapshot.val().stock_actual) + Number(element.cantidad)
                                                })
                                            }
                                        })
                                    })

                                    this.setVentaDevuelta(snapshot.val(), snapshot.val().tipo_pago)
                                    this.postSetDevolverProductosMasVendidosReportes(firebase.auth().currentUser.uid, snapshot.val().productos)
                                    this.setVentaCaja(snapshot.val(), snapshot.val().tipo_pago)
                                    var codigocaja = caja.codigo
                                    setTimeout(() => { this.deleteVenta(snapshot.val().codigo, codigocaja) }, 300)

                                }
                            }
                        })
                    }
                })
            }
        })
    }

    postSetDevolverVentasDiariasReportes = async (uidUser, valor) => {
        const rawResponse = await fetch('https://server-reportes.herokuapp.com/devolver_ventas_diarias', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'uid': uidUser,
                fecha: funtions.obtenerFechaActualReportes(),
            },
            body: JSON.stringify({
                "valor_total": valor
            })
        }).then(function (response) {
            return response.json();
        })
            .then(function (myJson) {
                console.log(myJson);
            });
    }

    postSetDevolverCuentasPorCobrarReportes = async (uidUser, valor,estado) => {
        const rawResponse = await fetch('https://server-reportes.herokuapp.com/devolucion_cuentas_por_cobrar_diario', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'uid': uidUser,
                fecha: funtions.obtenerFechaActualReportes(),
            },
            body: JSON.stringify({
                "estado":estado,
                "valor":valor
                })
        }).then(function (response) {
            return response.json();
        })
            .then(function (myJson) {
                console.log(myJson);
            });


    }

    postSetDevolverProductosMasVendidosReportes = async (uidUser, listaVentas) => {
        const rawResponse = await fetch('https://server-reportes.herokuapp.com/devolver_mas_vendidos', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'uid': uidUser,
                fecha: funtions.obtenerFechaActualReportes(),
            },
            body: JSON.stringify(listaVentas)
        })
    }

    deleteVenta = (codigo, caja) => {
        var db = firebase.database();
        var ventRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas/' + codigo);
        console.log(caja);
        var ventRefCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/' + caja + "/" + codigo);
        ventRef.remove().then(() => {
            ventRefCaja.remove().then(() => {
                this.setState({
                    eliminando: false
                })
                toaster.notify('Venta eliminada con exito')
                this.setState({ estadoModalCancelarVenta: false })
            }).then(error => {
                console.error(error)
            })

        })

    }

    setVentaDevuelta(itemVenta, tipo_pago) {
        var db = firebase.database();
        var codigoVentaCaja = funtions.guidGenerator()
        var operacionVentaRefCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_abiertas_usuario')
        operacionVentaRefCaja.once('value', (snap) => {
            if (snap.val()) {
                var caja = funtions.snapshotToArray(snap).filter(it => it.usuario === this.props.usuario.code)[0]
                if (Boolean(caja.estado)) {
                    var operacionVentaDevuelta = db.ref('users/' + firebase.auth().currentUser.uid + '/lista_ventas/ventas_devueltas/' + itemVenta.codigo)
                    itemVenta.caja = caja.codigo
                    itemVenta.factura_emitida = 'devuelta'
                    operacionVentaDevuelta.set(itemVenta)
                    if (Number(itemVenta.valor_acreditado) > 0) {
                        var operacionAcreditadoDevueltoCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/ventas_devueltas/lista_dinero_acreditado_venta_credito/' + itemVenta.codigo)
                        operacionAcreditadoDevueltoCaja.remove()

                        var operacionValorTotalCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo)
                        operacionValorTotalCaja.once('value', (snap) => {
                            if (snap.val()) {
                                operacionValorTotalCaja.update({
                                    valor_caja: Number(snap.val().valor_caja) - Number(itemVenta.valor_acreditado)
                                })
                            }
                        })
                    }
                }
            }
        })
    }

    setVentaCaja(itemVenta, tipo_pago) {
        console.log(itemVenta.cliente.identificacion)
        var db = firebase.database();
        var codigoVentaCaja = funtions.guidGenerator()
        var operacionVentaRefCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_abiertas_usuario')
        operacionVentaRefCaja.once('value', (snap) => {
            if (snap.val()) {
                var caja = funtions.snapshotToArray(snap).filter(it => it.usuario === this.props.usuario.code)[0]
                if (Boolean(caja.estado)) {

                    if (itemVenta.tipo_pago === 'efectivo') {
                        var operacionVentaCaja = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo + '/ventas_devueltas/' + itemVenta.codigo)
                        operacionVentaCaja.set(itemVenta)
                        this.postSetDevolverVentasDiariasReportes(firebase.auth().currentUser.uid, itemVenta.total)
                    }

                    var operacionVentaCajaEliminar = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo + '/ventas/' + itemVenta.codigo)
                    operacionVentaCajaEliminar.remove()

                    var cajaRefValorAcreditado = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo + '/lista_dinero_acreditado_venta_credito/' + itemVenta.codigo)
                    cajaRefValorAcreditado.remove()



                    if (itemVenta.tipo_pago === 'credito') {
                        console.log("caja credito")
                        var cuentaCobrarDeudaQuitarRef = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/').orderByChild('cliente/identificacion').equalTo(itemVenta.cliente.identificacion)
                        cuentaCobrarDeudaQuitarRef.once('value', (snap2) => {
                            if (snap2.val()) {
                                var cuentaCobrarDeudaQuitarRef = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/' + funtions.snapshotToArray(snap2)[0].cliente.identificacion + '/lista_deudas/' + itemVenta.codigo)
                                cuentaCobrarDeudaQuitarRef.remove()

                            }else{
                                console.log("na allo anada ")
                            }
                        })
                        this.postSetDevolverVentasDiariasReportes(firebase.auth().currentUser.uid, itemVenta.valor_acreditado)
                        var saldoDescontar=itemVenta.total-itemVenta.valor_acreditado
                        this.postSetDevolverCuentasPorCobrarReportes(firebase.auth().currentUser.uid, saldoDescontar,"existe")
                    }


                    setTimeout(() => {
                        var cuentaCobrarDeudaQuitarTodo = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/' + itemVenta.cliente.identificacion + '/lista_deudas/')
                        cuentaCobrarDeudaQuitarTodo.once('value', (snap3) => {
                            if (snap3.val()) {
                                console.log(snap3.val());

                            } else {
                                var cuentaCobrarDeudaQuitarTodoAhora = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/' + itemVenta.cliente.identificacion)
                                cuentaCobrarDeudaQuitarTodoAhora.remove()
                            }
                        })
                    }, 5000);
                    var cajaRefValorActual = db.ref('users/' + firebase.auth().currentUser.uid + '/caja/cajas_normales/' + caja.codigo)

                    if (Number(itemVenta.valor_acreditado) > 0) {
                        var cuentaCobrarDeudaQuitarRef = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/').orderByChild('cliente/identificacion').equalTo(itemVenta.cliente.identificacion)
                        cuentaCobrarDeudaQuitarRef.once('value', (snap2) => {
                            if (snap2.val()) {
                                var cuentaCobrarDeudaQuitarRef = db.ref('users/' + firebase.auth().currentUser.uid + '/cuentas_por_cobrar/cuentas_por_cobrar_basicas/' + funtions.snapshotToArray(snap2)[0].cliente.codigo + '/lista_acreditados/' + itemVenta.codigo)
                                cuentaCobrarDeudaQuitarRef.remove()
                            }
                        })
                    }

                    if (itemVenta.tipo_pago === 'efectivo') {
                        cajaRefValorActual.once('value', (snap2) => {
                            if (snap2.val()) {
                                cajaRefValorActual.update({
                                    valor_caja: Number(Number(snap2.val().valor_caja) - Number(itemVenta.total)).toFixed(2)
                                })

                            }
                        })
                    }

                }
            }
        })
    }

    enviarToPlantillaData = item => {
        console.log(item)
        const itemFormat = {
            numero_venta: item.numero_factura,
            tipo_venta: item.tipo_venta,
            productos: item.productos,
            subtotal: item.subtotal,
            iva: item.iva,
            total: item.total,
            descuento: item.descuento,
            fecha_venta: item.fecha_venta,
            hora_venta: item.hora_venta,
            tipo_pago: item.tipo_pago,
            valor_acreditado: item.valor_acreditado,
            fecha_a_pagar: item.fecha_a_pagar,
            numero_tarjeta: item.numero_tarjeta,
            nombre_banco: item.nombre_banco,
        }
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                var db = firebase.database();
                if (item.tipo_venta === 'factura') {
                    //    var clienteRef = db.ref('users/' + user.uid + '/clientes/' + item.cliente.codigo);
                    var empresaRef = db.ref('auth_admins/' + user.uid)
                    itemFormat.nombreCliente = item.cliente.nombre
                    itemFormat.emailCliente = item.cliente.email
                    itemFormat.identificacionCliente = item.cliente.identificacion
                    itemFormat.direccionCliente = item.cliente.direccion
                    empresaRef.once('value', (snap) => {
                        if (snap.val()) {
                            itemFormat.nombreEmpresa = snap.val().nombre_comercial
                            itemFormat.razon_social = snap.val().razon_social
                            itemFormat.direccion = snap.val().direccion
                            this.setState({
                                itemFormateadoImprimir: itemFormat,
                                // imprimirResivo: true
                            })
                            this.refEventoImprimir.handlePrint()
                        }
                    })
                    /*  clienteRef.once('value', (snapshot) => {
                         if (snapshot.val()) {
                             itemFormat.nombreCliente = snapshot.val().nombre
                             itemFormat.emailCliente = snapshot.val().email
                             itemFormat.identificacionCliente = snapshot.val().numero_identificacion
                             itemFormat.direccionCliente = snapshot.val().direccion
 
                          
                         }
                     }) */
                } else {
                    console.log('sdfsdf');
                    var empresaRef = db.ref('auth_admins/' + user.uid)
                    empresaRef.once('value', (snap) => {
                        if (snap.val()) {
                            itemFormat.nombreEmpresa = snap.val().nombre_comercial
                            itemFormat.razon_social = snap.val().razon_social
                            itemFormat.direccion = snap.val().direccion
                            this.setState({
                                itemFormateadoImprimir: itemFormat,
                                //  imprimirResivo: true
                            })
                            this.refEventoImprimir.handlePrint()
                            setTimeout(() => {

                                console.log(this.state.itemFormateadoImprimir);
                            }, 300);

                        }
                    })
                }
            }
        })

    }

    comprobarUsuario = (item) => {
        this.setState({
            codigoEmitirFactura: item.codigo,
            estadoModalEmitirFactura: true,
        })

    }


    recuperarJsonFactura = codigo => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                var db = firebase.database();
                var productosRef = db.ref('users/' + user.uid + '/facturas_ventas/' + codigo);
                var venteRef = db.ref('users/' + user.uid + '/ventas/' + codigo);
                productosRef.once('value', (snapshot) => {
                    if (snapshot.val()) {
                        venteRef.once('value', snapCaja => {
                            if (snapCaja.val()) {
                                var venteRefCaja = db.ref('users/' + user.uid + '/ventas_caja/' + snapCaja.val().caja + '/' + codigo);
                                if (snapCaja.val().lugar === "autorizacion") {

                                    this.postSetReenviar(user.uid, snapCaja.val().claveAcceso, codigo, snapCaja.val().caja)
                                    venteRef.update({
                                        factura_emitida: 'pendiente'
                                    })
                                    venteRefCaja.update({
                                        factura_emitida: 'pendiente'
                                    })
                                } else {
                                    this.postSet(user.uid, snapshot.val(), codigo, snapCaja.val().caja)
                                    venteRef.update({
                                        factura_emitida: 'pendiente'
                                    })
                                    venteRefCaja.update({
                                        factura_emitida: 'pendiente'
                                    })
                                }

                            }
                        })
                        //setSnackBars.openSnack('success', 'rootSnackBar', 'Factura emitida con exito', 2000)
                    } else {
                        console.log('no encontro nada')
                    }
                })
            }
        })
    }

    recuperarJsonFacturaReenviar = codigo => {
        console.log('dasd')
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                var db = firebase.database();
                var productosRef = db.ref('users/' + user.uid + '/facturas_ventas/' + codigo);
                var venteRef = db.ref('users/' + user.uid + '/ventas/' + codigo);
                console.log(productosRef.ref)
                productosRef.once('value', (snapshot) => {
                    if (snapshot.val()) {
                        venteRef.once('value', snapCaja => {
                            if (snapCaja.val()) {
                                var venteRefCaja = db.ref('users/' + user.uid + '/ventas_caja/' + snapCaja.val().caja + '/' + codigo);
                                if (snapCaja.val().lugar === 'autorizacion') {
                                    /*  this.postSetReenviar(user.uid, snapCaja.val().claveAcceso, codigo, snapCaja.val().caja)
                                     venteRef.update({
                                         factura_emitida: 'pendiente'
                                     })
                                     venteRefCaja.update({
                                         factura_emitida: 'pendiente'
                                     }) */
                                } else {
                                    this.postSet(user.uid, snapshot.val(), codigo, snapCaja.val().caja)
                                    venteRef.update({
                                        factura_emitida: 'pendiente'
                                    })
                                    venteRefCaja.update({
                                        factura_emitida: 'pendiente'
                                    })
                                }
                            }
                        })
                        //setSnackBars.openSnack('success', 'rootSnackBar', 'Factura emitida con exito', 2000)
                    } else {
                        console.log('no encontro nada')
                    }
                })
            }
        })
    }

    postSet = async (uidUser, jsonData, codigo, caja) => {
        console.log('sad')
        //const rawResponse = await fetch('https://stormy-bayou-19844.herokuapp.com/generarfactura', {
        const rawResponse = await fetch('https://server-desarrollo.herokuapp.com/generarfactura', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'id': uidUser,
                'codigo': codigo,
                caja: caja
            },
            body: JSON.stringify(jsonData)
        })
    }
    postSetReenviar = async (uidUser, claveAccesos, codigo, caja) => {
        console.log(claveAccesos)
        //const rawResponse = await fetch('https://stormy-bayou-19844.herokuapp.com/generarfactura', {
        var claveAcceso = {
            'claveAcceso': `${claveAccesos}`
        }
        const rawResponse = await fetch('https://server-desarrollo.herokuapp.com/reenviar', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'id': uidUser,
                'codigo': codigo,
                'caja': caja,
                'claveAcceso': `${claveAccesos}`
            },
            body: JSON.stringify(claveAcceso)
        })
    }

    obtenerMes = (mes) => {
        var devolucion = ''
        switch (mes) {
            case '07':
                devolucion = 'Julio'
                break;
        }

        return devolucion
    }

    imprimirPDF = (codigo) => {
        console.log(codigo)
        var db = firebase.database()
        this.setState({
            item: [],
            emisor: []
        })
        var refEmisor = db.ref(`auth_admins/${firebase.auth().currentUser.uid}`)

        var refImprimir = db.ref(`users/${firebase.auth().currentUser.uid}/facturas_ventas/${codigo}`)
        refImprimir.once('value', snap => {
            if (snap.val()) {
                this.state.item.push(snap.val())
                var fecha = snap.val().fecha_emision
                var mes = `${fecha.substr(3, 2)}`
                var dia = `${fecha.substr(0, 2)}`
                var mesFactura = this.obtenerMes(mes)
                refEmisor.once('value', snapshot => {
                    if (snapshot.val()) {
                        this.state.emisor.push(snapshot.val())
                        setTimeout(() => {
                            this.setState({
                                emisor: this.state.emisor,
                                mes: mesFactura,
                                item: this.state.item,
                                claveAcceso: snap.val().clave_acceso,
                                imprimir: true,
                            })
                        }, 300);
                    }
                })

            }
        })


    }



    render() {
        return (
            <div style={{ width: '100%' }}>
                <Table width="100%"
                // style={{ height: '91vh' }}
                >
                    <Table.Head>
                        {/*                     <Table.SearchHeaderCell width="5px" /> */}
                        <Table.TextHeaderCell width={115} flex="none" ></Table.TextHeaderCell>
                        <Table.TextHeaderCell width={100} flex="none">Numero de Comprobante</Table.TextHeaderCell>
                        <Table.TextHeaderCell width={205} flex="none"> Estado</Table.TextHeaderCell>
                        <Table.TextHeaderCell width={130} flex="none">Cliente</Table.TextHeaderCell>
                        <Table.TextHeaderCell width={156} flex="none">Productos</Table.TextHeaderCell>
                        <Table.TextHeaderCell width={85} flex="none">Tipo Pago</Table.TextHeaderCell>
                        {/*      <Table.TextHeaderCell width={100} flex="none">Subtotal</Table.TextHeaderCell> */}
                        <Table.TextHeaderCell width={85} flex="none">Total</Table.TextHeaderCell>
                        <Table.TextHeaderCell width={85} flex="none">Descuento</Table.TextHeaderCell>
                        {/* <Table.TextHeaderCell>Empleado</Table.TextHeaderCell> */}
                        <Table.TextHeaderCell width={175} flex="none" >Fecha - Hora venta</Table.TextHeaderCell>

                    </Table.Head>
                    <Table.Body height='auto' padding='0px'>
                        {
                            this.props.estadoTabla === 'cargando' &&
                            <>
                                <Table.Row isSelectable padding='0px' style={{ height: '75vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    <CircularProgress size={24} style={{ color: teal[500] }} />
                                </Table.Row>
                            </>
                        }
                        {
                            this.props.estadoTabla === 'vacio' &&
                            <>
                                <Table.Row isSelectable padding='0px' style={{ height: '75vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    Todavia no se han realizado ventas
                              </Table.Row>
                            </>
                        }
                        {
                            this.props.estadoTabla === 'llena' &&
                            <>
                                {this.props.lista.map((profile, i) => (
                                    <Table.Row key={profile.codigo} isSelectable padding='0px'>
                                        <Table.TextCell width={115} flex="none">
                                            <div style={{ display: 'flex', flexDirection: 'row', width: '50px' }}>
                                                <ReactToPrint
                                                    ref={el => (this.refEventoImprimir = el)}
                                                    trigger={() => <></>}
                                                    content={() => this.refImprimirResivo}
                                                />
                                                <Tooltip title="Imprimir resivo">
                                                    <IconButton onClick={() => {
                                                        this.enviarToPlantillaData(profile)

                                                    }}
                                                    >
                                                        <LocalPrintshopIcon fontSize="small" />
                                                    </IconButton>
                                                </Tooltip>
                                                {

                                                    <Tooltip title="Descargar pdf">
                                                        <IconButton onClick={() => this.imprimirPDF(profile.codigo)}
                                                        >
                                                            <PictureAsPdfIcon fontSize="small" />
                                                        </IconButton>
                                                    </Tooltip>
                                                }
                                                {

                                                    profile.urlpdf === '' &&
                                                    <></>
                                                }

                                            </div>
                                        </Table.TextCell>
                                        <Table.TextCell width={100} flex="none">{profile.numero_factura}</Table.TextCell>
                                        <Table.TextCell width={205} flex="none">{
                                            profile.cliente === 'Consumidor Final' ?
                                                <div style={{ display: 'flex', flexDirection: 'row', width: 'max-content' }}>


                                                    {
                                                        profile.factura_emitida === 'emitida' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <IconButton disabled>
                                                                <DoneAllIcon style={{ color: '#00c853' }} fontSize="small" />
                                                            </IconButton>
                                                            <div style={{ color: '#00c853', display: 'flex', alignItems: 'center' }}>Emitida</div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'no_emitida' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Devolver Venta">
                                                                <IconButton
                                                                    onClick={() => {
                                                                        this.setState({
                                                                            codigoEmitirFactura: profile.codigo,
                                                                            estadoModalCancelarVenta: true,
                                                                        })

                                                                    }}>
                                                                    <CloseIcon style={{ color: '#EF5350' }} fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <Tooltip title="Emitir Factura">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'emitir_factura'
                                                                    })
                                                                    setTimeout(() => {
                                                                        this.comprobarUsuario(profile)
                                                                    }, 100)
                                                                }}>
                                                                    <InputIcon color='primary' fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'reenviar' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Emitir Factura">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'emitir_factura'
                                                                    })
                                                                    setTimeout(() => {
                                                                        this.comprobarUsuario(profile)
                                                                    }, 100)
                                                                }}>
                                                                    <InputIcon color='primary' fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <div style={{ display: 'flex', color: 'red', alignItems: 'center', fontSize: 9, wordWrap: 'break-word' }}>REENVIAR: {profile.error_factura_emitida.toUpperCase()} </div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'pendiente' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <IconButton disabled>
                                                                <CircularProgress size={20} thickness={5} style={{ color: '#42A5F5' }} />
                                                            </IconButton>
                                                            <div style={{ color: '#42A5F5', display: 'flex', alignItems: 'center' }}>Emitiendo...</div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'error' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Devolver Venta">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'devolver_venta'
                                                                    })
                                                                    setTimeout(() => {

                                                                        this.setState({
                                                                            codigoEmitirFactura: profile.codigo,
                                                                            estadoModalCancelarVenta: true,
                                                                        })
                                                                    }, 100)
                                                                }}>
                                                                    <CloseIcon style={{ color: '#EF5350' }} fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <div style={{ display: 'flex', color: 'red', alignItems: 'center', fontSize: 9, wordWrap: 'break-word' }}>{profile.error_factura_emitida}</div>
                                                        </div>
                                                    }

                                                </div>
                                                :
                                                <div style={{ width: 'max-content', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>

                                                    {
                                                        profile.factura_emitida === 'emitida' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <IconButton disabled>
                                                                <DoneAllIcon style={{ color: '#00c853' }} fontSize="small" />
                                                            </IconButton>
                                                            <div style={{ color: '#00c853', display: 'flex', alignItems: 'center' }}>Emitida</div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'no_emitida' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Devolver Venta">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        codigoEmitirFactura: profile.codigo,
                                                                        estadoModalCancelarVenta: true,
                                                                    })
                                                                }}>
                                                                    <CloseIcon style={{ color: '#EF5350' }} fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <Tooltip title="Emitir Factura">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'emitir_factura'
                                                                    })
                                                                    setTimeout(() => {
                                                                        this.comprobarUsuario(profile)
                                                                    }, 100)
                                                                }}>
                                                                    <InputIcon color='primary' fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'reenviar' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Emitir Factura">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'emitir_factura'
                                                                    })
                                                                    setTimeout(() => {
                                                                        this.comprobarUsuario(profile)
                                                                    }, 100)
                                                                }}>
                                                                    <InputIcon color='primary' fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <div style={{ display: 'flex', color: 'red', alignItems: 'center', fontSize: 9, wordWrap: 'break-word' }}>REENVIAR: {profile.error_factura_emitida.toUpperCase()} </div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'pendiente' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <IconButton disabled>
                                                                <CircularProgress size={20} thickness={5} style={{ color: '#42A5F5' }} />
                                                            </IconButton>
                                                            <div style={{ color: '#42A5F5', display: 'flex', alignItems: 'center' }}>Emitiendo...</div>
                                                        </div>
                                                    }
                                                    {
                                                        profile.factura_emitida === 'error' &&
                                                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                                                            <Tooltip title="Devolver Venta">
                                                                <IconButton onClick={() => {
                                                                    this.setState({
                                                                        estadoacciones: 'devolver_venta'
                                                                    })
                                                                    setTimeout(() => {
                                                                        this.setState({
                                                                            codigoEmitirFactura: profile.codigo,
                                                                            estadoModalCancelarVenta: true,
                                                                        })
                                                                    }, 100)
                                                                }}>
                                                                    <CloseIcon style={{ color: '#EF5350' }} fontSize="small" />
                                                                </IconButton>
                                                            </Tooltip>
                                                            <ErrorEstado>{profile.error_factura_emitida}
                                                            </ErrorEstado>

                                                        </div>
                                                    }
                                                </div>
                                        }</Table.TextCell>
                                        <Table.TextCell width={130} flex="none" >{
                                            profile.cliente === 'Consumidor Final' ?
                                                profile.cliente
                                                :
                                                profile.cliente.nombre
                                        }</Table.TextCell>
                                        <Table.TextCell width={156} flex="none">{<MostrarProductosVenta lista={profile.productos}>
                                            <Button >Lista de Productos</Button>
                                        </MostrarProductosVenta>}</Table.TextCell>
                                        <Table.TextCell width={85} flex="none">{profile.tipo_pago}</Table.TextCell>
                                        {/*      <Table.TextCell width={100} flex="none">{profile.subtotal}</Table.TextCell> */}
                                        <Table.TextCell width={85} flex="none">{profile.total}</Table.TextCell>
                                        <Table.TextCell width={85} flex="none">{profile.descuento}</Table.TextCell>
                                        {/*  <Table.TextCell size={5}>{profile.empleado}</Table.TextCell> */}
                                        <Table.TextCell width={175} flex="none" >{profile.fecha_venta} {profile.hora_venta}</Table.TextCell>
                                    </Table.Row>
                                ))}
                            </>
                        }
                    </Table.Body>
                </Table>
                {
                    this.state.itemFormateadoImprimir.length === 0 ?
                        <></>
                        :
                        <ContainerPlantillas>
                            <ResivoVenta
                                item={this.state.itemFormateadoImprimir}
                                ref={el => (this.refImprimirResivo = el)}
                            />
                        </ContainerPlantillas>
                }

                <ModalContainerNormal
                    open={this.state.estadoModalCancelarVenta}
                    handleClose={() => this.setState({ estadoModalCancelarVenta: false })}
                >
                    <ModalCancelarVenta
                        estado={this.state.eliminando}
                        handleClose={() => this.setState({ estadoModalCancelarVenta: false })}
                        handleCancelarVenta={() => {
                            // this.recuperarJsonFactura(this.state.codigoEmitirFactura)
                            this.updateDataProductos(this.state.codigoEmitirFactura)

                        }}
                    />
                </ModalContainerNormal>
                <ModalContainerNormal
                    open={this.state.estadoModalEmitirFactura}
                    handleClose={() => this.setState({ estadoModalEmitirFactura: false })}
                >
                    <EmitirFacturaModal
                        handleClose={() => this.setState({ estadoModalEmitirFactura: false })}
                        handleEmitir={() => {
                            this.recuperarJsonFactura(this.state.codigoEmitirFactura)
                            this.setState({ estadoModalEmitirFactura: false })
                        }}
                    />
                </ModalContainerNormal>

                {
                    this.state.imprimir === true &&
                    <GenerarPDF
                        item={this.state.item}
                        emisor={this.state.emisor}
                        claveAcceso={this.state.claveAcceso}
                        mes={this.state.mes}
                        cerrar={() => {
                            this.setState({
                                imprimir: false
                            })
                        }}
                    />

                }
                {/*  {
                    this.state.imprimirResivo === true &&
                    <GenerarResivo
                        item={this.state.itemFormateadoImprimir}
                        cerrar={() => {
                            this.setState({
                                imprimirResivo: false
                            })
                        }}
                    />

                } */}

            </div>
        );
    }
}

export default TablaVentas;