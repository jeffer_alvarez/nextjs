import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle'
import React from 'react';
import { Spinner } from 'evergreen-ui';

const ModalCancelarVenta = (props) => {
    return (
        <div>
            <DialogTitle id="alert-dialog-title">{"¿Está seguro que desea devolver esta venta?"}</DialogTitle>
            <DialogActions>
                {
                    props.estado ?
                        <div style={{ width: '20%', marginBottom: 10 }}>
                            <Spinner size={24} />
                        </div>
                        :
                        <>

                            <Button onClick={props.handleClose} color="primary" >
                                Cancelar
                </Button>
                            <Button
                                onClick={() => props.handleCancelarVenta()} color="primary" >
                                Aceptar
                </Button>
                        </>
                }
            </DialogActions>
        </div>
    );
}

export default ModalCancelarVenta