import "firebase/database";
import "firebase/auth";

import {
  Button,
  Combobox,
  Dialog,
  Switch,
  TextInput,
  TextInputField,
  toaster
} from "evergreen-ui";
import React, { Component, useEffect, useState } from "react";

import ContainerPlantillas from "../../../../utils/container_plantillas";
import KeyHooks from "./KeyHooks";
import ReactToPrint from "react-to-print";
import ResivoVenta from "../../../../utils/resivo_venta";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";
import useKey from 'use-key-hook';

class MedioPago extends Component {

  state = {
    fechaEmision: '',
    dineroRecibido: 0.00,
    descuento: 0.00,
    totalDescontado: 0.00,
    vuelto: 0.00,
    numeroDocumento: '',
    dateFactura: '',
    imprimirRecibo: true,
    lista: [],
    estadoKeyDown: false,
    isShown: false,
    tipoPago: 'efectivo',
    fecha: `${new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()}`,
    valorAcreditado: 0,
    clienteSeleccionado: "",
    observacion: "",
    dinero_resibido: "",
    sumaTotal: "",
    tipo_venta: "",
    sumaSubTotal: "",
    descuento: "",
    cambio: "",
    tipo_pago: "",
    estadoVenta:false
  }


  componentDidMount() {


    this.setState({
      isShown: this.props.isShown
    })
    this.diasPlazo()
    setTimeout(() => {
      var fecha = funtions.obtenerFechaActual();
      var hora = funtions.obtenerHoraActual();
      this.setState({
        fechaEmision: fecha + "  " + hora,
        dineroRecibido: this.props.total,
      })
    }, 100);


  }

  diasPlazo = () => {
    var uid = firebase.auth().currentUser.uid
    var db = firebase.database();
    var empresaRef = db.ref('users/' + uid + "/configuracion/dias_a_pagar_defecto")
    empresaRef.on('value', (snap) => {
      if (snap.val()) {
        Date.prototype.addDays = function (days) {
          var date = new Date(this.valueOf())
          date.setDate(date.getDate() + days)
          var dayDate = date.getDate()
          var mes = date.getMonth() + 1
          if (dayDate.toString().length === 1) {
            dayDate = '0' + dayDate
          }
          if (mes.toString().length === 1) {
            mes = '0' + mes
          }
          return `${date.getFullYear()}-${mes}-${dayDate}`;
        }
        var date = new Date();
        this.setState({
          fecha: date.addDays(Number(snap.val().dias))
        })
      }
    })
  }

  componentWillReceiveProps(props) {
    if (props.facturar) {
      event.preventDefault();
      this.enviarToPlantillaData()
      setTimeout(() => {
        this.handleFianlizarVenta()
      }, 2000);
      props.cancelar()
    } else if (props.imprimir === true) {
      event.preventDefault();
      props.cancelar()
      this.setState({
        imprimirRecibo: !this.state.imprimirRecibo
      })
      var item = []
      this.enviarToPlantillaData()
      setTimeout(() => {
        this.handleFianlizarVenta()
      }, 2000);
    }
    document.getElementById("TextInputField-id_dinero_recibido").focus()
    document.getElementById("TextInputField-id_dinero_recibido").select()
  }



  enviarToPlantillaData = () => {
    const itemFormat = {
      numero_venta: this.props.lista[0].documento,
      tipo_venta: this.props.tipoVenta,
      productos: this.props.listaVentas,
      subtotal: this.props.sumaSubtotal,
      iva: this.props.iva,
      total: this.props.total,
      descuento: this.props.descuento,
      fecha_venta: funtions.obtenerFechaActual(),
      hora_venta: funtions.obtenerHoraActual(),
      tipo_pago: 'efectivo',
      valor_acreditado: 0,
      fecha_a_pagar: 0,
      numero_tarjeta: '',
      nombre_banco: '',
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        var db = firebase.database();
        if (this.props.tipoVenta === 'factura') {
          var clienteRef = db.ref('users/' + user.uid + '/clientes/' + this.props.cliente.identificacion);
          var empresaRef = db.ref('auth_admins/' + user.uid)
          clienteRef.once('value', (snapshot) => {
            if (snapshot.val()) {
              itemFormat.nombreCliente = snapshot.val().nombre
              itemFormat.emailCliente = snapshot.val().email
              itemFormat.identificacionCliente = snapshot.val().identificacion
              itemFormat.direccionCliente = snapshot.val().direccion

              empresaRef.once('value', (snap) => {
                if (snap.val()) {
                  itemFormat.nombreEmpresa = snap.val().nombre_comercial
                  itemFormat.razon_social = snap.val().razon_social
                  itemFormat.direccion = snap.val().direccion
                  this.setState({
                    itemFormateadoImprimir: itemFormat
                  })
                  this.refEventoImprimir.handlePrint
                }
              })
            }
          })
        } else {
          var empresaRef = db.ref('auth_admins/' + user.uid)
          empresaRef.once('value', (snap) => {
            if (snap.val()) {
              itemFormat.nombreEmpresa = snap.val().nombre_comercial
              itemFormat.razon_social = snap.val().razon_social
              itemFormat.direccion = snap.val().direccion
              this.setState({
                itemFormateadoImprimir: itemFormat
              })
              setTimeout(() => {
                this.refEventoImprimir.handlePrint()
              }, 100);
            }
          })
        }
      }
    })

  }

  handleFianlizarVenta = () => {
    var codigoRegistroVenta = funtions.guidGenerator()
    var userUid = firebase.auth().currentUser.uid
    var item = this.state.tipoPago.toLowerCase()
    var jsonData = {}
    /* console.log(this.props.listaVentas)
    console.log("Suma subtotal: " + this.props.sumaSubtotal)
    console.log("Suma sin iva" + this.props.sumaSinIva)
    console.log("Suma con iva:" + this.props.sumaConIva)
    console.log("Iva: " + this.props.iva)
    jsonData = this.createJsonFacturaElectronicaEfectivo() */
      switch (item) {
       case 'efectivo': {
 
         this.updateDataProductos()
         this.sumarNumeroFactura()
         this.postSetVentasDiariasReportes(userUid, this.props.total)
         this.postSetProductosMasVendidosReportes(userUid)
         this.setOperacionStockEfectivo(this.props.listaVentas)
         this.setSaveRegistroVentaEfectivo(codigoRegistroVenta, item)
         if (this.props.cliente.identificacion) {
 
           this.guardarDatosDelCliente(this.props.cliente)
           jsonData = this.createJsonFacturaElectronicaEfectivo()
           this.saveFacturasJson(jsonData, codigoRegistroVenta)
         } else {
           jsonData = this.createJsonFacturaElectronicaConsumidorFinal()
           this.saveFacturasJson(jsonData, codigoRegistroVenta)
         }
         this.setState({
           estadoVenta:false
         })
         //   this.enviarFacturaElectronica(codigoRegistroVenta, userUid, item)
         break
       }
       case 'credito': {
         this.updateDataProductos()
         this.sumarNumeroFactura()
         this.postSetVentasDiariasReportes(userUid, this.state.valorAcreditado)
         this.setOperacionStockCredito(this.props.listaVentas, this.state.valorAcreditado)
         this.postSetProductosMasVendidosReportes(userUid)
         this.setSaveRegistroVentaCredito(codigoRegistroVenta, item)
         jsonData = this.createJsonFacturaElectronicaCredito()
         this.saveFacturasJson(jsonData, codigoRegistroVenta)
         this.setState({
          estadoVenta:false
        })
         // this.enviarFacturaElectronica(codigoRegistroVenta, userUid, item)
         break
       } default: {
         break
       }
     }
 
     setTimeout(() => {
       this.props.cerrar()
     }, 500)
 
     toaster.notify('Venta guardada') 

  }

  setOperacionStockEfectivo = (listaProductos) => {
    const { clienteSeleccionado, observacion, dinero_resibido, sumaTotal, tipo_venta, sumaSubTotal, descuento, cambio, tipo_pago } = this.state
    var codigoStock = funtions.guidGenerator()
    var order = new Date()
    var db = firebase.database();
    var fechaFIrestore = funtions.obtenerFechaActualDB();
    var anio = fechaFIrestore.substr(0, 4)
    var mesnombre = fechaFIrestore.substr(5, 2)
    var dia = fechaFIrestore.substr(8, 2)
    //var operacionStockRef = db.ref('users/' + firebase.auth().currentUser.uid + '/operaciones_stock/' + codigoStock);
    var firestoreDB = firebase.firestore()
    var stockSva = firestoreDB.collection(firebase.auth().currentUser.uid).doc("operaciones_stock").collection(anio).doc(mesnombre).collection(dia).doc(codigoStock)
    var saveStock = stockSva.set({
      codigo: codigoStock,
      tipo_operacion: 'venta-producto',
      fecha: funtions.obtenerFechaActualDB(),
      hora: `${new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()}`,
      cliente_proveedor: this.props.tipoVenta === 'final' ? 'Consumidor Final' : this.props.cliente,
      productos: listaProductos,
      total_final: `${Number(this.props.total).toFixed(2)}`,
      empleado: this.props.usuario.code,
      observacion: observacion,
      subtotal: `${Number(this.props.sumaSubtotal).toFixed(2)}`,
      descuento: `${Number(descuento).toFixed(2)}`,
      otros_gastos: '0.00',
      flete: '0.00',
      valor_pagado: `${Number(this.props.total).toFixed(2)}`,
      medio_pago: "efectivo",
      saldo_favor: '0.00',
      en_deuda: '0.00',
      vuelto: cambio,
      acreditado: '0.00',
      order: order + "",
      caja: this.props.caja[0].codigo
    }).then(() => {
      console.log("se escribio en la base de datos")
    })
    //  operacionStockRef.set
  }

  setOperacionStockCredito = (listaProductos, valor_acreditado) => {
    const { clienteSeleccionado, observacion, dinero_resibido, sumaTotal, tipo_venta, sumaSubTotal, descuento, cambio, tipo_pago } = this.state
    const { cajaSeleccionada } = this.props

    var codigoStock = funtions.guidGenerator()
    var order = new Date()
    var db = firebase.database();
    var fechaFIrestore = funtions.obtenerFechaActualDB();
    var anio = fechaFIrestore.substr(0, 4)
    var mesnombre = fechaFIrestore.substr(5, 2)
    var dia = fechaFIrestore.substr(8, 2)
    //var operacionStockRef = db.ref('users/' + firebase.auth().currentUser.uid + '/operaciones_stock/' + codigoStock);
    var firestoreDB = firebase.firestore()
    var stockSva = firestoreDB.collection(firebase.auth().currentUser.uid).doc("operaciones_stock").collection(anio).doc(mesnombre).collection(dia).doc(codigoStock)
    var saveStock = stockSva.set({
      codigo: codigoStock,
      tipo_operacion: 'venta-producto',
      fecha: funtions.obtenerFechaActualDB(),
      hora: `${new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()}`,
      cliente_proveedor: this.props.tipoVenta === 'final' ? 'Consumidor Final' : this.props.cliente,
      productos: listaProductos,
      total_final: `${Number(this.props.total).toFixed(2)}`,
      empleado: this.props.usuario.code,
      observacion: observacion,
      subtotal: `${Number(this.props.sumaSubtotal).toFixed(2)}`,
      descuento: `${Number(descuento).toFixed(2)}`,
      otros_gastos: '0.00',
      flete: '0.00',
      valor_pagado: '0.00',
      medio_pago: "credito",
      saldo_favor: '0.00',
      en_deuda: Number(Number(this.props.total) - Number(valor_acreditado)).toFixed(2),
      vuelto: '0.00',
      acreditado: `${Number(valor_acreditado).toFixed(2)}`,
      order: order + "",
      caja: this.props.caja[0].codigo
    })
  }

  guardarDatosDelCliente = (cliente) => {
    this.props.firebaseClientesConection.collection("clientes").doc(cliente.identificacion).update({
      email: cliente.email,
      celular: cliente.celular,
      ciudad: cliente.ciudad,
      direccion: cliente.direccion
    }).then(function () {
      console.log("Document successfully written!");
    })
      .catch(function (error) {
        console.error("Error writing document: ", error);
      })
  }

  updateDataProductos = () => {
    const { listaVentas } = this.props;
    listaVentas.forEach(item => {
      var db = firebase.database();
      var productosRef = db.ref(
        "users/" + firebase.auth().currentUser.uid + "/productos/" + item.codigo
      );
      productosRef.once("value", snap => {
        if (snap.val()) {
          productosRef.update({
            stock_actual:
              Number(snap.val().stock_actual) - Number(item.cantidad)
          });
        }
      });
    });
  };

  setSaveRegistroVentaEfectivo = (codigoVenta, itemFacturas) => {
    var db = firebase.database();
    var operacionVentaRef = db.ref(
      "users/" + firebase.auth().currentUser.uid + "/ventas/" + codigoVenta
    );
    var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/' + this.props.caja[0].codigo + '/' + codigoVenta);
    var order = new Date();

    var itemVenta = {
      codigo: codigoVenta,
      numero_factura: this.props.lista[0].documento.substr(10, 19),
      cliente:
        this.props.cliente.length === 0 ? "Consumidor Final" : this.props.cliente,
      descuento: this.state.descuento,
      tipo_venta: this.props.cliente.length === 0 ? "final" : 'factura',
      factura_emitida: "no_emitida",
      observacion: '',
      dinero_resibido: Number(this.state.dineroRecibido).toFixed(6),
      cambio: Number(this.state.vuelto).toFixed(6),
      subtotal: this.props.lista[0].item.sumaSubtotal,
      iva: Number(this.props.lista[0].item.totalIva).toFixed(6),
      total: Number(this.state.totalDescontado) > 0 ? this.state.totalDescontado : Number(this.props.lista[0].item.total).toFixed(6),
      productos: this.props.listaVentas,
      fecha_venta: funtions.obtenerFechaActualDB(),
      hora_venta: `${new Date().getHours() +
        ":" +
        new Date().getMinutes() +
        ":" +
        new Date().getSeconds()}`,
      empleado: this.props.usuario.code,
      order: "" + order,
      estado: true,
      numero_tarjeta: "",
      nombre_banco: "",
      tipo_pago: 'efectivo',
      valor_acreditado: "0.00",
      fecha_a_pagar: "",
      caja: this.props.caja[0].codigo,
      urlpdf: ""
    };
    var tipo_pago = itemFacturas
    this.setVentaCaja(itemVenta, tipo_pago);
    operacionVentaRef.set(itemVenta);
    operacionVentaCajaRef.set(itemVenta)
  };

  setSaveRegistroVentaCredito = (codigoVenta, item) => {
    var db = firebase.database();
    var operacionVentaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas/' + codigoVenta);
    var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/' + this.props.caja[0].codigo + '/' + codigoVenta);
    var order = new Date()

    var itemVenta = {
      codigo: codigoVenta,
      numero_factura: this.props.lista[0].documento.substr(10, 19),
      cliente: this.props.cliente.length === 0 ? "Consumidor Final" : this.props.cliente,
      descuento: '0.00',
      tipo_venta: this.props.cliente.length === 0 ? "final" : 'factura',
      factura_emitida: 'no_emitida',
      observacion: '',
      dinero_resibido: Number(this.state.valorAcreditado).toFixed(6),
      cambio: '0.00',
      subtotal: Number(this.props.lista[0].item.sumaSubtotal).toFixed(6),
      iva: Number(this.props.lista[0].item.totalIva).toFixed(6),
      total: Number(this.props.lista[0].item.total).toFixed(6),
      productos: this.props.listaVentas,
      fecha_venta: funtions.obtenerFechaActualDB(),
      hora_venta: `${new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds()}`,
      empleado: this.props.usuario.code,
      order: '' + order,
      estado: true,
      numero_tarjeta: '',
      nombre_banco: '',
      tipo_pago: item,
      valor_acreditado: Number(this.state.valorAcreditado).toFixed(6),
      fecha_a_pagar: this.state.fecha,
      caja: this.props.caja[0].codigo,
      urlpdf: '',
    }
    this.setVentaCaja(itemVenta, item)
    operacionVentaRef.set(itemVenta)
    operacionVentaCajaRef.set(itemVenta)
  }


  sumarNumeroFactura = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        var db = firebase.database();
        var numeroFactura = db.ref('users/' + user.uid + "/configuracion")
        numeroFactura.once('value', (snap) => {
          if (snap.val()) {
            const numero_factura = snap.val().numero_factura
            const suma = Number(numero_factura) + 1
            const tamaño = String(suma).length
            const restaTamaño = 9 - Number(tamaño)
            var cadenaFinal = ''
            for (var i = 0; i < restaTamaño; i++) {
              cadenaFinal = cadenaFinal + '0'
            }
            numeroFactura.update({
              numero_factura: `${cadenaFinal}${suma}`
            })
          }
        })
      }
    })
  }
  // crear factura json verificado
  createJsonFacturaElectronicaEfectivo = () => {

    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();

    mm = addZero(mm)
    dd = addZero(dd)
    function addZero(i) {
      if (i < 10) {
        i = '0' + i;
      }
      return i;
    }
    var fecha = `${dd}/${mm}/${yyyy}`
    var json = {
      "ambiente": this.props.ambiente,
      "tipo_emision": 1,
      "secuencial": `${this.props.lista[0].documento.substr(10, 19)}`,
      "fecha_emision": fecha,
      "emisor": {
        "ruc": "",
        "obligado_contabilidad": false,
        "contribuyente_especial": "",
        "nombre_comercial": "",
        "razon_social": "",
        "direccion": "",
        "establecimiento": {
          "punto_emision": String(this.props.puntoEmision),
          "codigo": String(this.props.codigoEstablecimiento),
          "direccion": ""
        }
      },
      "moneda": "USD",
      "totales": {
        "total_sin_impuestos": Number(Number(this.props.sumaSubtotal).toFixed(3)),
        "impuestos": [
          {
            "base_imponible": Number(this.props.sumaSinIva),
            "valor": 0.0,
            "codigo": "2",
            "codigo_porcentaje": "0"
          },
          {
            "base_imponible": Number(Number(this.props.sumaConIva).toFixed(3)),
            "valor": Number(this.props.iva),
            "codigo": "2",
            "codigo_porcentaje": "2"
          }
        ],
        "importe_total": Number((Number(this.props.total) - Number(this.state.descuento)).toFixed(3)),
        "propina": 0.0,
        "descuento": Number(this.state.descuento).toFixed(3)
      },
      "comprador": {
        "email": this.props.cliente.email,
        "identificacion": this.props.cliente.identificacion,
        "tipo_identificacion": this.props.cliente.identificacion.length === 10 ? '05' : this.props.cliente.identificacion.length === 13 ? '04' : '',
        "razon_social": this.props.cliente.nombre,
        "direccion": this.props.cliente.direccion,
        "telefono": this.props.cliente.celular
      },
      "items": this.props.listaVentas.map(item => {
        return {
          cantidad: Number(item.cantidad),
          codigo_principal: item.codigo_barras.length > 0 ? item.codigo_barras : '0',
          codigo_auxiliar: item.codigo,
          precio_unitario: Number(item.precio_venta),
          descripcion: Boolean(item.tiene_iva) ? '* ' + item.descripcion_producto : item.descripcion_producto,
          precio_total_sin_impuestos: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
          impuestos: [
            {
              base_imponible: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
              valor: Boolean(item.tiene_iva) ? Number(Number(Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) * 0.12).toFixed(3)) / 1.12) : 0,
              tarifa: Boolean(item.tiene_iva) ? Number(item.porcentaje_iva) : 0,
              codigo: '2',
              codigo_porcentaje: Boolean(item.tiene_iva) ? '2' : '0'
            }
          ],
          descuento: 0.0
        }
      })
      ,
      "valor_retenido_iva": 0.00,
      "valor_retenido_renta": 0.00,
      "pagos": [
        {
          "medio": "efectivo",
          "total": Number(this.props.total)
        }
      ]
    }


    return json
  }
  createJsonFacturaElectronicaConsumidorFinal = () => {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();

    mm = addZero(mm)
    dd = addZero(dd)
    function addZero(i) {
      if (i < 10) {
        i = '0' + i;
      }
      return i;
    }
    var fecha = `${dd}/${mm}/${yyyy}`
    var json = {
      "ambiente": this.props.ambiente,
      "tipo_emision": 1,
      "secuencial": `${this.props.lista[0].documento.substr(10, 19)}`,
      "fecha_emision": fecha,
      "emisor": {
        "ruc": "",
        "obligado_contabilidad": false,
        "contribuyente_especial": "",
        "nombre_comercial": "",
        "razon_social": "",
        "direccion": "",
        "establecimiento": {
          "punto_emision": String(this.props.puntoEmision),
          "codigo": String(this.props.codigoEstablecimiento),
          "direccion": ""
        }
      },
      "moneda": "USD",
      "totales": {
        "total_sin_impuestos": Number(Number(this.props.sumaSubtotal).toFixed(3)),
        "impuestos": [
          {
            "base_imponible": Number(this.props.sumaSinIva),
            "valor": 0.0,
            "codigo": "2",
            "codigo_porcentaje": "0"
          },
          {
            "base_imponible": Number(Number(this.props.sumaConIva).toFixed(3)),
            "valor": Number(this.props.iva),
            "codigo": "2",
            "codigo_porcentaje": "2"
          }
        ],
        "importe_total": Number((Number(this.props.total) - Number(this.state.descuento)).toFixed(3)),
        "propina": 0.0,
        "descuento": Number(this.state.descuento).toFixed(3)
      },
      "comprador": {
        "identificacion": "9999999999999",
        "tipo_identificacion": '07',
        "razon_social": "Consumidor Final",
      },
      "items": this.props.listaVentas.map(item => {
        return {
          cantidad: Number(item.cantidad),
          codigo_principal: item.codigo_barras.length > 0 ? item.codigo_barras : '0',
          codigo_auxiliar: item.codigo,
          descripcion: Boolean(item.tiene_iva) ? '* ' + item.descripcion_producto : item.descripcion_producto,
          precio_unitario: Number(item.precio_venta),
          precio_total_sin_impuestos: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
          impuestos: [
            {
              base_imponible: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
              valor: Boolean(item.tiene_iva) ? Number(Number(Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) * 0.12).toFixed(3)) / 1.12) : 0,
              tarifa: Boolean(item.tiene_iva) ? Number(item.porcentaje_iva) : 0,
              codigo: '2',
              codigo_porcentaje: Boolean(item.tiene_iva) ? '2' : '0'
            }
          ],
          descuento: 0.0
        }
      })
      ,
      "valor_retenido_iva": 0.00,
      "valor_retenido_renta": 0.00,
      "pagos": [
        {
          "medio": "efectivo",
          "total": Number(this.props.total)
        }
      ]
    }
    return json
  }

  createJsonFacturaElectronicaCredito = () => {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1;
    var yyyy = hoy.getFullYear();

    mm = addZero(mm)
    dd = addZero(dd)
    function addZero(i) {
      if (i < 10) {
        i = '0' + i;
      }
      return i;
    }
    var fecha = `${dd}/${mm}/${yyyy}`
    var json = {
      "ambiente": this.props.ambiente,
      "tipo_emision": 1,
      "secuencial": `${this.props.lista[0].documento.substr(10, 19)}`,
      "fecha_emision": fecha,
      "emisor": {
        "ruc": "",
        "obligado_contabilidad": false,
        "contribuyente_especial": "",
        "nombre_comercial": "",
        "razon_social": "",
        "direccion": "",
        "establecimiento": {
          "punto_emision": String(this.props.puntoEmision),
          "codigo": String(this.props.codigoEstablecimiento),
          "direccion": ""
        }
      },
      "moneda": "USD",
      "totales": {
        "total_sin_impuestos": Number(Number(this.props.sumaSubtotal).toFixed(3)),
        "impuestos": [
          {
            "base_imponible": Number(this.props.sumaSinIva),
            "valor": 0.0,
            "codigo": "2",
            "codigo_porcentaje": "0"
          },
          {
            "base_imponible": Number(Number(this.props.sumaConIva).toFixed(3)),
            "valor": Number(this.props.iva),
            "codigo": "2",
            "codigo_porcentaje": "2"
          }
        ],
        "importe_total": Number((Number(this.props.total) - Number(this.state.descuento)).toFixed(3)),
        "propina": 0.0,
        "descuento": Number(this.state.descuento).toFixed(3)
      },
      "comprador": {
        "email": this.props.cliente.email,
        "identificacion": this.props.cliente.identificacion,
        "tipo_identificacion": this.props.cliente.identificacion.length === 10 ? '05' : this.props.cliente.identificacion.length === 13 ? '04' : '',
        "razon_social": this.props.cliente.nombre,
        "direccion": this.props.cliente.direccion,
        "telefono": this.props.cliente.celular
      },
      "items": this.props.listaVentas.map(item => {
        return {
          cantidad: Number(item.cantidad),
          codigo_principal: item.codigo_barras.length > 0 ? item.codigo_barras : '0',
          codigo_auxiliar: item.codigo,
          descripcion: Boolean(item.tiene_iva) ? '* ' + item.descripcion_producto : item.descripcion_producto,
          precio_unitario: Number(item.precio_venta),
          precio_total_sin_impuestos: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
          impuestos: [
            {
              base_imponible: Boolean(item.tiene_iva) ? Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) / 1.12).toFixed(3) : Number(Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)).toFixed(3),
              valor: Boolean(item.tiene_iva) ? Number(Number(Number((Number(Number(item.precio_venta).toFixed(3)) * Number(item.cantidad)) * 0.12).toFixed(3)) / 1.12) : 0,
              tarifa: Boolean(item.tiene_iva) ? Number(item.porcentaje_iva) : 0,
              codigo: '2',
              codigo_porcentaje: Boolean(item.tiene_iva) ? '2' : '0'
            }
          ],
          descuento: 0.0
        }
      })
      ,
      "valor_retenido_iva": 0.00,
      "valor_retenido_renta": 0.00,
      "credito": {
        "fecha_vencimiento": `${this.state.fecha}`,
        "monto": Number((Number(this.props.total) - Number(this.state.valorAcreditado)).toFixed(2))
      },
      "pagos": [
        {
          "medio": 'efectivo',
          "total": Number(this.state.valorAcreditado),
        }
      ]
    }

    return json
  }
  saveFacturasJson = (jsonData, codigoRegistroVenta) => {
    var db = firebase.database();
    var operacionFacturaJson = db.ref('users/' + firebase.auth().currentUser.uid + '/facturas_ventas/' + codigoRegistroVenta);

    operacionFacturaJson.set(jsonData)
  }

  postSetGeneratePdf = async (uidUser, jsonData, codigoRegistroVenta) => {
    const rawResponse = await fetch('https://stormy-bayou-19844.herokuapp.com/facturaPdf', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'id': uidUser,
        'codigo': codigoRegistroVenta,
      },
      body: JSON.stringify(jsonData)
    })
  }

  postSetVentasDiariasReportes = async (uidUser, valor) => {
    console.log(valor)
    const rawResponse = await fetch('https://server-reportes.herokuapp.com/save_ventas_diarias', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'uid': uidUser,
        fecha: funtions.obtenerFechaActualReportes(),
      },
      body: JSON.stringify({
        "valor_total": valor
      })
    }).then(function (response) {
      return response.json();
    })
      .then(function (myJson) {
        console.log(myJson);
      });


  }
  postSetCuentasPorCobrarReportes = async (uidUser, estado, valor) => {
    const rawResponse = await fetch('https://server-reportes.herokuapp.com/cuentas_por_cobrar_diario', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'uid': uidUser,
        fecha: funtions.obtenerFechaActualReportes(),
      },
      body: JSON.stringify({
        estado: estado,
        valor: valor
      })
    })
  }


  postSetProductosMasVendidosReportes = async (uidUser) => {
    const rawResponse = await fetch('https://server-reportes.herokuapp.com/productos_mas_vendidos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'uid': uidUser,
        fecha: funtions.obtenerFechaActualReportes(),
      },
      body: JSON.stringify(this.props.listaVentas)
    })
  }



  postSet = async (uidUser, jsonData, codigo) => {
    const rawResponse = await fetch('https://stormy-bayou-19844.herokuapp.com/generarfactura', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'id': uidUser,
        'codigo': codigo,
      },
      body: JSON.stringify(jsonData)
    })
  }

  setVentaCaja = (itemVenta, tipo_pago) => {
    var db = firebase.database();
    var codigoVentaCaja = funtions.guidGenerator();

    var operacionVentaRefCaja = db.ref(
      "users/" +
      firebase.auth().currentUser.uid +
      "/caja/cajas_abiertas_usuario"
    );
    operacionVentaRefCaja.on("value", snap => {
      if (snap.val()) {
        var loja=funtions.snapshotToArray(snap)
        console.log(loja)
        console.log(this.props.usuario.code)
        var caja = funtions
          .snapshotToArray(snap)[0]
         
        console.log(caja)
        if (Boolean(caja.estado)) {
          var operacionVentaCaja = db.ref(
            "users/" +
            firebase.auth().currentUser.uid +
            "/caja/cajas_normales/" +
            caja.codigo +
            "/ventas/" +
            itemVenta.codigo
          );
          var cajaRefValorActual = db.ref(
            "users/" +
            firebase.auth().currentUser.uid +
            "/caja/cajas_normales/" +
            caja.codigo
          );
          if (tipo_pago === "efectivo") {
            cajaRefValorActual.once("value", snap2 => {
              if (snap2.val()) {
                operacionVentaCaja.set(itemVenta);
                cajaRefValorActual.update({
                  valor_caja: Number(
                    Number(snap2.val().valor_caja) + Number(itemVenta.total)
                  ).toFixed(2)
                });
              }
            });
          }
          if (tipo_pago === "credito") {
            var cuentaCobrarClienteRef = db.ref(
              "users/" +
              firebase.auth().currentUser.uid +
              "/cuentas_por_cobrar/cuentas_por_cobrar_basicas/" +
              this.props.cliente.identificacion
            );
            var configuracionMes = db.ref(
              "users/" +
              firebase.auth().currentUser.uid +
              "/configuracion/dias_a_pagar_defecto/dias"
            );
            cuentaCobrarClienteRef.once("value", snap => {
              if (snap.val()) {
                var totalac = Number(this.props.lista[0].item.total) - this.state.valorAcreditado
                this.postSetCuentasPorCobrarReportes(firebase.auth().currentUser.uid, "existe", totalac)
                var aumentarDeudaRef = db.ref(
                  "users/" +
                  firebase.auth().currentUser.uid +
                  "/cuentas_por_cobrar/cuentas_por_cobrar_basicas/" +
                  this.props.cliente.identificacion +
                  "/lista_deudas/" +
                  itemVenta.codigo
                );
                var aumentarAcreditadoRef = db.ref(
                  "users/" +
                  firebase.auth().currentUser.uid +
                  "/cuentas_por_cobrar/cuentas_por_cobrar_basicas/" +
                  this.props.cliente.identificacion +
                  "/lista_acreditados/" +
                  itemVenta.codigo
                );
                aumentarDeudaRef.set({
                  codigo: itemVenta.codigo,
                  valor: Number(this.props.lista[0].item.total).toFixed(6),
                  fecha_registro: funtions.obtenerFechaActual(),
                  hora_registro: funtions.obtenerHoraActual(),
                  estado: true
                });
                var cajaRefValorAcreditado = db.ref(
                  "users/" +
                  firebase.auth().currentUser.uid +
                  "/caja/cajas_normales/" +
                  caja.codigo +
                  "/lista_dinero_acreditado_venta_credito/" +
                  itemVenta.codigo
                );

                if (Number(this.state.valorAcreditado) > 0) {
                  cajaRefValorAcreditado.set({
                    codigo: itemVenta.codigo,
                    valor: Number(this.state.valorAcreditado).toFixed(6),
                    fecha_registro: funtions.obtenerFechaActual(),
                    hora_registro: funtions.obtenerHoraActual(),
                    estado: true,
                    tipo: "pago_venta_credito"
                  });
                  aumentarAcreditadoRef.set({
                    codigo: itemVenta.codigo,
                    valor: Number(this.state.valorAcreditado).toFixed(6),
                    fecha_registro: funtions.obtenerFechaActual(),
                    hora_registro: funtions.obtenerHoraActual(),
                    estado: true,
                    tipo: "pago_venta_credito"
                  });
                  cajaRefValorActual.once("value", snap2 => {
                    if (snap2.val()) {
                      cajaRefValorActual.update({
                        valor_caja: Number(
                          Number(snap2.val().valor_caja) +
                          Number(this.state.valorAcreditado)
                        ).toFixed(2)
                      });
                    }
                  });
                }
              } else {
                var totala = Number(this.props.lista[0].item.total) - this.state.valorAcreditado
                this.postSetCuentasPorCobrarReportes(firebase.auth().currentUser.uid, "no_existe", totala)
                Date.prototype.addDays = function (days) {
                  var date = new Date(this.valueOf());
                  date.setDate(date.getDate() + days);
                  var dayDate = date.getDate();
                  var mes = date.getMonth() + 1;
                  if (dayDate.toString().length === 1) {
                    dayDate = "0" + dayDate;
                  }
                  if (mes.toString().length === 1) {
                    mes = "0" + mes;
                  }
                  return `${date.getFullYear()}-${mes}-${dayDate}`;
                };
                var date = new Date();
                configuracionMes.once("value", snapp => {
                  if (snapp.val()) {
                    cuentaCobrarClienteRef.set({
                      cliente: this.props.cliente,
                      codigo: this.props.cliente.identificacion,
                      estado: true,
                      estado_cuenta: "deuda",
                      fecha_registro: funtions.obtenerFechaActual(),
                      fecha_pago: date.addDays(Number(snap.val())),
                      hora_registro: funtions.obtenerHoraActual(),
                      order: "" + new Date(),
                      tipo_cuenta: "venta_credito",
                      total: Number(this.props.lista[0].item.total).toFixed(6),
                      usuario: this.props.usuario.code
                    });

                    var deudaRef = db.ref(
                      "users/" +
                      firebase.auth().currentUser.uid +
                      "/cuentas_por_cobrar/cuentas_por_cobrar_basicas/" +
                      this.props.cliente.identificacion +
                      "/lista_deudas/" +
                      itemVenta.codigo
                    );
                    deudaRef.set({
                      codigo: itemVenta.codigo,
                      valor: Number(this.props.lista[0].item.total).toFixed(6),
                      fecha_registro: funtions.obtenerFechaActual(),
                      hora_registro: funtions.obtenerHoraActual(),
                      estado: true,
                      tipo: "pago_venta_credito"
                    });

                    var cajaRefValorAcreditado = db.ref(
                      "users/" +
                      firebase.auth().currentUser.uid +
                      "/caja/cajas_normales/" +
                      caja.codigo +
                      "/lista_dinero_acreditado_venta_credito/" +
                      itemVenta.codigo
                    );
                    var aumentarAcreditadoRef = db.ref(
                      "users/" +
                      firebase.auth().currentUser.uid +
                      "/cuentas_por_cobrar/cuentas_por_cobrar_basicas/" +
                      this.props.cliente.identificacion +
                      "/lista_acreditados/" +
                      itemVenta.codigo
                    );

                    if (Number(this.state.valorAcreditado) > 0) {
                      cajaRefValorAcreditado.set({
                        codigo: itemVenta.codigo,
                        valor: this.state.valorAcreditado,
                        fecha_registro: funtions.obtenerFechaActual(),
                        hora_registro: funtions.obtenerHoraActual(),
                        estado: true,
                        tipo: "pago_venta_credito"
                      });
                      aumentarAcreditadoRef.set({
                        codigo: itemVenta.codigo,
                        valor: this.state.valorAcreditado,
                        fecha_registro: funtions.obtenerFechaActual(),
                        hora_registro: funtions.obtenerHoraActual(),
                        estado: true,
                        tipo: "pago_venta_credito"
                      });
                      cajaRefValorActual.once("value", snap2 => {
                        if (snap2.val()) {
                          cajaRefValorActual.update({
                            valor_caja: Number(
                              Number(snap2.val().valor_caja) +
                              Number(this.state.valorAcreditado)
                            ).toFixed(2)
                          });
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        }
      }
    });
  };



  imprimir = () => {
    setTimeout(() => {
      var bot = document.getElementsByClassName('action-button')
    }, 2000);
  }

  validarFecha = (e) => {
    if (e.target.value.length <= 10) {
      if (e.target.value.length === 4) {
        this.setState({
          fecha: e.target.value + '-'
        })
      } else if (e.target.value.length === 7) {
        this.setState({
          fecha: e.target.value + '-'
        })
      } else {
        this.setState({
          fecha: e.target.value
        })
      }
    }
    if (e.target.value.length === 10) {

    }



  }

  guardarVenta = () => {
    event.preventDefault();
    this.setState({
      estadoVenta:true   
    })
    this.enviarToPlantillaData()
    setTimeout(() => {
      this.handleFianlizarVenta()
    }, 2000);
    this.props.cancelar()


  }

  invertir() {
    var ano = this.state.fecha.substr(6, 4)
    var mes = this.state.fecha.substr(3, 2)
    var dia = this.state.fecha.substr(0, 2)
    this.setState({
      fecha: ano + "-" + mes + "-" + dia
    })

  }

  render() {
    return (
      <Dialog
        isShown={this.state.isShown}
        shouldCloseOnEscapePress={false}
        onCloseComplete={() => this.props.cerrar()}
        shouldCloseOnOverlayClick={false}
        hasFooter={false}
        hasHeader={false}
      >
        <div id='id_medio_pago'>
          <div style={{ width: "100%", display: 'flex', flexDirection: 'row' }}>
            <div style={{ display: 'flex', flexDirection: 'column', width: 'max-content' }}>
              <span style={{ float: "left", color: 'white', fontFamily: `Roboto,sans-serif`, backgroundColor: '#425a70', padding: '3px 15px ', borderRadius: 25, }}>
                {this.props.cliente.length === 0 ?
                  <span>Consumidor final</span>
                  :
                  <span >
                    {this.props.cliente.identificacion} - {this.props.cliente.nombre}
                  </span>
                }
              </span>
              <span>{this.props.lista[0].documento}</span>
            </div>
            <div style={{ flex: 1 }}></div>
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end', width: 'max-content' }}>
              <span style={{ float: "right", fontFamily: `Roboto,sans-serif` }}>
                Total $ <span style={{
                  fontSize: 20,
                  fontWeight: 600
                }}>
                  {Number(this.props.total).toFixed(2)}</span>
              </span>
              <span style={{
                float: "right",
                fontFamily: `Roboto,sans-serif`,
                color: this.state.totalDescontado < 0 ? 'red' : this.state.totalDescontado > 0 ? 'orange' : 'black'
              }}>
                Total con descuento $ {Number(this.state.totalDescontado).toFixed(2)}
              </span>
            </div>
          </div>
          <hr />
          <div
            style={{
              width: "100%",
              fontFamily: `Roboto,sans-serif`,
              display: "flex",
              flexDirection: "column"
            }}
          >
            <div>
              <span> Fecha emision </span>
              <TextInput value={this.state.fechaEmision} width={200} />
            </div>
            <br />
            <div style={{ width: "100%" }}>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <div style={{ width: 175 }}>
                  <label
                    style={{
                      color: "#333",
                      fontWeight: 500,
                      fontSize: "14px",
                      marginBottom: 0
                    }}
                  >
                    Forma pago
              </label>
                  <Combobox
                    width={175}
                    openOnFocus
                    defaultSelectedItem={"Efectivo"}
                    items={[this.props.cliente.length === 0 ? "Efectivo" : "Efectivo", "Credito"]}
                    onChange={selected => this.setState({ tipoPago: selected.toLowerCase() })}
                    placeholder="Forma de pago"
                  />
                </div>
                {
                  this.state.tipoPago === 'efectivo' ?
                    <>
                      <TextInputField
                        id="id_dinero_recibido"
                        value={this.state.dineroRecibido}
                        onChange={(event) => {
                          if (isNaN(event.target.value)) {
                          } else {
                            this.setState({
                              dineroRecibido: event.target.value
                            })
                            if (event.target.value.length > 0) {
                              setTimeout(() => {
                                var vueltoCalculado = Number(this.state.dineroRecibido) - Number(this.props.total).toFixed(2)
                                this.setState({
                                  vuelto: Number(Number(vueltoCalculado) + Number(this.state.descuento)).toFixed(2)
                                })
                              }, 100)
                            } else {
                              this.setState({
                                vuelto: 0,
                                dineroRecibido: this.props.total
                              })
                            }
                          }
                        }}
                        onClick={(e) => {
                          e.target.select();
                        }}
                        style={{
                          color: this.state.dineroRecibido < this.props.total ? 'red' : 'green'
                        }}
                        label={<span style={{ color: "#333" }}>Recibe</span>}
                        width={175}
                      />
                      <TextInputField
                        disabled
                        value={this.state.vuelto}
                        label={<span style={{ color: "#333" }}>Cambio</span>}
                        width={150}
                      />
                    </>
                    :
                    <>
                      <TextInputField
                        id="id_dinero_recibido"
                        value={this.state.valorAcreditado}
                        onChange={(event) => {
                          if (isNaN(event.target.value)) {

                          } else {
                            this.setState({
                              valorAcreditado: event.target.value
                            })

                          }
                        }}
                        label={<span style={{ color: "#333" }}>Valor Acreditado</span>}
                        width={175}
                      />
                      <TextInputField
                        value={this.state.fecha}
                        onChange={(e) => {
                          this.validarFecha(e)
                        }}
                        placeholder="02-04-2019"
                        label={<span style={{ color: "#333" }}>Fecha de pago</span>}
                        width={150}
                      />
                    </>

                }
                <TextInputField
                  value={this.state.descuento}
                  onChange={(e) => {
                    if (!isNaN(e.target.value)) {
                      if (e.target.value.length > 0) {
                        this.setState({
                          descuento: e.target.value
                        })
                        setTimeout(() => {
                          this.setState({
                            totalDescontado: (this.props.total - this.state.descuento).toFixed(2)
                          })
                          var vueltoCalculado = Number(this.state.dineroRecibido) - Number(this.props.total).toFixed(2)
                          this.setState({
                            vuelto: Number(Number(vueltoCalculado) + Number(this.state.descuento)).toFixed(2)
                          })
                        }, 100)
                      } else {
                        this.setState({
                          descuento: 0
                        })
                        setTimeout(() => {
                          this.setState({
                            totalDescontado: 0.00
                          })
                        }, 100)
                      }
                    }
                  }}
                  onClick={(e) => {
                    e.target.select();
                  }}
                  style={{
                    color: this.state.descuento > this.props.total ? 'red' : 'green'
                  }}
                  label={<span style={{ color: "#333" }}>Descuento</span>}
                  width={150}
                />
              </div>
              {/*   <div style={{ width: "100%" }}>
                <div style={{ float: "right", marginRight: 15 }}>
                  <div style={{ display: "flex" }}>
                    <Switch
                      checked={this.state.imprimirRecibo}
                      onChange={() => this.setState({
                        imprimirRecibo: !this.state.imprimirRecibo
                      })}
                    />
                    <label
                      style={{
                        color: "#333",
                        fontWeight: 500,
                        fontSize: "14px",
                        marginTop: -2,
                        paddingLeft: 15
                      }}
                    >
                      Imprimir comprobante (F10)
                </label>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
          <hr />
          <div>
            <div style={{ float: "right" }}>
              <ReactToPrint
                ref={el => (this.refEventoImprimir = el)}
                trigger={() => <></>}
                content={() => this.refImprimirResivo}
                onBeforePrint={() => this.imprimir()}
              />

              <Button
                onClick={() => this.guardarVenta()}
                appearance="primary"
                isLoading={this.state.estadoVenta}
                disabled={this.state.estadoCerrarCaja === true ? true : false}
                marginRight="15px"
              >
                Aceptar (F9)
          </Button>
              <Button 
               isLoading={this.state.estadoVenta}
               disabled={this.state.estadoVenta === true ? true : false}
              onClick={() => this.props.volverEditar()} iconBefore="arrow-left">
                Volver a editar
          </Button>
              <ContainerPlantillas>
                <ResivoVenta
                  item={this.state.itemFormateadoImprimir}
                  ref={el => (this.refImprimirResivo = el)}

                />
              </ContainerPlantillas>
            </div>
          </div>
        </div>
      </Dialog>
    )
  }
}



export default MedioPago;
