import React, { useEffect, useState } from 'react'

import { Combobox } from 'evergreen-ui/commonjs/combobox';

const PreciosProducto = (props) => {

    const [precios, setPrecios] = useState([])

    useEffect(() => {
        props.precios.map(item => {
            precios.push(`${item.nombre}`)
        })


    }, [])



    return (
        <Combobox
            width={100}
            openOnFocus
            defaultSelectedItem={"Precio A"}
            items={precios}
            onChange={selected => props.cambiarPrecio(selected)}
            placeholder="Forma de pago"
        />
    )
}

export default PreciosProducto;