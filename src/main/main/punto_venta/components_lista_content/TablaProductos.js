import { Heading, IconButton, Table, TextInput } from "evergreen-ui";
import React, { useEffect } from "react";

import MUIDataTable from "mui-datatables";
import ToolbarSelectItem from "../../../tollbars/ToolbarSelectItem";
import themes from "../../../../utils/themeCustom";

const columns = [
  {
    name: "codigo",
    label: "Codigo",
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: "descripcion_producto",
    label: "Producto",
    options: {
      filter: true,
      sort: true
    }
  },
  {
    name: "cantidad",
    label: "Cantidad",
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: "tipoPrecio",
    label: "Precio",
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: "tipoPrecio",
    label: "PVP",
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: "descuento",
    label: "Descuento",
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: "descuento",
    label: "Subtotal",
    options: {
      filter: true,
      sort: false
    }
  },
  {
    name: "descuento",
    label: "Total",
    options: {
      filter: true,
      sort: false
    }
  }
];

const options = {
  filterType: "checkbox",
  elevation: "0",
  resizableColumns: true,
  rowHover: false,
  filter: false,
  sort: false,
  search: false,
  print: false,
  download: false,
  viewColumns: false,
  rowsPerPage:5
};

function TablaProductos(props) {

useEffect(()=>{
})
  return (
    <div
      style={{
        marginTop: "0px",
        marginBottom: "0px",
        borderColor: "#dcdcdc",
        borderStyle: "solid",
        borderWidth: "1px",
        borderRadius: "5px"
      }}
    >
     
      <MUIDataTable
        title={"Lista Productos"}
        data={props.lista}
        columns={columns}
        options={options}
      />

      {/*  <Table>
        <Table.Head>
          <Table.SearchHeaderCell flexBasis={40} flexShrink={0} flexGrow={0} />
          <Table.TextHeaderCell>Descripción</Table.TextHeaderCell>
          <Table.TextHeaderCell flexBasis={110} flexShrink={0} flexGrow={0}>
            Tipo de precio
          </Table.TextHeaderCell>
          <Table.TextHeaderCell flexBasis={70} flexShrink={0} flexGrow={0}>
            Descuento
          </Table.TextHeaderCell>
          <Table.TextHeaderCell flexBasis={80} flexShrink={0} flexGrow={0}>
            Sub total
          </Table.TextHeaderCell>
          <Table.TextHeaderCell flexBasis={70} flexShrink={0} flexGrow={0}>
            Total
          </Table.TextHeaderCell>
        </Table.Head>
        <Table.Body height={240}>
          {productos.map(produc => (
            <Table.Row
              key={produc.id}
              isSelectable
              style={{ background: "#456" }}
              onSelect={() => console.log(produc.nombre)}
            >
              <Table.TextCell flexBasis={60} flexShrink={0} flexGrow={0}>
                <IconButton icon="cross" height={24} />
              </Table.TextCell>
              <Table.TextCell
                isNumber
                flexBasis={100}
                flexShrink={0}
                flexGrow={0}
              >
                <TextInput
                  value={produc.cantidad}
                  style={{ width: "60px" }}
                  type="number"
                />
              </Table.TextCell>
              <Table.TextCell>{produc.nombre}</Table.TextCell>
              <Table.TextCell flexBasis={110} flexShrink={0} flexGrow={0}>
                {produc.tipoPrecio}
              </Table.TextCell>
              <Table.TextCell flexBasis={70} flexShrink={0} flexGrow={0}>
                {produc.descuento}
              </Table.TextCell>
              <Table.TextCell flexBasis={70} flexShrink={0} flexGrow={0}>
                {produc.subTotal}
              </Table.TextCell>
              <Table.TextCell flexBasis={70} flexShrink={0} flexGrow={0}>
                {produc.total}
              </Table.TextCell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table> */}
    </div>
  );
}

export default TablaProductos;
