import { Button, IconButton, Table } from "evergreen-ui";
import React, { Component, useEffect } from "react";

import { Combobox } from "evergreen-ui/commonjs/combobox";
import PreciosProducto from "./PreciosProductos";

class TablaProducto1 extends Component {

  render() {
    return (
      <Table>
        <Table.Head>
          <Table.TextHeaderCell width={50} flex='none' />
          <Table.TextHeaderCell>Tipo Precio</Table.TextHeaderCell>
          <Table.TextHeaderCell>Codigo</Table.TextHeaderCell>
          <Table.TextHeaderCell>Producto</Table.TextHeaderCell>
          <Table.TextHeaderCell>Cantidad</Table.TextHeaderCell>
          <Table.TextHeaderCell>Precio</Table.TextHeaderCell>
          <Table.TextHeaderCell>PVP</Table.TextHeaderCell>
          <Table.TextHeaderCell>Descuento</Table.TextHeaderCell>
          <Table.TextHeaderCell>Subtotal</Table.TextHeaderCell>
          <Table.TextHeaderCell>Total</Table.TextHeaderCell>
        </Table.Head>
        <Table.Body height={300}>
          {this.props.lista.map((profile, i) => (
            <Table.Row key={profile.codigo} isSelectable>
              <Table.TextCell width={50} flex='none'>
                <IconButton
                  onClick={() => this.props.eliminarItem(i)}
                  icon="trash"
                  intent="danger"
                />
              </Table.TextCell>
              <Table.TextCell padding="0px">{
                <PreciosProducto precios={profile.precios} cambiarPrecio={(precio) => this.props.cambiarPrecios(profile.codigo, precio)} />
              }</Table.TextCell>
              <Table.TextCell padding="0px">{profile.codigo}</Table.TextCell>
              <Table.TextCell>{profile.descripcion_producto}</Table.TextCell>
              <Table.TextCell isNumber>{profile.cantidad}</Table.TextCell>
              <Table.TextCell isNumber>{profile.precio_venta}</Table.TextCell>
              <Table.TextCell isNumber>{profile.precio_publico}</Table.TextCell>
              <Table.TextCell isNumber>{profile.descuento}</Table.TextCell>
              <Table.TextCell isNumber>{profile.subtotal}</Table.TextCell>
              <Table.TextCell isNumber>{profile.total}</Table.TextCell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );
  }
}

export default TablaProducto1;
