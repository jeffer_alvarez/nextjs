import React, { Component } from 'react';

class GenerarResivo extends Component {


    state = {
        item: [],
        emisor: [],
        imprimir: false,
        mes: '',
        claveAcceso: ''
    }



    componentDidMount() {
        this.setState({
            item: this.props.item,
            emisor: this.props.emisor,
            mes: this.props.mes,
            claveAcceso: this.props.claveAcceso
        })


        setTimeout(() => {
          
            $("#div_print").print();
            this.props.cerrar()
        }, 300);
 


    }

    llenar = (clave) => {
       // JsBarcode("#barcode", clave);
    }

    componentWillReceiveProps(props) {
        this.setState({
            item: props.item,
            emisor: props.emisor,
            mes: props.mes,
            claveAcceso: props.claveAcceso
        })
        /*   JsBarcode("#barcode", props.claveAcceso);
          $("#div_print").print();
          props.cerrar()  */
    }
    render() {
        const {
            item
        } = this.props


        return (
            <div id="div_print" style={{ width: 219, height: 'max-content', margin: 0, }}>
                {
                    item != null &&
                    <>
                          <h1 style={{textAlign:'center',fontFamily:'Lexend Exa'}}>SERVIFAC</h1>
                   
                        <hr style={{ width: 25, marginTop: 5 }} />
                      
                         <h5 style={{textAlign:'center'}}>   {item.razon_social}</h5>
                       
                        <div style={{ textAlign: 'left', fontSize: 12, marginTop: 8 }}>
                            <span>Dirección: {item.direccion}</span>
                        </div>
                        <div style={{ width: '100%', fontFamily: "Lexend Exa", fontSize: 12, marginTop: 16 }}>
                            <div>

                            </div>
                            <div style={{ textAlign: 'center' }}>
                                N°  COMPROBANTE: {item.numero_venta}
                            </div>
                            <div style={{ textAlign: 'center', marginTop: 8 }}>
                                COMPRAS REALIZADAS EN EFECTIVO
                        </div>
                           
                        </div>

                        <div style={{ fontFamily: "Lexend Exa", borderBottom: '.5px solid rgba(0,0,0,0.3)', borderTop: '.5px solid rgba(0,0,0,0.3)', paddingLeft: 5, paddingTop: 3, paddingBottom: 3, marginTop: 8.5, display: 'flex', flexDirection: 'row', fontSize: 10 }}>
                            <div style={{ width: 15 }}>C.</div>
                            <div style={{ width: 105 }}>Descripción</div>
                            <div style={{}}></div>
                            <div style={{ width: 50 }}>P. Unt.</div>
                            <div style={{ width: 50 }}>Valor</div>
                        </div>
                        <div>
                            {
                                item.productos.map(item =>

                                    <div key={item.codigo} style={{ fontFamily: "Lexend Exa", paddingLeft: 5, paddingRight: 5, paddingTop: 5, display: 'flex', flexDirection: 'row', fontSize: 10 }}>
                                        <div style={{ width: 15 }}>{item.cantidad}</div>
                                        <div style={{ width: 110 }}>{item.tiene_iva ? "*" + item.descripcion_producto.toUpperCase() : item.descripcion_producto}</div>
                                        <div style={{}}></div>
                                        <div style={{ width: 50 }}>{`$ ${item.tiene_iva ? ((Number(item.precio_venta) / 1.12) * Number(item.cantidad)).toFixed(3) : (Number(item.precio_venta) * Number(item.cantidad)).toFixed(3)}`}</div>
                                        <div style={{ width: 50 }}>{`$ ${item.tiene_iva ?
                                            ((Number(item.precio_venta) / 1.12) * Number(item.cantidad)).toFixed(2)
                                            :
                                            (Number(item.precio_venta) * Number(item.cantidad)).toFixed(2)}`}</div>
                                    </div>
                                )
                            }
                        </div>
                        <div style={{ borderBottom: '.5px solid rgba(0,0,0,0.3)', marginTop: 5, marginBottom: 5 }}></div>
                        <div style={{ marginBottom: 16 }}>
                            <div style={{ fontFamily: "Lexend Exa", paddingLeft: 10, paddingRight: 8, display: 'flex', flexDirection: 'row', fontSize: 8, textAlign: 'right' }}>
                                <div style={{ flex: 0.5 }}></div>
                                <div style={{}}>
                                    <div>SUBTOTAL SIN IMPUESTOS:</div>
                                    <div>SUBTOTAL IVA 12%:</div>
                                    <div>VALOR IVA 12%:</div>
                                    <div>DESCUENTO:</div>
                                    <div>VALOR TOTAL:</div>
                                </div>
                                <div style={{ flex: 0.3 }}></div>
                                <div style={{ fontFamily: "Lexend Exa", fontSize: 8, textAlign: 'right' }}>
                                    <div>{`$ ${Number((Number(item.subtotal) + Number(item.iva))).toFixed(2)}`}</div>
                                    <div>{`$ ${Number(item.subtotal).toFixed(2)}`}</div>
                                    <div>{`$ ${Number(item.iva).toFixed(2)}`}</div>
                                    <div>{`$ ${Number(item.descuento).toFixed(2)}`}</div>
                                    <div>{`$ ${Number(item.total).toFixed(2)}`}</div>
                                </div>
                            </div>
                        </div>

                        {
                            item.tipo_venta === 'factura' ?
                                <div style={{ fontFamily: "Lexend Exa", fontSize: 10, display: 'flex', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginRight: 10 }}>Revisa tu factura <br />electrónica en tu correo</div>
                                :
                                <div
                                    style={{ fontFamily: "Lexend Exa", fontSize: 10, display: 'flex', paddingLeft: 15, flexDirection: 'column' }}>
                                    <span>Cliente: CONSUMIDOR FINAL</span>
                                    <span>RUC:     9999999999999</span>

                                </div>
                        }
                        {
                            item.tipo_venta === 'factura' &&
                            <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lexend Exa", marginLeft: 15, fontSize: 8 }}>
                                <div style={{ fontSize: 8, display: 'flex' }}>{`Nombre: ${item.nombreCliente}`}</div>
                                <div style={{ fontSize: 8, display: 'flex' }}>{`Correo: ${item.emailCliente}`}</div>
                                <div style={{ fontSize: 8, display: 'flex' }}>{`Cédula: ${item.identificacionCliente}`}</div>
                                <div style={{ fontSize: 8, display: 'flex' }}>{`Dirección: ${item.direccionCliente}`}</div>
                            </div>
                        }
                        {
                            item.tipo_pago === 'efectivo' ?
                                <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lexend Exa", marginLeft: 15, fontSize: 10 }}>
                                    {/*  <div style={{ fontSize: 8, display: 'flex' }}>{`Tipo Pago: ${item.tipo_pago}`}</div> */}
                                </div>
                                : item.tipo_pago === 'credito' ?
                                    <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lexend Exa", marginLeft: 15, fontSize: 10 }}>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Tipo Pago: A Credito`}</div>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Valor Acreditado: $ ${item.valor_acreditado}`}</div>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Saldo Pendiente: $ ${item.total - item.valor_acreditado}`}</div>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Fecha a Pagar: ${item.fecha_a_pagar}`}</div>

                                    </div>
                                    :
                                    <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lexend Exa", marginLeft: 15, fontSize: 10 }}>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Tipo Pago:  ${item.tipo_pago}`}</div>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Numero: ${item.numero_tarjeta}`}</div>
                                        <div style={{ fontSize: 8, display: 'flex' }}>{`Nombre Banco: ${item.nombre_banco}`}</div>
                                    </div>
                        }
                        <div style={{ fontFamily: "Lexend Exa", fontSize: 10, marginTop: 8 }}>
                            <div style={{ borderBottom: '.5px solid rgba(0,0,0,0.3)', borderTop: '.5px solid rgba(0,0,0,0.3)', textAlign: 'center', padding: 8 }}>FACTURADO POR SERVIFAC - GLOBALTECH</div>

                        </div>



                    </>
                }

            </div>

        );
    }
}

export default GenerarResivo;