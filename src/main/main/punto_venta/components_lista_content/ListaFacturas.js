import "firebase/database";
import "firebase/auth";

import { Button, Card, Heading, IconButton, Pane, Paragraph, SideSheet } from "evergreen-ui";
import React, { Component } from "react";

import TablaVentas from "./TablaVentas";
import firebase from "firebase/app";
import funtions from "../../../../utils/funtions";

class ListaFacturas extends Component {

  state = {
    listaVentas: [],
    listaVentasTemporal: [],
    estadoTabla: 'cargando'
  }
  componentDidMount() {
   // this.obtenerDataBaseDatosFacturas()
    //console.log(this.props.cajaSeleccionada);
    // this.obtenerDataBaseDatos()
    if (this.props.cajaSeleccionada) {
      if (this.props.cajaSeleccionada.length > 0) {
        this.obtenerDataBaseDatos()
      } else {
        this.setState({
          listaVentas: [],
          listaVentasTemporal: [],
          estadoTabla: 'vacio'
        })
      }

    } else {
      this.setState({
        listaVentas: [],
        listaVentasTemporal: [],
        estadoTabla: 'vacio'
      })
    }

  }

  componentWillReceiveProps(props) {
  //  this.obtenerDataBaseDatosFacturas()

    if (props.cajaSeleccionada) {
      if (props.cajaSeleccionada.length > 0) {
        this.obtenerDataBaseDatos()
      } else {
        this.setState({
          listaVentas: [],
          listaVentasTemporal: [],
          estadoTabla: 'vacio'
        })
      }

    } else {
      this.setState({
        listaVentas: [],
        listaVentasTemporal: [],
        estadoTabla: 'vacio'
      })
    }
  }
  obtenerDataBaseDatos = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        var db = firebase.database();
        var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/' + this.props.cajaSeleccionada[0].codigo);
        //   var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/');
        //    var productosRef = db.ref('users/' + user.uid + '/ventas').orderByChild('caja').equalTo(this.props.cajaSeleccionada[0].codigo)

        console.log(operacionVentaCajaRef.ref);
        operacionVentaCajaRef.on('value', sna => {
          console.log(operacionVentaCajaRef.ref);
          if (sna.val()) {
            this.setState({
              listaVentas: [],
              listaVentasTemporal: [],
              estadoTabla: 'cargando'
            })
            var lista = funtions.snapshotToArray(sna)

            var filterList = lista.sort((a, b) => {
              a = new Date(a.order);
              b = new Date(b.order);
              return a > b ? -1 : a < b ? 1 : 0;
            })
            this.setState({
              listaVentas: filterList,
              listaVentasTemporal: filterList,
              estadoTabla: 'llena'
            })

          } else {
            this.setState({
              listaVentas: [],
              listaVentasTemporal: [],
              estadoTabla: 'vacio'
            })
          }
        })
        console.log(this.props.cajaSeleccionada[0].codigo)
        /*  productosRef.on('value', (snapshot) => {
           if (snapshot.val()) {
             console.log('hay datos');
             this.setState({
               listaVentas: [],
               listaVentasTemporal: [],
               estadoTabla: 'cargando'
             })
             var lista = funtions.snapshotToArray(snapshot)
 
             var filterList = lista.sort((a, b) => {
               a = new Date(a.order);
               b = new Date(b.order);
               return a > b ? -1 : a < b ? 1 : 0;
             })
             console.log('llena');
             this.setState({
               listaVentas: filterList,
               listaVentasTemporal: filterList,
               estadoTabla: 'llena'
             })
 
           } else {
             console.log('vacia');
             this.setState({
               listaVentas: [],
               listaVentasTemporal: [],
               estadoTabla: 'vacio'
             })
           }
         }); */
      }
    });
  }
  obtenerDataBaseDatosFacturas = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        var db = firebase.database();
        var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/facturas_ventas').orderByChild('secuencial');
        //var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas');
        //   var operacionVentaCajaRef = db.ref('users/' + firebase.auth().currentUser.uid + '/ventas_caja/');
        //    var productosRef = db.ref('users/' + user.uid + '/ventas').orderByChild('caja').equalTo(this.props.cajaSeleccionada[0].codigo)
        operacionVentaCajaRef.on('value', sna => {
          if (sna.val()) {

            var lista = funtions.snapshotToArray(sna)
            lista.map(item => {
              if (item.secuencial > "000000257") {
                console.log(`${item.fecha_emision} ${item.secuencial} ${item.id}`)
              }
            })
          }
        })

      }
    });
  }

  render() {
    return (
      <div style={{ marginTop: 60 }}>
        <TablaVentas usuario={this.props.usuario} lista={this.state.listaVentas} estadoTabla={this.state.estadoTabla} />
      </div>
    );
  }
}

export default ListaFacturas;
