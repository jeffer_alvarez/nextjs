import useKey from 'use-key-hook';

function KeyHooks(props) {
    useKey((pressedKey) => {
        console.log('Detected Key press', pressedKey);
    }, {
            detectKeys: ['F9', 27, 53],
        });
        return(
            <div>
               {props.children}
            </div>
        )
};

export default KeyHooks;