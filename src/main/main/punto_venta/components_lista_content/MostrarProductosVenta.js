import { Menu, Popover, Position } from 'evergreen-ui';

import React from 'react'

const MostrarProductosVenta = (props) => {
    return (
        <Popover
            position={Position.BOTTOM_LEFT}
            content={
                <Menu>
                    <Menu.Group title="Lista Productos">
                    {
                        props.lista.map(item=>{
                            return  <Menu.Item>{item.descripcion_producto.toUpperCase()}</Menu.Item>
                        })
                    }
                       
                      
                    </Menu.Group>
                   
                </Menu>
            }
        >
          {
              props.children
          }
        </Popover>
    )
}


export default MostrarProductosVenta;


