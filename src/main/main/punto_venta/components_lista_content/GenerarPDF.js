import React, { Component } from 'react';

class GenerarPDF extends Component {

    
        state = {           
            item: [],
            emisor: [],
            imprimir: false,
            mes: '',
            claveAcceso: ''
        }
   


    componentDidMount() {
        this.setState({
            item: this.props.item,
            emisor: this.props.emisor,            
            mes: this.props.mes,
            claveAcceso: this.props.claveAcceso
        })

      
        setTimeout(() => {
            this.llenar(this.props.claveAcceso)
        $("#div_print").print(); 
        this.props.cerrar()
       }, 300); 

        
        
    }

    llenar=(clave)=>{
        JsBarcode("#barcode", clave);
    }

    componentWillReceiveProps(props) {
        this.setState({
            item: props.item,
            emisor: props.emisor,            
            mes: props.mes,
            claveAcceso: props.claveAcceso
        })
      /*   JsBarcode("#barcode", props.claveAcceso);
        $("#div_print").print();
        props.cerrar()  */
    }
    render() {
        return (
            <div id="div_print" style={{ width: '100%', height: '100vh'}}>
                <div style={{ display: 'flex', alignItems: 'flex-end', width: '100%', flexDirection: 'row', paddingRight: 175, paddingTop: 25, justifyContent: 'flex-end' }}>
                    {/*   <div style={{ padding: 10, backgroundColor: 'lime', fontSize: '1.1rem', fontWeight: 800, borderRadius: 12, marginInlineEnd: 15 }}>
                    PAGADA
            </div>
                <div style={{ padding: 10, backgroundColor: 'lime', fontSize: '1.1rem', fontWeight: 800, borderRadius: 12 }}>
                    A CREDITO
            </div> */}
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', width: '100%', padding: 25, paddingTop: 25, paddingBottom: 15 }}>
                    <div style={{ width: '50%', marginTop: 25 }}>
                        <div style={{ display: 'flex', flexDirection: 'column', marginTop: 75 }}>
                            {this.state.emisor.length != 0 &&
                                <>
                                    {/*     <h6 style={{ fontSize: '1rem' }}>Dátil</h6> */}
                                    <h6 style={{ fontWeight: 100, }}> {this.state.emisor[0].razon_social}</h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>RUC {this.state.emisor[0].ruc}</h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.emisor[0].direccion}</h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}></h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.emisor[0].obligado_contabilidad === true ? 'Obligado a llevar contabilidad' : ' No obligado a llevar contabilidad'}  </h6>
                                </>}
                        </div>


                    </div>
                    <div style={{ width: '50%' }}>
                        <div style={{ display: 'flex', alignItems: 'flex-end', flexDirection: 'column' }}>
                            {this.state.emisor.length != 0 &&
                                <>
                                    <h4>Factura {`${this.state.emisor[0].establecimiento.codigo}`}-{`${this.state.emisor[0].establecimiento.punto_emision}`}-{this.state.item[0].secuencial}</h4>
                                    <h5>{this.state.mes} {this.state.item[0].fecha_emision.substr(0, 2)}, {this.state.item[0].fecha_emision.substr(6, 4)}</h5>
                                </>}
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', marginTop: 25 }}>
                            {this.state.item.length != 0 &&
                                this.state.item[0].clave_acceso &&
                                <> <h6 style={{ fontSize: '1rem' }}>Autorización</h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1.2rem', letterSpacing: 1 }}>N.º {this.state.item[0].numero_autorizacion}</h6>
                                    <h6 style={{ fontSize: '1rem' }}>Ambiente :<spam style={{ fontWeight: 100, fontSize: '1rem' }}> {this.state.emisor[0].ambiente === 1 ? 'PRUEBAS' : 'PRODUCCIÓN'}</spam></h6>
                                    <h6>Clave de acceso :</h6>
                                    <svg style={{ width: '100%', marginTop: -20 }} id="barcode"></svg></>}
                            {/* //<h6 style={{ fontWeight: 100, fontSize: '1rem' }}>0504201901099271255400120010020000135051994717612</h6> */}
                        </div>
                    </div>
                </div>

                <div style={{ width: '100%', padding: '0px 25px', }}>
                    <div style={{ borderTop: '1px solid #e4e4e4', borderTop: '1px solid #e4e4e4', width: '100%' }}></div>
                    <div style={{ display: 'flex', flexDirection: 'row', marginBottom: 16 }}>
                        <div style={{ width: '50%' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', marginTop: 25 }}>
                                {this.state.item.length != 0 &&
                                    <>
                                        <h6 style={{ fontSize: '1rem' }}>{this.state.item[0].comprador.razon_social}</h6>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].comprador.tipo_identificacion === '05' ? 'CEDULA' : this.state.item[0].comprador.tipo_identificacion === '04' ? 'RUC' : ''} {this.state.item[0].comprador.identificacion}</h6>
                                        {/*  <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>ZUMBI</h6> */}
                                        {/*   <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>Tel. 0988160796</h6> */}
                                    </>
                                }
                            </div>
                        </div>

                        <div style={{ width: '50%' }}>
                            <div style={{ display: 'flex', flexDirection: 'row', marginTop: 25, padding: 5, borderTop: '1px solid #e4e4e4', borderBottom: '1px solid #e4e4e4' }}>
                                <div style={{ width: '45%' }}>
                                    <h6 style={{ fontSize: '1rem' }}>Forma de pago</h6>
                                </div>
                                <div style={{ width: '45%' }}>
                                    <h6 style={{ fontSize: '1rem' }}>Plazo</h6>
                                </div>
                                <div style={{ width: '10%' }}>
                                    <h6 style={{ fontSize: '1rem' }}>Monto</h6>
                                </div>
                                {/*  <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>N.º 0504201901099271255400120010020000135051994717612</h6>
                        <h6>Ambiente :<spam style={{ fontWeight: 100, fontSize: '1.2rem' }}> PRODUCCIÓN</spam></h6>
                        <h6>Clave de acceso :</h6>
                        <svg style={{ width: '90%', marginTop: -20 }} id="barcode"></svg> */}
                                {/* //<h6 style={{ fontWeight: 100, fontSize: '1rem' }}>0504201901099271255400120010020000135051994717612</h6> */}
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row', marginTop: 5, padding: 5 }}>
                                {this.state.item.length != 0 &&
                                    <>
                                        <div style={{ width: '45%' }}>
                                            <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].pagos[0].medio === 'efectivo' ? 'Sin utilización del sistema financiero' : 'Otros con utilización del sistema financiero'}</h6>
                                        </div>
                                        <div style={{ width: '45%' }}>
                                            <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].credito ? '(vence ' + this.state.item[0].credito.fecha_vencimiento + ")" : ''}</h6>
                                        </div>
                                        <div style={{ width: '10%' }}>
                                            <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.importe_total).toFixed(2)}</h6>
                                        </div>
                                    </>
                                }
                                {/*  <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>N.º 0504201901099271255400120010020000135051994717612</h6>
                        <h6>Ambiente :<spam style={{ fontWeight: 100, fontSize: '1.2rem' }}> PRODUCCIÓN</spam></h6>
                        <h6>Clave de acceso :</h6>
                        <svg style={{ width: '90%', marginTop: -20 }} id="barcode"></svg> */}
                                {/* //<h6 style={{ fontWeight: 100, fontSize: '1rem' }}>0504201901099271255400120010020000135051994717612</h6> */}
                            </div>
                        </div>
                    </div>
                </div>



                <div style={{ display: 'flex', flexDirection: 'row', width: '100%', padding: '0px 25px' }}>

                    <div style={{ width: '100%', display: 'flex', flexDirection: 'row', borderTop: '1px solid #e4e4e4', borderBottom: '1px solid #e4e4e4', padding: '5px 0' }}>

                        <div style={{ width: '10%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Cantidad</h6>
                        </div>
                        <div style={{ width: '15%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Codigo</h6>
                        </div>
                        <div style={{ width: '40%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Descripcion</h6>
                        </div>
                        <div style={{ width: '15%' }}>
                            <h6 style={{ fontSize: '1rem', textAlign: 'right' }}>Precio unitario</h6>
                        </div>
                        <div style={{ width: '10%' }}>
                            <h6 style={{ fontSize: '1rem', textAlign: 'right' }}>Descuento</h6>
                        </div>
                        <div style={{ width: '10%', textAlign: 'right' }}>
                            <h6 style={{ fontSize: '1rem' }}>Monto</h6>
                        </div>
                        {/*  <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>N.º 0504201901099271255400120010020000135051994717612</h6>
                        <h6>Ambiente :<spam style={{ fontWeight: 100, fontSize: '1.2rem' }}> PRODUCCIÓN</spam></h6>
                        <h6>Clave de acceso :</h6>
                        <svg style={{ width: '90%', marginTop: -20 }} id="barcode"></svg> */}
                        {/* //<h6 style={{ fontWeight: 100, fontSize: '1rem' }}>0504201901099271255400120010020000135051994717612</h6> */}


                    </div>


                </div>
                {
                    this.state.item != 0 &&
                    this.state.item[0].items.map(item => {
                        return <div style={{ display: 'flex', flexDirection: 'row', width: '100%', padding: '0px 25px' }}>

                            <div style={{ width: '100%', display: 'flex', flexDirection: 'row', padding: '5px 0' }}>

                                <div style={{ width: '10%' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{item.cantidad}</h6>
                                </div>
                                <div style={{ width: '15%', display: 'flex', flexDirection: 'column' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{item.codigo_auxiliar} - </h6>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{item.codigo_principal}</h6>
                                </div>
                                <div style={{ width: '40%' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{item.descripcion}</h6>
                                </div>
                                <div style={{ width: '15%', textAlign: 'right', paddingRight: 20 }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(item.precio_total_sin_impuestos).toFixed(3)}</h6>
                                </div>
                                <div style={{ width: '10%', textAlign: 'right', paddingRight: 10 }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(item.descuento).toFixed(3)}</h6>
                                </div>
                                <div style={{ width: '10%', textAlign: 'right' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(item.precio_total_sin_impuestos).toFixed(3)}</h6>
                                </div>
                                {/*  <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>N.º 0504201901099271255400120010020000135051994717612</h6>
<h6>Ambiente :<spam style={{ fontWeight: 100, fontSize: '1.2rem' }}> PRODUCCIÓN</spam></h6>
<h6>Clave de acceso :</h6>
<svg style={{ width: '90%', marginTop: -20 }} id="barcode"></svg> */}
                                {/* //<h6 style={{ fontWeight: 100, fontSize: '1rem' }}>0504201901099271255400120010020000135051994717612</h6> */}
                            </div>
                        </div>
                    })
                }

                <div style={{ width: '100%', padding: '0px 25px', }}>
                    <div style={{ borderTop: '1px solid #e4e4e4', width: '100%' }}></div>
                    <div style={{ display: 'flex', flexDirection: 'row', }}>


                        <div style={{ width: '60%' }}>

                        </div>

                        <div style={{ width: '25%', textAlign: 'right' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', marginTop: 15 }}>
                                <h6 style={{ fontSize: '1rem' }}>Subtotal sin impuestos</h6>
                                <h6 style={{ fontSize: '1rem' }}>Subtotal IVA 12%</h6>
                                <h6 style={{ fontSize: '1rem' }}>Subtotal IVA 0%</h6>
                                <h6 style={{ fontSize: '1rem' }}>Valor IVA 12%</h6>
                                <h6 style={{ fontSize: '1rem' }}>Valor total</h6>
                            </div>
                        </div>
                        <div style={{ width: '15%', textAlign: 'right' }}>
                            <div style={{ display: 'flex', flexDirection: 'column', marginTop: 15, marginBottom: 15 }}>
                                {
                                    this.state.item.length != 0 &&
                                    <>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.total_sin_impuestos).toFixed(3)}</h6>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.impuestos[1].base_imponible).toFixed(3)}</h6>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.impuestos[0].base_imponible).toFixed(3)}</h6>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.impuestos[1].valor).toFixed(3)}</h6>
                                        <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].totales.importe_total).toFixed(3)}</h6>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div style={{ width: '100%', padding: '0px 25px', }}>
                    <div style={{ borderTop: '1px solid #e4e4e4', borderTop: '1px solid #e4e4e4', width: '100%', padding: '15px 0px' }}>
                        <h6 style={{ fontSize: '1rem' }}>Pagos</h6>
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', borderTop: '1px solid #e4e4e4', borderBottom: '1px solid #e4e4e4', padding: '5px 0' }}>
                        <div style={{ width: '25%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Fecha de pago</h6>
                        </div>
                        <div style={{ width: '25%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Forma  de pago</h6>
                        </div>
                        <div style={{ width: '25%' }}>
                            <h6 style={{ fontSize: '1rem' }}>Notas</h6>
                        </div>
                        <div style={{ width: '25%', textAlign: 'right' }}>
                            <h6 style={{ fontSize: '1rem' }}>Monto</h6>
                        </div>
                    </div>

                    <div style={{ display: 'flex', flexDirection: 'row', padding: '5px 0' }}>
                        {
                            this.state.item.length != 0 &&
                            <>
                                <div style={{ width: '25%' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].fecha_emision}</h6>
                                </div>
                                <div style={{ width: '25%' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].pagos[0].medio.toUpperCase()}</h6>
                                </div>
                                <div style={{ width: '25%' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}></h6>
                                </div>
                                <div style={{ width: '25%', textAlign: 'right' }}>
                                    <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].pagos[0].total).toFixed(3)}</h6>
                                </div>
                            </>
                        }
                    </div>
                    {
                        this.state.item.length != 0 &&
                        this.state.item[0].credito &&
                        <div style={{ display: 'flex', flexDirection: 'row', padding: '5px 0' }}>
                            <div style={{ width: '25%' }}>
                                <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>{this.state.item[0].fecha_emision}</h6>
                            </div>
                            <div style={{ width: '25%' }}>
                                <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>CREDITO</h6>
                            </div>
                            <div style={{ width: '25%' }}>
                                <h6 style={{ fontWeight: 100, fontSize: '1rem' }}></h6>
                            </div>
                            <div style={{ width: '25%', textAlign: 'right' }}>
                                <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].credito.monto).toFixed(3)}</h6>
                            </div>

                        </div>
                    }
                </div>
                <div style={{ display: 'flex', flexDirection: 'row', padding: '0px 25px' }}>
                    <div style={{ width: '60%' }}>
                    </div>
                    <div style={{ width: '25%', textAlign: 'right' }}>
                        <h6 style={{ fontSize: '1rem' }}>Total pagado</h6>
                    </div>
                    <div style={{ width: '15%', textAlign: 'right' }}>
                        {
                            this.state.item.length != 0 &&
                            <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].pagos[0].total).toFixed(3)}</h6>
                        }
                    </div>
                </div>

                {
                    this.state.item.length != 0 &&
                    this.state.item[0].credito &&
                    <div style={{ display: 'flex', flexDirection: 'row', padding: '0px 25px' }}>
                        <div style={{ width: '60%' }}>
                        </div>
                        <div style={{ width: '25%', textAlign: 'right' }}>
                            <h6 style={{ fontSize: '1rem' }}>Saldo Pendiente</h6>
                        </div>
                        <div style={{ width: '15%', textAlign: 'right' }}>
                            {
                                this.state.item.length != 0 &&
                                <h6 style={{ fontWeight: 100, fontSize: '1rem' }}>$ {Number(this.state.item[0].credito.monto).toFixed(3)}</h6>
                            }
                        </div>
                    </div>
                }




            </div>

        );
    }
}

export default GenerarPDF;