import React from 'react';
import { red } from '@material-ui/core/colors';

const ErrorEstado = props => {
    return (
        <div style={{
            fontSize: 10,
            color: red[300],
            fontStyle: 'italic',
            width: '100%',
            textAlign:'center',
        }}
        >
            {props.children}
        </div>
    )
}

export default ErrorEstado;