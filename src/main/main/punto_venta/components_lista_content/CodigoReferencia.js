import Autosuggest from 'react-autosuggest';

// Imagine you have a list of languages that you'd like to autosuggest.


// Teach Autosuggest how to calculate suggestions for any given input value.


// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.codigo_referencia;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
  <div>
    {suggestion.descripcion_producto}
  </div>
);

class Example extends React.Component {

  state = {
    value: '',
    suggestions:  this.props.data
  };

  componentDidMount(){  
    if(this.props.borrar){
      this.setState({
        value:''
      })
    }  
  }

  componentWillReceiveProps(props){    
    if(props.borrar){
      this.setState({
        value:''
      })
    }
  }

  getSuggestions = (value) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
  
   if(value.length===13){
    return inputLength === 0 ? [] : this.props.data.filter(lang =>
      lang.codigo_barras.slice(0, inputLength) === inputValue
    );
    
   }else{
    return inputLength === 0 ? [] : this.props.data.filter(lang =>
      lang.codigo_referencia.slice(0, inputLength) === inputValue
    );
   }
  };

  onChange = (event, { newValue }) => {    
    this.setState({
      value: newValue
    });
   if(newValue.length===13){
    this.setState({
      value: ''
    });
    this.props.buscar(newValue)
  }else{
   
    this.props.buscar(newValue)

   }
  };
  onKeyPress = (event) => {
    if(event.charCode===13){
      this.props.cantidad()
    }
    
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions:this.getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Buscar por Código(F3)',
      value,
      onChange: this.onChange,
      onKeyPress:this.onKeyPress
    };

    // Finally, render it!
    return (
      <>
        <Autosuggest
          id="codigo_producto"
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          inputProps={inputProps}
        />
        <style jsx global>{`
      .react-autosuggest__container {
        position: relative;
      }
      
      .react-autosuggest__input {
        width: 240px;
        height: 33px;
        padding: 10px 20px;
        font-family: Helvetica, sans-serif;
        font-weight: 300;
        font-size: 14px;
        border: 1px solid #aaa;
        border-radius: 4px;
      }
      
      .react-autosuggest__input--focused {
        outline: none;
      }
      
      .react-autosuggest__input--open {
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
      }
      
      .react-autosuggest__suggestions-container {
        display: none;
      }
      
      .react-autosuggest__suggestions-container--open {
        display: block;
        position: absolute;
        top: 30px;
        width: 280px;
        border: 1px solid #aaa;
        background-color: #fff;
        font-family: Helvetica, sans-serif;
        font-weight: 300;
        font-size: 12px;
        max-height:250px;
        overflow-y: scroll;
        border-bottom-left-radius: 4px;
        border-bottom-right-radius: 4px;
        z-index: 2;
      }
      
      .react-autosuggest__suggestions-list {
        margin: 0;
        padding: 0;
        list-style-type: none;
      }
      
      .react-autosuggest__suggestion {
        cursor: pointer;
        padding: 10px 20px;
      }
      
      .react-autosuggest__suggestion--highlighted {
        background-color: #ddd;
      }
      `}</style>
      </>
    );
  }
}

export default Example