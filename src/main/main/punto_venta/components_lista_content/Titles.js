import React from "react";
import { Heading } from "evergreen-ui";

function Titles(props) {
  return (
    <div style={{ display: "flex" }}>
      <Heading size={300}>...</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Cantidad</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Descripcion</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Tipo de precio</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Descuento</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Precio unitario</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>Total</Heading>
    </div>
  );
}

export default Titles;
