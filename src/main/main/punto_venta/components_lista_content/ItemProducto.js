import React from "react";
import { Heading, IconButton, TextInput } from "evergreen-ui";

function ItemProducto(props) {
  return (
    <div
      style={{
        display: "flex",
        marginTop: "8px",
        marginBottom: "8px",
        padding: "4px",
        borderColor: "#dcdcdc",
        borderStyle: "solid",
        borderWidth: "1px",
        borderRadius: "5px",
        alignItems: "center"
      }}
    >
      <IconButton icon="cross" height={24} />
      <div style={{ flex: "0.1" }} />
      <TextInput value={2} style={{ width: "50px" }} type="number" />
      <div style={{ flex: "0.1" }} />
      <Heading size={300}>Arrox macareño x libras</Heading>
      <div style={{ flex: "2" }} />
      <Heading size={300}>Precio A</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>0,00</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>12,00</Heading>
      <div style={{ flex: "1" }} />
      <Heading size={300}>24,00</Heading>
    </div>
  );
}

export default ItemProducto;
