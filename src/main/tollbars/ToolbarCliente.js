import "firebase/database";
import "firebase/auth";

import {
  SearchInput,
  TextInputField,
  toaster,
  Spinner
} from "evergreen-ui";
import React, { Component, useEffect, useRef, useState } from "react";

import BuscarClienteNombre from "./components/BuscarClienteNombre";
import TipoDocumento from "./components/TipoDocumento";
import firebase from "firebase/app";
import themes from "../../utils/themeCustom";
import { Grid } from "@material-ui/core";
import setSnackBars from "../../utils/setSnackBars";



class ToolbarCliente extends Component {
  state = {
    documento: "",
    celular: "",
    ciudad: "",
    nombre: "",
    direccion: "",
    email: "",
    visible: "none",
    listaClientes: [],
    cliente: [],
    desabilitar: false
  };


  componentWillReceiveProps(props) {
    if (props.estado === false) {
      this.setState({
        documento: "",
        celular: "",
        nombre: "",
        direccion: "",
        email: "",
        visible: "none",
        cliente: [],
        ciudad: ''
      });
    } else if (props.estado) {
      if (props.cliente.length > 0) {
        this.setState({
          cliente: props.cliente,
          documento: props.cliente.identificacion,
          celular: props.cliente.celular,
          nombre: props.cliente.nombre,
          direccion: props.cliente.direccion,
          email: props.cliente.email,
        })
      }
    }
    if (props.editar) {
      if (props.cliente.nombre) {
        this.setState({
          visible: 'block',
        })

        this.setState({
          cliente: props.cliente,
          desabilitar: true,
          documento: props.cliente.identificacion,
          celular: props.cliente.celular,
          nombre: props.cliente.nombre,
          direccion: props.cliente.direccion,
          email: props.cliente.email,
        })
      } else if (props.cliente.length === 0) {
        this.setState({
          desabilitar: false
        })
      }
    }

  }

  cargarClienteRegistroCivil = async () => {
    firebase.auth().onAuthStateChanged(async(user) => {
      if (user) {

        const rawResponse = await fetch('https://server-clientes-registro-civil.herokuapp.com/cedulaReg', {
          method: 'POST',
          headers: {
            'cedula': this.state.documento,
            'iud': user.uid
          },
          body: ''
        }).then((response) => {
          return response.json();
        }).then((data) => {
          console.log(data)
          if (data.identificacion) {
            console.log('cliente encontrado')
            console.log(data)
            this.setState({
              celular: data.celular ? data.celular : '',
              nombre: data.razonsocial ? data.razonsocial : '',
              direccion: data.direccion ? data.direccion : '',
              ciudad: data.ciudad ? data.ciudad : '',
              email: data.email ? data.email : '',
              identificacion: data.identificacion ? data.identificacion : '',
              cedulaCorrecta: true
            })
            this.setState({
              buscandoCedula: false
            });
            setTimeout(() => {
              this.subirCliente()
            }, 100)
          } else {
            console.log('cliente no encontrado')
            this.setState({
              cedulaCorrecta: false
            })
            this.setState({
              buscandoCedula: false
            });
          }
        }).catch((error) => {
          setSnackBars.openSnack(
            "error",
            "rootSnackBar",
            "Error de internet vuelva a intentar",
            2000
          );
          this.setState({
            buscandoCedula: false
          });
        });
      } else {
        // User is signed out.
      }
    });

  }

  subirCliente = () => {
    var cliet = {
      celular: this.state.celular,
      direccion: this.state.direccion,
      email: this.state.email,
      identificacion: this.state.identificacion,
      ciudad: this.state.ciudad,
      nombre: this.state.nombre
    }

    this.props.cargarCliente(cliet);
  }

  cargarCliente = cliente => {
    this.setState({
      celular: cliente.celular,
      direccion: cliente.direccion,
      email: cliente.email,
      documento: cliente.identificacion,
      ciudad: cliente.ciudad,
      cliente: cliente
    })

    this.props.cargarCliente(cliente);
  };

  borrarCliente = () => {
    this.setState({
      documento: "",
      celular: "",
      nombre: "",
      direccion: "",
      email: "",
      cliente: []
    });
  };

  render() {
    return (
      <>
        <Grid container spacing={0} style={{ marginTop: 50, background: themes.lightest }}>
          <Grid container sm={9}>
            <Grid item xs={12} sm={4} style={{ height: 70 }}>
              <div style={{ width: '100%', padding: 8 }}>
                <label
                  style={{
                    color: "#333",
                    fontWeight: 500,
                    fontSize: "1rem"
                  }}
                >
                  Tipo de documento
                </label>
                <TipoDocumento
                  tipoVenta={
                    this.state.nombre.length > 0 ? "factura" : "consumidor_final"
                  }
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={4} style={{ height: 70 }}>
              <div style={{ width: '100%', padding: 8 }}>
                <label
                  style={{
                    color: "#333",
                    fontWeight: 500,
                    fontSize: "1rem",
                    marginBottom: -17
                  }}
                >
                  RUC/ C.I
                </label>
                <div style={{ flexDirection: 'row', display: 'flex' }}>
                  <SearchInput
                    disabled={this.state.desabilitar}
                    id="id_cedula"
                    placeholder="RUC/CI/PAS (F1)"
                    label={
                      <span style={{ color: "#333", fontWeight: 500 }}>
                        Direccion
                </span>
                    }
                    value={this.state.documento}
                    onKeyPress={(e) => {
                      if (e.charCode === 13) {
                        if (this.state.documento.length === 10 || this.state.documento.length === 13) {
                          if (this.state.documento.length === 10) {
                            //setTimeout(() => { this.buscarCliente() }, 100)
                            setTimeout(() => { this.cargarClienteRegistroCivil() }, 100)
                            this.setState({
                              buscandoCedula: true
                            });
                          } else {
                            this.setState({
                              celular: "",
                              nombre: "",
                              direccion: "",
                              email: "",
                              cedulaCorrecta: false
                            });
                          }

                          if (this.state.documento.length === 13) {
                            //setTimeout(() => { this.buscarCliente() }, 100)
                            setTimeout(() => { this.cargarClienteRegistroCivil() }, 100)
                            this.setState({
                              buscandoCedula: true
                            });
                          } else {
                            this.setState({
                              celular: "",
                              nombre: "",
                              direccion: "",
                              email: "",
                              cedulaCorrecta: false
                            });
                          }

                        } else {
                          setSnackBars.openSnack(
                            "error",
                            "rootSnackBar",
                            "Ingresa la identificacion correcta",
                            2000
                          );
                        }
                      }
                    }}
                    onChange={e => {
                      this.setState({
                        documento: e.target.value,
                        identificacion:e.target.value
                      });
                      if (e.target.value.length === 0) {
                        this.setState({
                          documento: "",
                          celular: "",
                          nombre: "",
                          direccion: "",
                          email: "",
                          ciudad: '',
                          visible: "block"
                        });
                        this.props.cargarCliente([]);
                        setTimeout(() => {
                          this.setState({
                            visible: "none"
                          });
                        }, 50)

                      }
                    }}
                    onBlur={() => {
                      if (this.state.documento.length < 10) {
                        this.setState({
                          documento: '',
                          visible: 'none'
                        })
                      }
                    }}
                    style={{
                      borderRadius: "0px",
                      fontSize: "15px",
                      width: "100%",
                      color: this.state.cedulaCorrecta ? 'green' : 'red'
                    }}
                    width={"100%"}
                  />
                  <Spinner size={30} style={{ display: this.state.buscandoCedula ? 'block' : 'none' }} />
                </div>
              </div>
            </Grid>
            <Grid item xs={12} sm={4} style={{ height: 70 }}>
              {/*  {this.state.visible != "none" ? (
              <></>
            ) : (
                <BuscarClienteNombre
                  id="id_nombre_cliente"
                  clientes={this.state.listaClientes}
                  cargarCliente={cliente => this.cargarCliente(cliente)}
                  nombres={this.state.nombres}
                  style={{
                    height: "32px",
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%",
                    display: "none"
                  }}
                  height={32}
                  width="100%"
                />
              )} */}
              <div style={{ padding: 8 }}>
                <TextInputField
                  placeholder="NOMBRE / RAZON SOCIAL (F1)"
                  disabled={this.state.desabilitar}
                   onChange={e => {
                      this.setState({
                        nombre: e.target.value
                      })}}
                  value={this.state.nombre}
                  label={<span style={{ color: "#333" }}>Nombre del cliente</span>}
                  style={{
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%",
                  }}
                /* display={this.state.visible} */
                />
              </div>
            </Grid>

            <Grid item xs={12} sm={3} style={{ height: 60 }}>
              <div style={{ padding: 8 }}>
                <TextInputField
                  name="text-input-name8"
                  placeholder="Ciudad"
                  disabled={this.state.desabilitar}
                  value={this.state.ciudad}
                  onChange={(e) => {
                    this.setState({
                      ciudad: e.target.value
                    })
                    setTimeout(() => {
                      this.subirCliente()
                    }, 100)
                  }}
                  style={{
                    height: "32px",
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%"
                  }}
                />
              </div>
            </Grid>

            <Grid item xs={12} sm={3} style={{ height: 60 }}>
              <div style={{ padding: 8 }}>
                <TextInputField
                  name="text-input-name2"
                  placeholder="Dirección"
                  disabled={this.state.desabilitar}
                  value={this.state.direccion}
                  onChange={(e) => {
                    this.setState({
                      direccion: e.target.value
                    })
                    setTimeout(() => {
                      this.subirCliente()
                    }, 100)
                  }}
                  style={{
                    height: "32px",
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%"
                  }}
                />
              </div>
            </Grid>

            <Grid item xs={12} sm={3} style={{ height: 60 }}>
              <div style={{ padding: 8 }}>
                <TextInputField
                  name="text-input-name45"
                  placeholder="Teléfono / Celular"
                  disabled={this.state.desabilitar}
                  value={this.state.celular}
                  onChange={(event) => {
                    this.setState({
                      celular: event.target.value
                    })
                    setTimeout(() => {
                      this.subirCliente()
                    }, 100)
                  }}
                  style={{
                    height: "32px",
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%"
                  }}
                />
              </div>
            </Grid>

            <Grid item xs={12} sm={3} style={{ height: 60 }}>
              <div style={{ padding: 8 }}>
                <TextInputField
                  name="text-input-name2"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={(event) => {
                    this.setState({
                      email: event.target.value
                    })
                    setTimeout(() => {
                      this.subirCliente()
                    }, 100)
                  }}
                  disabled={this.state.desabilitar}
                  style={{
                    height: "32px",
                    borderRadius: "0px",
                    fontSize: "15px",
                    width: "100%"
                  }}
                />
              </div>
            </Grid>
          </Grid>
          <Grid container sm={3} >
            <div
              style={{
                fontSize: 18,
                padding: 8,
                width: '100%',
                display: 'flex'
              }}
            >
              <div>
                <p style={{ marginBottom: 0 }}>Subtotal: </p>
                <p style={{ marginBottom: 0 }}>Iva: </p>
                <p>Total:</p>
              </div>
              <div style={{ flex: 0.8 }} />
              <div style={{ float: "right" }}>
                <p style={{ marginBottom: 0 }}>
                  {Number(this.props.subtotal).toFixed(3)}
                </p>
                <p style={{ marginBottom: 0 }}>
                  {Number(this.props.iva).toFixed(3)}
                </p>
                <p>{Number(this.props.totales).toFixed(3)}</p>
              </div>
            </div>
          </Grid>

        </Grid>
      </>
    );
  }
}



export default ToolbarCliente;
