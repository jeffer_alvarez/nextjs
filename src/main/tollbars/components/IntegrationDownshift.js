import "firebase/database";
import "firebase/auth";

import React, { useEffect } from "react";

import Downshift from "downshift";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import deburr from "lodash/deburr";
import firebase from "firebase/app";
import { useList } from "react-firebase-hooks/database";

const suggestions = [
  { label: "Afghanistan" },
  { label: "Aland Islands" },
  { label: "Albania" },
  { label: "Algeria" },
  { label: "American Samoa" },
  { label: "Andorra" },
  { label: "Angola" },
  { label: "Anguilla" },
  { label: "Antarctica" },
  { label: "Antigua and Barbuda" },
  { label: "Argentina" },
  { label: "Armenia" },
  { label: "Aruba" },
  { label: "Australia" },
  { label: "Austria" },
  { label: "Azerbaijan" },
  { label: "Bahamas" },
  { label: "Bahrain" },
  { label: "Bangladesh" },
  { label: "Barbados" },
  { label: "Belarus" },
  { label: "Belgium" },
  { label: "Belize" },
  { label: "Benin" },
  { label: "Bermuda" },
  { label: "Bhutan" },
  { label: "Bolivia, Plurinational State of" },
  { label: "Bonaire, Sint Eustatius and Saba" },
  { label: "Bosnia and Herzegovina" },
  { label: "Botswana" },
  { label: "Bouvet Island" },
  { label: "Brazil" },
  { label: "British Indian Ocean Territory" },
  { label: "Brunei Darussalam" }
];

function renderInput(inputProps) {
  const { InputProps, ref, ...other } = inputProps;

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        ...InputProps
      }}
      {...other}
    />
  );
}

function renderSuggestion({
  suggestion,
  index,
  itemProps,
  highlightedIndex,
  selectedItem
}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion.label) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.label}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
    >
      {suggestion.label}
    </MenuItem>
  );
}

function getSuggestions(value) {
  const inputValue = deburr(value.trim()).toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter(suggestion => {
        const keep =
          count < 5 &&
          suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

let popperNode;

function useSearchProductos() {
  const [valueSearch, setValueSearch] = React.useState("");
  const [data, setData] = React.useState([]);

  console.log("ejecutando effect");
  var db = firebase
    .database()
    .ref(`users/3aW7ird3qyP8AJJqNoqgCPJiwu03/productos`)
    .endAt(valueSearch.toString());

  db.once("value", snap => {
    console.log("entro...");
    if (snap.val()) {
      setData(Object.values(snap.val()));
      console.log("si");
    } else {
      setData([]);
      console.log("no");
    }
  });
  return { setValueSearch, data, valueSearch };
}

function IntegrationDownshift(props) {
  const [valueSearch, setValueSearch] = React.useState("");
  //const { setValueSearch, data, valueSearch } = useSearchProductos();
  const { error, loading, value } = useList(
    firebase
      .database()
      .ref(`users/3aW7ird3qyP8AJJqNoqgCPJiwu03/productos`)
      .endAt(valueSearch.toString())
  );
  return (
    <div style={{ flexDirection: "row", display: "flex", marginTop: "40px" }}>
      {/* data.toString() */}
    {/*   <p>
        {error && <strong>Error: {"error.toString()"}</strong>}
        {loading && <span>List: Loading...</span>}
        {!loading && value && value.toString()}
      </p> */}
      <Downshift id="downshift-popper">
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          highlightedIndex,
          inputValue,
          isOpen,
          selectedItem
        }) => (
          <div>
            {renderInput({
              fullWidth: true,
              InputProps: getInputProps({
                placeholder: "With Popper",
                onChange: e => {
                  console.log(e.target.value);
                  setValueSearch(e.target.value);
                }
              }),
              ref: node => {
                popperNode = node;
              }
            })}
            <Popper open={isOpen} anchorEl={popperNode}>
              <div {...(isOpen ? getMenuProps : {})}>
                <Paper
                  square
                  style={{
                    marginTop: 8,
                    width: popperNode ? popperNode.clientWidth : null
                  }}
                >
                  {getSuggestions(inputValue).map((suggestion, index) =>
                    renderSuggestion({
                      suggestion,
                      index,
                      itemProps: getItemProps({ item: suggestion.label }),
                      highlightedIndex,
                      selectedItem
                    })
                  )}
                </Paper>
              </div>
            </Popper>
          </div>
        )}
      </Downshift>
    </div>
  );
}

export default IntegrationDownshift;
