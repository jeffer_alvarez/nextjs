import { FormControl, InputLabel, NativeSelect } from "@material-ui/core";
import React, { useEffect, useState } from "react";

function TipoDocumento(props) {

  const [tipoVenta, setTipoVenta] = useState('consumidor_final')
  const handleChange = (event) => {
    setTipoVenta(event.target.value)
  }

  useEffect(() => {
    setTipoVenta(props.tipoVenta)
  })

  return (
    <FormControl style={{ height: 40, width: '100%', marginTop: -16 }}>
      <NativeSelect
        style={{ height: 40, color: "#95989A" }}
        value={tipoVenta}
        onChange={handleChange}
      >
        <option style={{ color: "#95989A" }} value='consumidor_final'>
          Consumidor Final
        </option>
        <option value='factura'>Factura</option>
      </NativeSelect>
    </FormControl>
  );
}

export default TipoDocumento;
