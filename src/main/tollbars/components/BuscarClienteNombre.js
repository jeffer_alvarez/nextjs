import "firebase/database";
import "firebase/auth";

import { Autocomplete, RadioGroup, SearchInput, TextInput, TextInputField } from "evergreen-ui";
import React, { Component, useEffect } from "react";

import Autosuggest from "react-autosuggest";
import Paper from "@material-ui/core/Paper";
import firebase from "firebase/app";

class BuscarClienteNombre extends Component {
  render() {


    return (
      <div>

        <Autocomplete
          id="id_nombre_cliente"
          display='none'
          title="Clientes"
          onChange={changedItem => this.props.cargarCliente(changedItem)}
          items={this.props.clientes}
          itemToString={(i => (i ? String(i.nombre) : ''))}
          style={{ background: "#456" }}
          height={45}
          children={props => {
            const { getInputProps, getRef, inputValue, } = props;

            return (
              <TextInputField
                placeholder="Nombre del cliente (F2)"
                label={<span style={{ color: '#333' }}>Nombre del cliente</span>}
                value={inputValue}
                display={props.visible}
                height={45}
                width='200px'
                onChange={(e) => {
                  //setValueSearch(changedItem.target.value);
                  console.log("skalslñ" + e);
                }}
                innerRef={getRef}
                {...getInputProps()}
              />
            );
          }}
        />
      </div>
    );
  }
};

export default BuscarClienteNombre;
