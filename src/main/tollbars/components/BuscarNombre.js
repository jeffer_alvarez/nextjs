import "firebase/database";
import "firebase/auth";

import {
  Autocomplete,
  RadioGroup,
  SearchInput,
  TextInput,
  TextInputField
} from "evergreen-ui";
import React, { useEffect } from "react";

import Autosuggest from "react-autosuggest";
import Paper from "@material-ui/core/Paper";
import firebase from "firebase/app";

const BuscarNombre = props => {

  return (
    <div>
    
      <Autocomplete
        id="id_nombre_producto"
        title="Productos"
        onChange={changedItem => props.cargarProducto(changedItem)}
        items={props.productos}
        itemToString={i => (i ? String(i.descripcion_producto) : "")}
        
      >
        {props => {
          const { getInputProps, getRef, inputValue } = props;
          return (
            <TextInputField
              id="nombre_pro"
              label={<span style={{ color: "white" }}>Nombre Producto</span>}
              color="white"
              placeholder="Nombre del producto (F4)"
              value={inputValue}
              onChange={(e)=>console.log(e.target.value)}
              width="450px"
              innerRef={getRef}
              style={{fontSize:15,borderRadius:0}}
              {...getInputProps()}
            />
          );
        }}
      </Autocomplete>
    </div>
  );
};

export default BuscarNombre;
