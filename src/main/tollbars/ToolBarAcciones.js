import { Button } from "evergreen-ui";
import React from "react";
import themes from "../../utils/themeCustom";

function ToolBarAcciones(props) {
  return (
    <div
      style={{
        height: "50px",
        background: themes.base,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'center',
        position: "fixed",
        bottom: 0,
        width: "100%"
      }}
    >
      {
        props.editar === false &&
        <Button
          id="id_factuar"
          height={40}
          marginRight={16}
          iconBefore="tick-circle"
          onClick={()=>props.abrirFactura()}
        >
          Facturar (F8)
      </Button>}
      <Button
        height={40}
        marginRight={16}
        iconBefore="cross"
        onClick={()=>props.cancelar()}
      >
        Cancelar       </Button>
    </div>
  );
}

export default ToolBarAcciones;
