import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import Typography from "@material-ui/core/Typography";
import themes from "../../utils/themeCustom";

function ToolbarPrimario(props) {
  return (
    <div
      style={{
        height: "50px",
        background: themes.base,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        position: "fixed",
        top: 0,
        width: "100%",
        paddingLeft: 8
      }}
    >
      <IconButton color="inherit" onClick={() => props.handleDrawerOpen()} aria-label="Open drawer" style={{
        display: props.open ? 'none' : 'block'
      }} >
        <MenuIcon fontSize="small" />
      </IconButton>
      <IconButton onClick={() => props.handleDrawerClose()} fontSize="small" style={{
        display: props.open ? 'block' : 'none'
      }}>
        <ChevronLeftIcon style={{ color: 'white' }} />
      </IconButton>
      <Typography variant="h6" color="inherit" noWrap style={{ paddingLeft: 8 }}>
        {props.title}
      </Typography>
    </div>
  );
}

export default ToolbarPrimario;
