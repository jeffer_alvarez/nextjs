import "firebase/database";
import "firebase/auth";

import {
  Button,
  Dialog,
  Pane,
  SearchInput,
  Spinner,
  toaster
} from "evergreen-ui";
import React, { Component, useEffect, useState } from "react";

import BuscarNombre from "./components/BuscarNombre";
import MedioPago from "../main/punto_venta/components_lista_content/MedioPago";
import TablaProducto1 from "../main/punto_venta/components_lista_content/TablaProductos1";
import { TextInputField } from "evergreen-ui/commonjs/text-input";
import ToolBarAcciones from "./ToolBarAcciones";
import ToolbarCliente from "./ToolbarCliente";
import firebase from "firebase/app";
import funtions from "../../utils/funtions";
import themes from "../../utils/themeCustom";
import Example from "../main/punto_venta/components_lista_content/CodigoReferencia";
import BuscarDescripcion from "../main/punto_venta/components_lista_content/BuscarDescripcion";

class ToolbarSelectItem extends Component {
  state = {
    clienteSeleccionado: "",
    observacion: "",
    dinero_resibido: "",
    sumaTotal: "",
    cambio: "",
    tipo_pago: "",
    listaProductos: [],
    facturar: false,
    punto_emision: "",
    codigo_establecimiento: "",
    ambiente: "",
    lista: [],
    listaProductosPrecio: [],
    producto: [],
    cliente: [],
    listaProductosVenta: [],
    codigo: "",
    limpiar: false,
    cantidadProductos: 1,
    visible: "none",
    descuento: 0.0,
    precio: 0,
    estadoMedioPago: false,
    nombreProducto: "",
    isShown: false,
    numeroDocumento: "",
    numeroFactura: "",
    cajaSeleccionada: [],
    usuarioSeleccionado: [],
    totalVentas: 0,
    sumaSubtotal: "",
    iva: "",
    eliminado: false,
    estadoCliente: true,
    totalVentasSinIva: 0,
    totalVentasconIva: 0,
    tipo_venta: "final",
    editar: false,
    venta: [],
    imprimir: false,
    tipoVenta: ""
  };

  detectar = () => {
    document.addEventListener("keydown", event => {
      const keyName = event.key;
      if (this.state.estadoMedioPago === false) {
        if (keyName === "F8") {
          event.preventDefault();
          setTimeout(() => {
            if (this.state.totalVentas > 0) {
              if (this.state.cliente.nombre) {
                this.setState({
                  tipoVenta: "factura"
                });
              } else {
                this.setState({
                  tipoVenta: "final"
                });
              }
              this.setState({
                isShown: true,
                lista: []
              });
              this.obtenerTotal();
              this.obtenerNumeroFactura();
            } else {
              toaster.notify('No existen productos para registrar la venta')
            }
          }, 300);
        }
      }

      if (event.key === "F11") {
        event.preventDefault();
      } else if (event.key === "F1") {
        event.preventDefault();
        document.getElementById("id_cedula").focus();
      } else if (keyName === "F9") {
        this.setState({
          facturar: true
        });
      } else if (event.key === "F2") {
        event.preventDefault();
        document
          .getElementById("TextInputField-id_nombre_cliente-input")
          .focus();
      } else if (event.key === "F3") {
        event.preventDefault();
        //
      } else if (event.key === "F4") {
        event.preventDefault();
        document
          .getElementById("TextInputField-id_nombre_producto-input")
          .focus();
      } else if (keyName === "F10") {
        event.preventDefault();
        this.setState({
          imprimir: true
        });
      } else if (keyName === "F5") {
        event.preventDefault();
      }
    });
  };

  componentDidMount() {
    this.detectar();
    this.setState({
      estadoMedioPago: false,
      isShown: false
    });
    var db = firebase.database();
    var uid = firebase.auth().currentUser.uid;
    var refPrecio = db.ref(`users/${uid}/precios_personalizados`);
    var refProductos = db
      .ref(`users/${uid}/productos`)
      .orderByChild("estado")
      .equalTo(true);
    refProductos.on("value", snap => {
      if (snap.val()) {
        var lista = Object.values(snap.val());

        this.setState({
          isShown: false,
          estadoMedioPago: false
        });
        this.setState({
          listaProductos: lista,
          estadoMedioPago: false,
          isShown: false
        });
      }
      this.props.cargarLista(lista);
    });
    refPrecio.on("value", snap => {
      if (snap.val()) {
        var listaPrecios = funtions.snapshotToArray(snap);
        this.setState({
          isShown: false,
          estadoMedioPago: false
        });
        this.setState({
          listaProductosPrecio: listaPrecios
        });
      }
    });
    this.obteberCajaSeleccionada();
    document.getElementById("id_cedula").focus();
  }

  componentWillReceiveProps(props) {
    // event.preventDefault()

    $("#id_cedula").focus();
    document.getElementById("id_cedula").focus();
  }

  obtenerNumeroFactura = () => {
    var db = firebase.database();
    var uid = firebase.auth().currentUser.uid;
    var refEstablecimiento = db.ref(`auth_admins/${uid}/establecimiento/`);
    var refAmbiente = db.ref(`auth_admins/${uid}`);
    var refNumeroFactura = db.ref(`users/${uid}/configuracion/`);
    refAmbiente.on("value", snap => {
      if (snap.val()) {
        this.setState({
          ambiente: snap.val().ambiente
        });
      }else{
        console.log("error")
      }
    });
    refEstablecimiento.on("value", snap => {
      if (snap.val()) {
        refNumeroFactura.on("value", snapchat => {
          if (snapchat.val()) {
            var numeroFact = snapchat.val().numero_factura;
            const suma = Number(numeroFact) + 1;
            const tamaño = String(suma).length;
            const restaTamaño = 9 - Number(tamaño);
            var cadenaFinal = "";
            for (var i = 0; i < restaTamaño; i++) {
              cadenaFinal = cadenaFinal + "0";
            }
            const sumaFinal = `${cadenaFinal}${suma}`;
            var codigo = snap.val().codigo;
            var puntoVenta = snap.val().punto_emision;
            var factur = `FA${codigo}-${puntoVenta}-${sumaFinal}`;
            var fecha = funtions.obtenerFechaActual();
            var hora = funtions.obtenerHoraActual();
            var date = fecha + "  " + hora;
            var totales = 0;
            this.state.listaProductosVenta.map(item => {
              totales += Number(item.total);
            });
            var datos = this.obtenerTotalesFactura();
            totales.toFixed(6);
            this.setState({
              numeroDocumento: factur,
              numeroFactura: sumaFinal,
              punto_emision: puntoVenta,
              codigo_establecimiento: codigo
            });
            var item = {
              documento: factur,
              vence: date,
              saldo: totales,
              item: datos
            };

            this.state.lista.splice(0, 1);
            this.state.lista.push(item);
            this.setState({
              lista: this.state.lista,
              isShown: false,
              estadoMedioPago: true
            });
          }
        });
      }else{
        console.log("error 2")
      }
    });
  };

  obteberCajaSeleccionada = () => {
    var db = firebase.database();
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        var db = firebase.database();
        var operacionVentaRefCaja = db.ref(
          "users/" +
            firebase.auth().currentUser.uid +
            "/caja/cajas_abiertas_usuario"
        );
        var ususarios = db.ref(
          "users/" + firebase.auth().currentUser.uid + "/usuarios"
        );
        var usuario = {};
        operacionVentaRefCaja.on("value", snap => {
          if (snap.val()) {
            ususarios.on("value", snapUsu => {
              if (snapUsu.val()) {
                var lista = funtions.snapshotToArray(snapUsu);
                lista.map(item => {
                  if (item.estado) {
                    usuario = item;
                  }
                });
                var caja = funtions.snapshotToArray(snap);
                if (caja != null) {
                  this.setState({
                    cajaSeleccionada: caja,
                    usuarioSeleccionado: usuario
                  });
                } else {
                  this.setState({
                    cajaSeleccionada: [],
                    usuarioSeleccionado: null
                  });
                }
              }
            });
          } else {
            this.setState({
              cajaSeleccionada: []
            });
          }
        });
      }
    });
  };

  cargarProducto = productoItem => {
    this.setState({
      codigo: productoItem.codigo_barras
    });
    if (this.state.producto.length > 0) {
      this.state.producto.splice(0, 1);
      this.state.producto.push(productoItem);
      this.setState({
        producto: this.state.producto,
        cantidadProductos: 1,
        descuento: 0
      });
      document.getElementById("TextInputField-id_cantidad").focus();
      document.getElementById("TextInputField-id_cantidad").select();
    } else {
      this.state.producto.push(productoItem);
      this.setState({
        producto: this.state.producto,
        cantidadProductos: 1,
        descuento: 0
      });
      document.getElementById("TextInputField-id_cantidad").focus();
      document.getElementById("TextInputField-id_cantidad").select();
    }
  };

  buscarProducto = value => {
    this.setState({
      limpiar: false
    });

    if (value.length === 13 || value.length === 12 || value.length === 9) {
      this.setState({
        visible: "block",
        cantidadProductos: 1
      });
      var producto = this.state.listaProductos.filter(
        item => item.codigo_barras === value
      );
      if (producto.length > 0) {
        if (this.state.listaProductosVenta.length === 0) {
          var productoItem = {};
          var db = firebase.database();
          var listaPreciosEscoger = [];
          var referenciaPrecio = db.ref(
            `users/${firebase.auth().currentUser.uid}/precios_personalizados/${
              producto[0].codigo
            }`
          );
          referenciaPrecio.once("value", snap => {
            if (snap.val()) {
              listaPreciosEscoger = funtions.snapshotToArray(snap);
              var precioVen = listaPreciosEscoger.filter(
                item => item.nombre === "Precio A"
              )[0];

              var precioItem = precioVen.nuevo_precio;
              var porcentaje = precioVen.porcentaje;
              var precioPublico = producto[0].tiene_iva
                ? Number(precioItem / 1.12).toFixed(6)
                : Number(precioItem).toFixed(6);
              if (Number(producto[0].stock_actual) < 1) {
                this.setState({
                  visible: "block"
                });
                toaster.danger(
                  <p>
                    No existe el estock actual
                    <br />
                    Stock actual {producto[0].stock_actual}
                  </p>
                );
              } else {
                this.state.listaProductos.map(item => {
                  if (item.codigo === producto[0].codigo) {
                    item.stock_actual =
                      Number(item.stock_actual) -
                      Number(this.state.cantidadProductos);
                  }
                });
                this.setState({
                  listaProductos: this.state.listaProductos
                });

                productoItem = {
                  cantidad: 1,
                  codigo: producto[0].codigo,
                  codigo_barras: producto[0].codigo_barras,
                  descripcion_producto: producto[0].descripcion_producto,
                  porcentaje_iva: producto[0].porcentaje_iva,
                  precio_costo: producto[0].precio_costo,
                  precio_venta: Number(precioItem).toFixed(6),
                  precio_venta_a: producto[0].precio_venta_b,
                  stock_actual: Number(producto[0].stock_actual) - 1,
                  tiene_iva: producto[0].tiene_iva,
                  precios: listaPreciosEscoger,
                  precio_publico: producto[0].tiene_iva
                    ? Number(precioItem / 1.12).toFixed(6)
                    : Number(precioItem).toFixed(6),
                  descuento: this.state.descuento,
                  porcentaje: porcentaje,
                  tipo_precio_seleccionado:
                    producto[0].tipo_precio_seleccionado,
                  subtotal:
                    Number(this.state.cantidadProductos) *
                    Number(precioPublico),
                  total: Number(
                    Number(this.state.cantidadProductos) * Number(precioItem)
                  ).toFixed(6)
                };
                this.state.listaProductosVenta.push(productoItem);
                setTimeout(() => {
                  this.setState({
                    listaProductosVenta: this.state.listaProductosVenta,
                    codigo: "",
                    cantidadProductos: 1,
                    producto: [],
                    visible: "none"
                  });
                  this.obtenerTotal();
                }, 100);
              }
            } else {
              toaster.danger(<p>No existe precios para el producto</p>);
            }
          });
        
        } else {
          var encontrado = false;
          var nuevoCodigo = "";
          var productoEncontrado = this.state.listaProductosVenta.filter(
            item => item.codigo === producto[0].codigo
          );
          if (productoEncontrado.length > 0) {
            if (Number(producto[0].stock_actual) < 1) {
              toaster.danger(
                <p>
                  No existe el estock actual
                  <br />
                  Stock actual {producto[0].stock_actual}
                </p>
              );
            } else {
              this.state.listaProductos.map(item => {
                if (item.codigo === producto[0].codigo) {
                  item.stock_actual =
                    Number(item.stock_actual) -
                    Number(this.state.cantidadProductos);
                }
              });
              this.setState({
                listaProductos: this.state.listaProductos
              });
              this.state.listaProductosVenta.map(item => {
                if (item.codigo === productoEncontrado[0].codigo) {
                  item.cantidad = Number(item.cantidad) + 1;
                  item.subtotal = item.precio_publico * item.cantidad;
                  item.total = Number(
                    item.precio_venta * item.cantidad
                  ).toFixed(6);
                  (item.stock_actual = Number(producto[0].stock_actual) - 1),
                    setTimeout(() => {
                      this.setState({
                        listaProductosVenta: this.state.listaProductosVenta,
                        codigo: "",
                        cantidadProductos: 1,
                        producto: [],
                        visible: "none"
                      });
                      this.obtenerTotal();
                    }, 100);
                }
              });
            }
          } else {
            var productoItem = {};

            var db = firebase.database();
            var listaPreciosEscoger = [];
            var referenciaPrecio = db.ref(
              `users/${
                firebase.auth().currentUser.uid
              }/precios_personalizados/${producto[0].codigo}`
            );
            referenciaPrecio.once("value", snap => {
              if (snap.val()) {
                listaPreciosEscoger = funtions.snapshotToArray(snap);
                var precioVen = listaPreciosEscoger.filter(
                  item => item.nombre === "Precio A"
                )[0];
                var precioItem = precioVen.nuevo_precio;
                var porcentaje = precioVen.porcentaje;
                var precioPublico = producto[0].tiene_iva
                  ? Number(precioItem / 1.12).toFixed(6)
                  : Number(precioItem).toFixed(6);
                if (Number(producto[0].stock_actual) < 1) {
                  toaster.danger(
                    <p>
                      No existe el estock actual
                      <br />
                      Stock actual {producto[0].stock_actual}
                    </p>
                  );
                } else {
                  this.state.listaProductos.map(item => {
                    if (item.codigo === producto[0].codigo) {
                      item.stock_actual =
                        Number(item.stock_actual) -
                        Number(this.state.cantidadProductos);
                    }
                  });
                  productoItem = {
                    cantidad: 1,
                    codigo: producto[0].codigo,
                    codigo_barras: producto[0].codigo_barras,
                    descripcion_producto: producto[0].descripcion_producto,
                    porcentaje_iva: producto[0].porcentaje_iva,
                    precio_costo: producto[0].precio_costo,
                    precio_venta: Number(precioItem).toFixed(6),
                    precio_venta_a: producto[0].precio_venta_b,
                    precios: listaPreciosEscoger,
                    stock_actual: producto[0].stock_actual,
                    tiene_iva: producto[0].tiene_iva,
                    precio_publico: producto[0].tiene_iva
                      ? Number(precioItem / 1.12).toFixed(6)
                      : Number(precioItem).toFixed(6),
                    porcentaje: porcentaje,
                    tipo_precio_seleccionado:
                      producto[0].tipo_precio_seleccionado,
                    descuento: this.state.descuento,
                    subtotal:
                      Number(this.state.cantidadProductos) *
                      Number(precioPublico),
                    total: Number(
                      Number(this.state.cantidadProductos) * Number(precioItem)
                    ).toFixed(6)
                  };
                  this.state.listaProductosVenta.push(productoItem);
                  setTimeout(() => {
                    this.setState({
                      listaProductosVenta: this.state.listaProductosVenta,
                      codigo: "",
                      cantidadProductos: 1,
                      producto: [],
                      visible: "none"
                    });
                    this.obtenerTotal();
                  }, 100);
                }
              } else {
                toaster.danger(<p>No existe precios para el producto</p>);
              }
            });
          }
        }
      } else {
        toaster.warning("No existe producto con ese codigo");
        this.setState({
          codigo: "",
          visible: "none"
        });
        //toaster.error("No existe producto con ese codigo");
      }
    } else if (value.length === 0) {
    } else {
      var nombre = this.state.listaProductos.filter(
        item => item.codigo_referencia === value
      );
      if (nombre.length === 1) {
        this.setState({
          visible: "block",
          producto: nombre,
          nombreProducto: nombre[0].descripcion_producto
        });
      }
    }
  };

  agregarProducto = () => {
    if (this.state.producto.length > 0) {
      if (this.state.listaProductosVenta.length === 0) {
        var productoItem = {};
        var db = firebase.database();
        var listaPreciosEscoger = [];
        var referenciaPrecio = db.ref(
          `users/${firebase.auth().currentUser.uid}/precios_personalizados/${
            this.state.producto[0].codigo
          }`
        );
        referenciaPrecio.once("value", snap => {
          if (snap.val()) {
            listaPreciosEscoger = funtions.snapshotToArray(snap);
            var precioVen = listaPreciosEscoger.filter(
              item => item.nombre === "Precio A"
            )[0];
            var precioItem = precioVen.nuevo_precio;
            var porcentaje = precioVen.porcentaje;
            var precioPublico = this.state.producto[0].tiene_iva
              ? Number(precioItem / 1.12).toFixed(6)
              : Number(precioItem).toFixed(6);
            if (
              Number(this.state.producto[0].stock_actual) <
              Number(this.state.cantidadProductos)
            ) {
              this.setState({
                nombreProducto: this.state.producto[0].descripcion_producto
              });
              toaster.danger(
                <p>
                  No existe el estock actual
                  <br />
                  Stock actual {this.state.producto[0].stock_actual}
                </p>
              );
            } else {
              this.state.listaProductos.map(item => {
                if (item.codigo === this.state.producto[0].codigo) {
                  item.stock_actual =
                    Number(item.stock_actual) -
                    Number(this.state.cantidadProductos);
                }
              });
              this.setState({
                listaProductos: this.state.listaProductos
              });
              productoItem = {
                cantidad: this.state.cantidadProductos,
                codigo: this.state.producto[0].codigo,
                codigo_barras: this.state.producto[0].codigo_barras,
                descripcion_producto: this.state.producto[0]
                  .descripcion_producto,
                porcentaje_iva: this.state.producto[0].porcentaje_iva,
                precio_costo: this.state.producto[0].precio_costo,
                precio_venta: Number(precioItem).toFixed(6),
                precios: listaPreciosEscoger,
                precio_venta_a: this.state.producto[0].precio_venta_b,
                stock_actual:
                  Number(this.state.producto[0].stock_actual) -
                  Number(this.state.cantidadProductos),
                tiene_iva: this.state.producto[0].tiene_iva,
                precio_publico: this.state.producto[0].tiene_iva
                  ? Number(precioItem / 1.12).toFixed(6)
                  : Number(precioItem).toFixed(6),
                descuento: this.state.descuento,
                porcentaje: porcentaje,
                tipo_precio_seleccionado: this.state.producto[0]
                  .tipo_precio_seleccionado,
                subtotal: Number(
                  Number(this.state.cantidadProductos) * Number(precioPublico)
                ).toFixed(6),
                total: Number(
                  Number(this.state.cantidadProductos) * Number(precioItem)
                ).toFixed(6)
              };

              this.state.listaProductosVenta.push(productoItem);
              setTimeout(() => {
                this.setState({
                  listaProductosVenta: this.state.listaProductosVenta,
                  codigo: "",
                  limpiar: true,
                  cantidadProductos: 1,
                  producto: [],
                  visible: "block"
                });
                this.obtenerTotal();

                setTimeout(() => {
                  this.setState({
                    visible: "none",
                    limpiar: true
                  });
                }, 10);
              }, 100);
            }
          } else {
            toaster.danger(<p>No existe precios para el producto</p>);
          }
        });
      } else {
        var encontrado = false;
        var nuevoCodigo = "";
        var productoEncontrado = this.state.listaProductosVenta.filter(
          item => item.codigo === this.state.producto[0].codigo
        );

        if (productoEncontrado.length > 0) {
          if (
            Number(this.state.producto[0].stock_actual) <
            Number(this.state.cantidadProductos)
          ) {
            toaster.danger(
              <p>
                No existe el estock actual
                <br />
                Stock actual {this.state.producto[0].stock_actual}
              </p>
            );
          } else {
            this.state.listaProductos.map(item => {
              if (item.codigo === this.state.producto[0].codigo) {
                item.stock_actual =
                  Number(item.stock_actual) -
                  Number(this.state.cantidadProductos);
              }
            });
            this.setState({
              listaProductos: this.state.listaProductos
            });
            this.state.listaProductosVenta.map(item => {
              if (item.codigo === productoEncontrado[0].codigo) {
                item.cantidad =
                  Number(item.cantidad) + Number(this.state.cantidadProductos);
                item.subtotal = item.precio_publico * item.cantidad;
                item.total = Number(item.precio_venta * item.cantidad).toFixed(
                  6
                );
                setTimeout(() => {
                  this.setState({
                    listaProductosVenta: this.state.listaProductosVenta,
                    codigo: "",
                    limpiar: true,
                    cantidadProductos: 1,
                    producto: [],
                    visible: "block"
                  });

                  this.obtenerTotal();

                  setTimeout(() => {
                    this.setState({
                      visible: "none"
                    });
                  }, 10);
                }, 100);
              }
            });
          }
        } else {
          var productoItem = {};
          var db = firebase.database();
          var listaPreciosEscoger = [];
          var referenciaPrecio = db.ref(
            `users/${firebase.auth().currentUser.uid}/precios_personalizados/${
              this.state.producto[0].codigo
            }`
          );
          referenciaPrecio.once("value", snap => {
            if (snap.val()) {
              listaPreciosEscoger = funtions.snapshotToArray(snap);
              var precioVen = listaPreciosEscoger.filter(
                item => item.nombre === "Precio A"
              )[0];
              var precioItem = precioVen.nuevo_precio;
              var porcentaje = precioVen.porcentaje;
              var precioPublico = this.state.producto[0].tiene_iva
                ? Number(precioItem / 1.12).toFixed(6)
                : Number(precioItem).toFixed(6);
              if (
                Number(this.state.producto[0].stock_actual) <
                Number(this.state.cantidadProductos)
              ) {
                toaster.danger(
                  <p>
                    No existe el estock actual
                    <br />
                    Stock actual {this.state.producto[0].stock_actual}
                  </p>
                );
              } else {
                this.state.listaProductos.map(item => {
                  if (item.codigo === this.state.producto[0].codigo) {
                    item.stock_actual =
                      Number(item.stock_actual) -
                      Number(this.state.cantidadProductos);
                  }
                });
                productoItem = {
                  cantidad: this.state.cantidadProductos,
                  codigo: this.state.producto[0].codigo,
                  codigo_barras: this.state.producto[0].codigo_barras,
                  descripcion_producto: this.state.producto[0]
                    .descripcion_producto,
                  porcentaje_iva: this.state.producto[0].porcentaje_iva,
                  precio_costo: this.state.producto[0].precio_costo,
                  precio_venta: Number(precioItem).toFixed(6),
                  precio_venta_a: this.state.producto[0].precio_venta_b,
                  precios: listaPreciosEscoger,
                  stock_actual: this.state.producto[0].stock_actual,
                  tiene_iva: this.state.producto[0].tiene_iva,
                  precio_publico: this.state.producto[0].tiene_iva
                    ? Number(precioItem / 1.12).toFixed(6)
                    : Number(precioItem).toFixed(6),
                  descuento: this.state.descuento,
                  porcentaje: porcentaje,
                  tipo_precio_seleccionado: this.state.producto[0]
                    .tipo_precio_seleccionado,
                  subtotal: Number(
                    Number(this.state.cantidadProductos) * Number(precioPublico)
                  ).toFixed(6),
                  total: Number(
                    Number(this.state.cantidadProductos) * Number(precioItem)
                  ).toFixed(6)
                };
                this.state.listaProductosVenta.push(productoItem);
                setTimeout(() => {
                  this.setState({
                    listaProductosVenta: this.state.listaProductosVenta,
                    codigo: "",
                    cantidadProductos: 1,
                    producto: [],
                    visible: "block",
                    limpiar: true
                  });

                  this.obtenerTotal();

                  setTimeout(() => {
                    this.setState({
                      visible: "none"
                    });
                  }, 10);
                }, 100);
              }
            } else {
              toaster.danger(<p>No existe precios para el producto</p>);
            }
          });
        }
      }
    } else {
      toaster.danger("No existe prodcuto con ese codigo");
    }
  };

  obtenerTotal = () => {
    var totales = 0;
    var iva = 0;
    var totalIva = 0;
    var sinIva = 0;
    var subTotal = 0;
    var subtotalSinIva = 0;
    var subtotalConIva = 0;
    this.state.listaProductosVenta.map(item => {
      if (item.tiene_iva) {
        sinIva = Number(Number(item.total) / 1.12).toFixed(6);
        iva = Number(Number(item.total) - Number(sinIva)).toFixed(6);
        totalIva += Number(iva);
        subtotalConIva = Number(
          Number(subtotalConIva) + Number(item.total) / 1.12
        ).toFixed(6);
        subTotal = Number(Number(subTotal) + Number(item.total) / 1.12).toFixed(
          6
        );
        totales += Number(item.total);
      } else {
        subtotalSinIva = Number(
          Number(subtotalSinIva) + Number(item.total)
        ).toFixed(6);
        subTotal = Number(subTotal) + Number(item.total);
        totales += Number(item.total);
      }
    });
    totales.toFixed(6);
    this.setState({
      totalVentasconIva: subtotalConIva,
      totalVentasSinIva: subtotalSinIva,
      totalVentas: totales,
      sumaSubtotal: subTotal,
      iva: totalIva
    });
    /*  setTimeout(() => {
       this.props.totales(totales, totalIva, subTotal, true);
     }, 200); */
  };

  obtenerTotalesFactura = () => {
    var totales = 0;
    var iva = 0;
    var totalIva = 0;
    var sinIva = 0;
    var subTotal = 0;
    this.state.listaProductosVenta.map(item => {
      if (item.tiene_iva) {
        sinIva = Number(Number(item.total) / 1.12).toFixed(6);
        iva = Number(Number(item.total) - Number(sinIva)).toFixed(6);
        totalIva += Number(iva);
        subTotal = Number(Number(subTotal) + Number(item.total) / 1.12).toFixed(
          6
        );
        totales += Number(item.total);
      } else {
        subTotal = Number(subTotal) + Number(item.total);
        totales += Number(item.total);
      }
    });
    totales.toFixed(6);
    this.setState({
      totalVenta: totales
    });
    // setTotalVenta(totales);
    var totalesFactura = {
      total: Number(totales).toFixed(6),
      sumaSubtotal: Number(subTotal).toFixed(6),
      totalIva: Number(totalIva).toFixed(6)
    };
    return totalesFactura;
  };

  eliminarItem = i => {
    this.state.listaProductosVenta.splice(i, 1);
    this.setState({
      listaProductosVenta: this.state.listaProductosVenta
    });
    this.obtenerTotal();
  };

  abrirModal = () => {
    if (estadoMedioPago) {
    } else {
      setEstadoMedioPago(true);
    }
  };

  limpiarDatos = () => {
    this.setState({
      facturar: false,
      editar: false,
      producto: [],
      cliente: [],
      listaProductosVenta: [],
      cantidadProductos: 1,
      visible: "none",
      descuento: "",
      nombreProducto: "",
      numeroDocumento: "",
      numeroFactura: "",
      totalVentasSinIva: 0,
      totalVentasconIva: 0,
      tipo_venta: "final",
      estadoMedioPago: false,
      eliminado: true,
      isShown: false,
      listaProductosVenta: [],
      totalVentas: 0,
      sumaSubtotal: 0,
      iva: 0,
      cliente: [],
      estadoCliente: false,
      venta: []
    });
    setTimeout(() => {
      this.setState({
        estadoCliente: true
      });
    }, 10);
    /*  listaProductosVenta.length = 0;
     setListaProductosVenta(listaProductosVenta);
     this.props.totales(0, 0, 0, false);
     setEstadoMedioPago(false); */
  };

  editarVenta = venta => {
    this.setState({
      editar: true,
      listaProductosVenta: venta.productos,
      cliente: venta.cliente,
      venta: venta
    });
    setTimeout(() => {
      this.obtenerTotal();
    }, 100);
  };

  guardarVentaEditada = () => {
    this.handleFianlizarVenta();
  };

  handleFianlizarVenta = () => {
    var codigoRegistroVenta = this.state.venta.codigo;
    var userUid = firebase.auth().currentUser.uid;
    var item = "efectivo";

    // this.updateDataProductos()
    //this.sumarNumeroFactura()

    this.setSaveRegistroVentaEfectivo(codigoRegistroVenta, item);
    //this.enviarFacturaElectronica(codigoRegistroVenta, userUid, item)
  };

  updateDataProductos = () => {
    this.state.listaProductosVenta.forEach(item => {
      var db = firebase.database();
      var productosRef = db.ref(
        "users/" + firebase.auth().currentUser.uid + "/productos/" + item.codigo
      );
      productosRef.once("value", snap => {
        if (snap.val()) {
          productosRef.update({
            stock_actual:
              Number(snap.val().stock_actual) - Number(item.cantidad)
          });
        }
      });
    });
  };

  updateAgregarDataProductos = lista => {
    lista.forEach(item => {
      var db = firebase.database();
      var productosRef = db.ref(
        "users/" + firebase.auth().currentUser.uid + "/productos/" + item.codigo
      );
      productosRef.once("value", snap => {
        if (snap.val()) {
          productosRef.update({
            stock_actual:
              Number(snap.val().stock_actual) + Number(item.cantidad)
          });
        }
      });
    });
  };

  setSaveRegistroVentaEfectivo = (codigoVenta, itemFacturas) => {
    var db = firebase.database();
    var operacionVentaRef = db.ref(
      "users/" +
        firebase.auth().currentUser.uid +
        "/ventas/" +
        codigoVenta +
        "/productos"
    );
    operacionVentaRef.remove();
    setTimeout(() => {
      operacionVentaRef.update(this.state.listaProductosVenta);
    }, 100);

    var tipo_pago = itemFacturas;
    this.setVentaCaja(itemVenta, tipo_pago);
    operacionVentaRef.set(itemVenta);
  };

  createJsonFacturaElectronicaEfectivo = () => {
    var date = new Date();
    var json = {
      ambiente: this.props.ambiente,
      tipo_emision: 1,
      secuencial: Number(this.props.numeroFactura),
      fecha_emision: date.toISOString(),
      emisor: {
        ruc: "",
        obligado_contabilidad: false,
        contribuyente_especial: "",
        nombre_comercial: "",
        razon_social: "",
        direccion: "",
        establecimiento: {
          punto_emision: String(this.props.puntoEmision),
          codigo: String(this.props.codigoEstablecimiento),
          direccion: ""
        }
      },
      moneda: "USD",
      totales: {
        total_sin_impuestos: Number(Number(this.props.sumaSubtotal).toFixed(3)),
        impuestos: [
          {
            base_imponible: Number(this.props.sumaSinIva),
            valor: 0.0,
            codigo: "2",
            codigo_porcentaje: "0"
          },
          {
            base_imponible: Number(Number(this.props.sumaConIva).toFixed(3)),
            valor: Number(this.props.iva),
            codigo: "2",
            codigo_porcentaje: "2"
          }
        ],
        importe_total: Number(Number(this.props.total).toFixed(3)),
        propina: 0.0,
        descuento: Number(Number(this.props.descuento).toFixed(3))
      },
      comprador: {
        email: this.props.cliente.email,
        identificacion: this.props.cliente.numero_identificacion,
        tipo_identificacion: this.props.cliente.tipo_identificacion,
        razon_social: this.props.cliente.nombre,
        direccion: this.props.cliente.direccion,
        telefono: this.props.cliente.celular
      },
      items: this.props.listaVentas.map(item => {
        return {
          cantidad: Number(item.cantidad),
          codigo_principal:
            item.codigo_barras.length > 0 ? item.codigo_barras : "0",
          codigo_auxiliar: item.codigo,
          precio_unitario: Number(
            Number(
              (
                Number(item.precio_costo) * Number(item.porcentaje) +
                Number(item.precio_costo)
              ).toFixed(3) / 1.12
            ).toFixed(3)
          ),
          descripcion: Boolean(item.tiene_iva)
            ? "* " + item.descripcion_producto
            : item.descripcion_producto,
          precio_total_sin_impuestos: Number(
            Number(
              (
                (Number(item.precio_costo) * Number(Number(item.porcentaje)) +
                  Number(item.precio_costo)) *
                Number(item.cantidad)
              ).toFixed(3) / 1.12
            ).toFixed(3)
          ),
          impuestos: [
            {
              base_imponible: Number(
                Number(
                  (
                    (Number(item.precio_costo) *
                      Number(Number(item.porcentaje)) +
                      Number(item.precio_costo)) *
                    Number(item.cantidad)
                  ).toFixed(3) / 1.12
                ).toFixed(3)
              ),
              valor: Boolean(item.tiene_iva)
                ? Number(
                    (
                      (((Number(item.precio_costo) *
                        Number(Number(item.porcentaje)) +
                        Number(item.precio_costo)) /
                        1.12) *
                        Number(item.porcentaje_iva)) /
                      100
                    ).toFixed(3)
                  )
                : 0,
              tarifa: Boolean(item.tiene_iva) ? Number(item.porcentaje_iva) : 0,
              codigo: "2",
              codigo_porcentaje: Boolean(item.tiene_iva) ? "2" : "0"
            }
          ],
          descuento: 0.0
        };
      }),
      valor_retenido_iva: 0.0,
      valor_retenido_renta: 0.0,
      pagos: [
        {
          medio: "efectivo",
          total: Number(this.props.total)
        }
      ]
    };
    return json;
  };

  saveFacturasJson = (jsonData, codigoRegistroVenta) => {
    var db = firebase.database();
    var operacionFacturaJson = db.ref(
      "users/" +
        firebase.auth().currentUser.uid +
        "/facturas_ventas/" +
        codigoRegistroVenta
    );
    operacionFacturaJson.set(jsonData);
  };

  postSetGeneratePdf = async (uidUser, jsonData, codigoRegistroVenta) => {
    const rawResponse = await fetch(
      "https://stormy-bayou-19844.herokuapp.com/facturaPdf",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          id: uidUser,
          codigo: codigoRegistroVenta
        },
        body: JSON.stringify(jsonData)
      }
    );
  };

  enviarFacturaElectronica = (codigoRegistroVenta, userUid, item) => {
    event.preventDefault();
    if (this.props.tipoVenta === "factura") {
      var jsonData = {};
      switch (item) {
        case "efectivo": {
          jsonData = this.createJsonFacturaElectronicaEfectivo();
          this.saveFacturasJson(jsonData, codigoRegistroVenta);

          this.postSetGeneratePdf(userUid, jsonData, codigoRegistroVenta);
          setTimeout(() => {
            this.props.cerrar();
          }, 500);
          toaster.notify("Venta guardada");
          break;
        }
        /*   case 'credito': {
              jsonData = this.createJsonFacturaElectronicaCredito(item)
              this.saveFacturasJson(jsonData, codigoRegistroVenta)
              this.sumarNumeroFactura()
              this.enviarFacturaElectrónica(facturaElectronica, uidUser, jsonData, codigoRegistroVenta)
              this.setState({ abrirModalFinalizarVenta: false })
              break
          }
          case 'tarjeta-credito': {
              jsonData = this.createJsonFacturaElectronicaTarjetaCredito(item)
              this.saveFacturasJson(jsonData, codigoRegistroVenta)
              this.sumarNumeroFactura()
              this.enviarFacturaElectrónica(facturaElectronica, uidUser, jsonData, codigoRegistroVenta)
              this.setState({ abrirModalFinalizarVenta: false })
              break
          }
          case 'tarjeta-debito': {
              jsonData = this.createJsonFacturaElectronicaTarjetaCredito(item)
              this.saveFacturasJson(jsonData, codigoRegistroVenta)
              this.sumarNumeroFactura()
              this.enviarFacturaElectrónica(facturaElectronica, uidUser, jsonData, codigoRegistroVenta)
              this.setState({ abrirModalFinalizarVenta: false })
              break
          }
          case 'cheque': {
              jsonData = this.createJsonFacturaElectronicaTarjetaCredito(item)
              this.postSetGeneratePdf(uidUser, jsonData, codigoRegistroVenta)
              this.saveFacturasJson(jsonData, codigoRegistroVenta)
              this.sumarNumeroFactura()
              this.enviarFacturaElectrónica(facturaElectronica, uidUser, jsonData, codigoRegistroVenta)
              this.setState({ abrirModalFinalizarVenta: false })
              break
          }
          case 'transferencia': {
              jsonData = this.createJsonFacturaElectronicaTransferencia()
              this.postSetGeneratePdf(uidUser, jsonData, codigoRegistroVenta)
              this.saveFacturasJson(jsonData, codigoRegistroVenta)
              this.sumarNumeroFactura()
              this.enviarFacturaElectrónica(facturaElectronica, uidUser, jsonData, codigoRegistroVenta)
              this.setState({ abrirModalFinalizarVenta: false })
              break
          }
          default: {
              break
          } */
      }
    } else {
      setTimeout(() => {
        this.props.cerrar();
      }, 500);
      toaster.notify("Venta guardada");
    }
  };

  cambiarPrecios = (codigo, precio) => {
    var lisPrecio = this.state.listaProductosPrecio.filter(
      item => item.id === codigo
    )[0];
    if (lisPrecio.length) {
    } else {
      lisPrecio = Object.values(lisPrecio);
    }
    lisPrecio.splice(4, 1);
    var precio = lisPrecio.filter(item => item.nombre === precio)[0];
    this.state.listaProductosVenta.map(item => {
      if (item.codigo === codigo) {
        item.precio_venta = Number(precio.nuevo_precio).toFixed(6);
        item.precio_publico = Number(Number(item.precio_venta) / 1.12).toFixed(
          6
        );
        item.subtotal = Number(item.precio_publico).toFixed(6);
        item.total = Number(item.precio_venta) * Number(item.cantidad);
      }
    });
    this.setState({
      listaProductosVenta: this.state.listaProductosVenta
    });
    setTimeout(() => {
      this.obtenerTotal();
    }, 100);
  };

  abrirFactura = () => {
    event.preventDefault();
    setTimeout(() => {
      if (this.state.totalVentas > 0) {
        if (this.state.cliente.nombre) {
          this.setState({
            tipoVenta: "factura"
          });
        } else {
          this.setState({
            tipoVenta: "final"
          });
        }
        this.setState({
          isShown: true,
          lista: []
        });
        this.obtenerTotal();
        this.obtenerNumeroFactura();
      } else {
        console.log(this.state.totalVentas)
        //toaster.notify('No existen productos para registrar la venta')
      }
    }, 300);
  };

  onChange = value => {};

  onBlur = () => {};

  onFocus = () => {};

  onSearch = val => {};

  render() {
    return (
      <div id="id_ventas">
        <>
          <ToolbarCliente
            subtotal={this.state.sumaSubtotal}
            iva={this.state.iva}
            totales={this.state.totalVentas}
            estado={this.state.estadoCliente}
            cargarCliente={cliente => {
              this.setState({ cliente: cliente, tipoVenta: "factura" });
            }}
            cliente={this.state.cliente}
            editar={this.state.editar}
          />

          {this.state.listaProductos.length > 0 &&
          this.state.listaProductosPrecio.length > 0 ? (
            <>
              <div
                style={{
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "center",
                  display: "flex",
                  backgroundColor: themes.base,
                  height: 65
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column"
                  }}
                >
                  <label
                    style={{
                      color: "white",
                      fontWeight: 500,
                      fontSize: "14px",
                      margin: 0,
                      padding: 0,
                      paddingBottom: 3
                    }}
                  >
                    Codigo producto
                  </label>
                  <Example
                    data={this.state.listaProductos}
                    borrar={this.state.limpiar}
                    cantidad={() => {
                      document
                        .getElementById("TextInputField-id_cantidad")
                        .focus();
                      document
                        .getElementById("TextInputField-id_cantidad")
                        .select();
                    }}
                    buscar={value => this.buscarProducto(value)}
                  />
                  {/* <SearchInput
                      id="codigo_producto"
                      label="Default text input field"
                      placeholder="Buscar por Código(F3)"
                      value={this.state.codigo}
                      onKeyPress={(e) => {
                        if (e.charCode === 13) {
                          console.log('paso')
                        }
                      }}
                      onChange={e => {
                        if (e.target.value.length === 0) {
                          this.setState({
                            cantidad: 1,
                            descuento: 0,
                            visible: "block"
                          });
                          setTimeout(() => {
                            this.setState({
                              visible: "none"
                            });
                          }, 100)
                        } else if (e.target.value.length === 13) {
                          this.setState({ codigo: e.target.value });
                          setTimeout(() => {
                            this.buscarProducto()
                          }, 100)
                        }
                        this.setState({ codigo: e.target.value, visible: 'block' });
                      }}
                      style={{
                        borderRadius: "0px",
                        fontSize: "15px"
                      }}
                      width={"100%"}
                    /> */}
                </div>

                {/*   <IntegrationDownshift /> */}
                {this.state.visible != "none" ? (
                  <></>
                ) : (
                  <>
                    {/*  <BuscarDescripcion
                        data={this.state.listaProductos}

                      /> */}
                    <BuscarNombre
                      productos={this.state.listaProductos}
                      cargarProducto={producto => this.cargarProducto(producto)}
                      style={{
                        height: "35px",
                        borderRadius: "0px",
                        fontSize: "15px",
                        width: "100%"
                      }}
                      width="50%"
                    />
                  </>
                )}
                <TextInputField
                  onClick={() => {
                    this.setState({
                      visible: "none",
                      codigo: ""
                    });
                  }}
                  id="id_nombre"
                  placeholder="Nombre del producto"
                  value={this.state.nombreProducto}
                  label={
                    <span style={{ color: "white" }}>Nombre Producto (F4)</span>
                  }
                  style={{
                    borderRadius: "0px",
                    fontSize: "15px"
                  }}
                  display={this.state.visible}
                  width={"450px"}
                />

                <TextInputField
                  id="id_cantidad"
                  value={this.state.cantidadProductos}
                  onChange={e => {
                    if (isNaN(e.target.value)) {
                      toaster.warning("Ingrese un valor numerico");
                    } else {
                      this.setState({ cantidadProductos: e.target.value });
                    }
                  }}
                  onKeyPress={e => {
                    if (e.charCode === 13) {
                      this.agregarProducto();
                    }
                  }}
                  label={<span style={{ color: "white" }}>Cantidad</span>}
                  placeholder="Cantidad"
                  style={{
                    borderRadius: "0px",
                    fontSize: "15px",
                    textAlign: "right"
                  }}
                  width={"10%"}
                />
                {/*      <TextInputField
                        value={this.state.precio}
                        label={<span style={{ color: "white" }}>Precio Unt.</span>}
                        placeholder="Desc %"
                        style={{
                          height: "32px",
                          borderRadius: "0px",
                          fontSize: "15px",
                          textAlign: "right"
                        }}
                        width={"10%"}
                      /> */}
              </div>
              {this.state.listaProductosVenta.length > 0 && (
                <TablaProducto1
                  lista={this.state.listaProductosVenta}
                  eliminarItem={i => this.eliminarItem(i)}
                  cambiarPrecios={(codigo, precio) =>
                    this.cambiarPrecios(codigo, precio)
                  }
                />
              )}
            </>
          ) : (
            <Pane
              display="flex"
              flexDirection="column"
              alignItems="center"
              fontFamily="cursive"
              justifyContent="center"
              height={400}
            >
              <Spinner size={24} />
              Cargando datos para empezar a trabajar ...
            </Pane>
          )}

          <Dialog
            isShown={this.state.isShown}
            onCloseComplete={() => this.setState({ isShown: false })}
            hasFooter={false}
            hasHeader={false}
          >
            <Pane
              display="flex"
              flexDirection="column"
              alignItems="center"
              fontFamily="cursive"
              justifyContent="center"
              height={150}
            >
              <Spinner size={24} />
              Cargando datos de la factura ...
            </Pane>
          </Dialog>
        </>

        {this.state.estadoMedioPago === true && (
          <MedioPago
            imprimir={this.state.imprimir}
            facturar={this.state.facturar}
            tipoVenta={this.state.tipoVenta}
            caja={this.state.cajaSeleccionada}
            usuario={this.state.usuarioSeleccionado}
            lista={this.state.lista}
            total={this.state.totalVenta}
            numeroFactura={this.state.numeroFactura}
            isShown={this.state.estadoMedioPago}
            listaVentas={this.state.listaProductosVenta}
            cliente={this.state.cliente}
            cerrar={() => this.limpiarDatos()}
            codigoEstablecimiento={this.state.codigo_establecimiento}
            puntoEmision={this.state.punto_emision}
            iva={this.state.iva}
            sumaSubtotal={this.state.sumaSubtotal}
            sumaSinIva={this.state.totalVentasSinIva}
            sumaConIva={this.state.totalVentasconIva}
            ambiente={this.state.ambiente}
            descuento={0}
            firebaseClientesConection={this.props.firebaseClientesConection}
            volverEditar={() => {
              this.setState({
                estadoMedioPago: false
                //cliente: this.state.cliente
              });
            }}
            cancelar={() => {
              event.preventDefault();
              this.setState({
                facturar: false,
                imprimir: false
              });
            }}
          />
        )}
        <ToolBarAcciones
          editar={this.state.editar}
          cancelar={() => this.limpiarDatos()}
          abrirFactura={() => this.abrirFactura()}
        />
      </div>
    );
  }
}

export default ToolbarSelectItem;
