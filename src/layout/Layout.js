import "firebase/database";
import "firebase/auth";
import "firebase/firebase-firestore";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import Head from "next/head";
import Login from "../login/Login";
import Main from "../main/Main";
import React from "react";
import firebase from "firebase/app";
import functions from "../utils/funtions";
import theme from "../utils/theme";

//firebase connect database

//components Login

//Material Ui Theme

// Initialize Firebase
if (!firebase.apps.length) {
  var config = {
    apiKey: "AIzaSyDRr4xJB6BxM5Uu-xHEe7lo_6hVZ_hC8DU",
    authDomain: "facbtaapps-2bd69.firebaseapp.com",
    databaseURL: "https://facbtaapps-2bd69.firebaseio.com",
    projectId: "facbtaapps-2bd69",
    storageBucket: "facbtaapps-2bd69.appspot.com",
    messagingSenderId: "182776326473"
  };

  var firebaseConfigClientes = {
    apiKey: "AIzaSyB9Iq4Opykyv2-1Pj_n1lM4hR5Wo7wAbro",
    authDomain: "clientesdata-fe443.firebaseapp.com",
    databaseURL: "https://clientesdata-fe443.firebaseio.com",
    projectId: "clientesdata-fe443",
    storageBucket: "clientesdata-fe443.appspot.com",
    messagingSenderId: "323486223214",
    appId: "1:323486223214:web:c140b5e10e3b84a9"
  };

  
  
  
  
  const firebaseApp = firebase.initializeApp(config);
}


const themeCont = createMuiTheme({ ...theme });

function Layout(props) {
  const [sesionEstado, setSesionEstado] = React.useState("cerrada");
  const [title, setTitle] = React.useState("Inicio");
  const [firebaseClientesConection, setFirebaseClientesConection] = React.useState('')

  React.useEffect(() => {  
    setTimeout(() => {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          setTimeout(() => {
            setSesionEstado("iniciada");
          }, 300);
        } else {
          setTimeout(() => {
            setSesionEstado("cerrada");
          }, 300);
        }
      });
    }, 300);
    const firebaseClientes = firebase.initializeApp(firebaseConfigClientes, "joder_2");
    setFirebaseClientesConection(firebaseClientes.firestore())
  }, []);

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <html lang="es" />
        <link href="https://fonts.googleapis.com/css?family=Lexend+Exa&display=swap" rel="stylesheet" />
        <link
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous"
        />
        <script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/JavaScript" src="/static/print.js"></script>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
        />
        <meta http-equiv='cache-control' content='no-cache' />
        <meta http-equiv='expires' content='0' />
        <meta http-equiv='pragma' content='no-cache'></meta>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
      </Head>

      <MuiThemeProvider theme={themeCont}>
        <div id="rootSnackBar" />

        {sesionEstado === "iniciada" && <Main firebaseClientesConection={firebaseClientesConection} />}

        {sesionEstado === "cerrada" && <Login />}
      </MuiThemeProvider>
    </div>
  );
}

export default Layout;
