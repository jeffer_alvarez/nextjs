var xml=`<?xml version="1.0" encoding="UTF-8"?>
    <factura id="comprobante" version="2.1.0">
        <infoTributaria>
            <ambiente>1</ambiente>
            <tipoEmision>1</tipoEmision>
            <razonSocial>Papeleria y Bazar Yeslita</razonSocial>
            <nombreComercial>Ligia Machuca</nombreComercial>
            <ruc>1900597871001</ruc>     
            <claveAcceso>2806201901190059787100110010020000011251234567814</claveAcceso>     
            <codDoc>01</codDoc>
            <estab>001</estab>
            <ptoEmi>002</ptoEmi>
            <secuencial>000001125</secuencial>
            <dirMatriz>24 de mayo y Antonio María Isasi</dirMatriz>
        </infoTributaria>
        <infoFactura>
            <fechaEmision>28/06/2019</fechaEmision>
            <dirEstablecimiento>24 de mayo y Antonio María Isasi</dirEstablecimiento>
            <contribuyenteEspecial>12345</contribuyenteEspecial>
            <obligadoContabilidad>SI</obligadoContabilidad>       
            <tipoIdentificacionComprador>07</tipoIdentificacionComprador>      
            <razonSocialComprador>Consumidor Final</razonSocialComprador>
            <identificacionComprador>9999999999999</identificacionComprador>            
            <totalSinImpuestos>50.00</totalSinImpuestos>       
            <totalDescuento>0.00</totalDescuento>       
            <totalConImpuestos>
                <totalImpuesto>
                    <codigo>2</codigo>
                    <codigoPorcentaje>0</codigoPorcentaje>
                    <descuentoAdicional>0.00</descuentoAdicional>
                    <baseImponible>50.00</baseImponible>
                    <tarifa>49.50</tarifa>
                    <valor>50.00</valor>
                    <valorDevolucionIva>50.00</valorDevolucionIva>
                </totalImpuesto>
                <totalImpuesto>
                    <codigo>2</codigo>
                    <codigoPorcentaje>0</codigoPorcentaje>
                    <descuentoAdicional>0.00</descuentoAdicional>
                    <baseImponible>50.00</baseImponible>
                    <tarifa>49.50</tarifa>
                    <valor>50.00</valor>
                    <valorDevolucionIva>50.00</valorDevolucionIva>
                </totalImpuesto>
            </totalConImpuestos>     
            <propina>50.00</propina>      
            <importeTotal>50.00</importeTotal>
            <moneda>moneda0</moneda>
            <pagos>
                <pago>
                    <formaPago>01</formaPago>
                    <total>50.00</total>
                    <plazo>50.00</plazo>
                    <unidadTiempo>unidadTiem</unidadTiempo>
                </pago>         
            </pagos>
            <valorRetIva>50.00</valorRetIva>
            <valorRetRenta>50.00</valorRetRenta>
        </infoFactura>
        <detalles>
            <detalle>
                <codigoPrincipal>codigoPrincipal0</codigoPrincipal>
                <codigoAuxiliar>codigoAuxiliar0</codigoAuxiliar>
                <descripcion>descripcion0</descripcion>
                <unidadMedida>unidadMedida0</unidadMedida>
                <cantidad>50.000000</cantidad>
                <precioUnitario>50.000000</precioUnitario>
                <precioSinSubsidio>50.000000</precioSinSubsidio>
                <descuento>50.00</descuento>
                <precioTotalSinImpuesto>50.00</precioTotalSinImpuesto>
                <detallesAdicionales>
                    <detAdicional nombre="nombre0" valor="valor0"/>
                    <detAdicional nombre="nombre1" valor="valor1"/>
                </detallesAdicionales>
                <impuestos>
                    <impuesto>
                        <codigo>2</codigo>
                        <codigoPorcentaje>0</codigoPorcentaje>
                        <tarifa>49.50</tarifa>
                        <baseImponible>50.00</baseImponible>
                        <valor>50.00</valor>
                    </impuesto>
                    <impuesto>
                        <codigo>2</codigo>
                        <codigoPorcentaje>0</codigoPorcentaje>
                        <tarifa>49.50</tarifa>
                        <baseImponible>50.00</baseImponible>
                        <valor>50.00</valor>
                    </impuesto>
                </impuestos>
            </detalle>
            <detalle>
                <codigoPrincipal>codigoPrincipal1</codigoPrincipal>
                <codigoAuxiliar>codigoAuxiliar1</codigoAuxiliar>
                <descripcion>descripcion1</descripcion>
                <unidadMedida>unidadMedida1</unidadMedida>
                <cantidad>50.000000</cantidad>
                <precioUnitario>50.000000</precioUnitario>
                <precioSinSubsidio>50.000000</precioSinSubsidio>
                <descuento>50.00</descuento>
                <precioTotalSinImpuesto>50.00</precioTotalSinImpuesto>
                <detallesAdicionales>
                    <detAdicional nombre="nombre2" valor="valor2"/>
                    <detAdicional nombre="nombre3" valor="valor3"/>
                </detallesAdicionales>
                <impuestos>
                    <impuesto>
                        <codigo>2</codigo>
                        <codigoPorcentaje>0</codigoPorcentaje>
                        <tarifa>49.50</tarifa>
                        <baseImponible>50.00</baseImponible>
                        <valor>50.00</valor>
                    </impuesto>
                    <impuesto>
                        <codigo>2</codigo>
                        <codigoPorcentaje>0</codigoPorcentaje>
                        <tarifa>49.50</tarifa>
                        <baseImponible>50.00</baseImponible>
                        <valor>50.00</valor>
                    </impuesto>
                </impuestos>
            </detalle>
        </detalles>   
</factura>`
    
export default xml