import React, { Component } from 'react';

import funtions from './funtions';

function generateBarcode() {
    $("#barcode").barcode(
        "12345670", // Valor del codigo de barras
        "ean8" // tipo (cadena)
    );

}
class ResivoVenta extends Component {

    componentDidMount() {
        //libreria barcode
        //  JsBarcode("#barcode", "1007201901190050489300110010020000019214599540114");
    }

    state = {
        fecha: funtions.obtenerFechaActualResivo(),
        hora: funtions.obtenerHoraActual()
    }


    render() {
        const {
            item,
        } = this.props
     
        if (item != null) {


            return (
                <div style={{ width: 250, height: 'max-content', fontFamily: "Lucida Sans Typewriter", }}>
                    <div style={{ width: '100%', fontFamily: "Lucida Console", fontSize: 25, marginTop: 16, display: 'flex', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>
                        SERVIFAC
                    </div>
                  
                    <div style={{ textAlign: 'center', marginTop: 8,fontSize:15 }}>
                    {item.razon_social}
                        </div>
                        <div style={{ textAlign: 'left', fontSize: 15, marginTop: 8,fontFamily:'Sans Serif' }}>                           
                            <span>Dirección: {item.direccion}</span>
                        </div>
                    <div style={{ width: '100%', fontFamily: "Lucida Sans Typewriter", fontSize: 9, marginTop: 16 }}>
                        <div>

                        </div>
                        <div style={{ textAlign: 'center', fontSize: 15 }}>
                            N°  COMPROBANTE: {item.numero_venta}
                        </div>
                        <div style={{ textAlign: 'center', marginTop: 8, fontSize: 15 }}>
                            COMPRAS REALIZADAS EN EFECTIVO
                        </div>
                        <div style={{ textAlign: 'right', fontSize: 10, marginTop: 14 }}>
                            <span>Fecha: {this.state.fecha}</span>
                            <span style={{ paddingLeft: 15 }}>Hora: {this.state.hora}</span>
                        </div>
                    </div>

                    <div style={{ fontFamily: "Lucida Sans Typewriter", borderBottom: '.5px solid rgba(0,0,0,0.3)', borderTop: '.5px solid rgba(0,0,0,0.3)', paddingLeft: 5, paddingTop: 3, paddingBottom: 3, marginTop: 8.5, display: 'flex', flexDirection: 'row', fontSize: 9}}>
                        <div style={{ width: 25,fontFamily:"Lucida Sans Typewriter",fontSize: 9 }}>Cant</div>
                        <div style={{ width: 125,fontFamily:"Lucida Sans Typewriter",fontSize: 9 }}>Descripción</div>
                        <div style={{}}></div>
                        <div style={{ width: 50 ,fontFamily:"Lucida Sans Typewriter",fontSize: 9}}>P. Unt.</div>
                        <div style={{ width: 50 ,fontFamily:"Lucida Sans Typewriter",fontSize: 9}}>Valor</div>
                    </div>
                    <div>
                        {
                            item.productos.map(item =>

                                <div key={item.codigo} style={{ fontFamily: "Lucida Sans Typewriter", paddingLeft: 5, paddingRight: 5, paddingTop: 5, display: 'flex', flexDirection: 'row', fontSize: 8 }}>
                                    <div style={{ width: 25,fontFamily:"Lucida Sans Typewriter",fontSize: 10 }}>{item.cantidad}</div>
                                    <div style={{ width: 125,fontFamily:"Lucida Sans Typewriter",fontSize: 10 }}>{item.tiene_iva ? "*" + item.descripcion_producto.toUpperCase() : item.descripcion_producto}</div>
                                    <div style={{}}></div>
                                    <div style={{ width: 50,fontFamily:"Lucida Sans Typewriter",fontSize: 10 }}>{`$ ${item.tiene_iva ? ((Number(item.precio_venta) / 1.12) * Number(item.cantidad)).toFixed(3) : (Number(item.precio_venta) * Number(item.cantidad)).toFixed(3)}`}</div>
                                    <div style={{ width: 50,fontFamily:"Lucida Sans Typewriter",fontSize: 10 }}>{`$ ${item.tiene_iva ?
                                        ((Number(item.precio_venta) / 1.12) * Number(item.cantidad)).toFixed(2)
                                        :
                                        (Number(item.precio_venta) * Number(item.cantidad)).toFixed(2)}`}</div>
                                </div>
                            )
                        }
                    </div>
                    <div style={{ borderBottom: '.5px solid rgba(0,0,0,0.3)', marginTop: 5, marginBottom: 5 }}></div>
                    <div style={{ marginBottom: 16 }}>
                        <div style={{ fontFamily: "Lucida Sans Typewriter", paddingLeft: 10, paddingRight: 8, display: 'flex', flexDirection: 'row', fontSize: 6, textAlign: 'right' }}>
                            <div style={{ flex: 0.5 }}></div>
                            <div style={{}}>
                                <div style={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>SUBTOTAL SIN IMPUESTOS:</div>
                                <div style={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>SUBTOTAL IVA 12%:</div>
                                <div style={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>VALOR IVA 12%:</div>
                                <div style={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>DESCUENTO:</div>
                                <div style={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>VALOR TOTAL:</div>
                            </div>
                            <div style={{ flex: 0.3 }}></div>
                            <div style={{ fontFamily: "Lucida Sans Typewriter", fontSize: 10, textAlign: 'right' }}>
                                <div tyle={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>{`$ ${Number((Number(item.subtotal) + Number(item.iva))).toFixed(2)}`}</div>
                                <div tyle={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>{`$ ${Number(item.subtotal).toFixed(2)}`}</div>
                                <div tyle={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>{`$ ${Number(item.iva).toFixed(2)}`}</div>
                                <div tyle={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>{`$ ${Number(item.descuento).toFixed(2)}`}</div>
                                <div tyle={{fontFamily:"Lucida Sans Typewriter",fontSize: 10}}>{`$ ${Number(item.total).toFixed(2)}`}</div>
                            </div>
                        </div>
                    </div>

                    {
                        item.tipo_venta === 'factura' ?
                            <div style={{ fontFamily: "Lucida Sans Typewriter", fontSize: 10, display: 'flex', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginRight: 10 }}>Revisa tu factura <br />electrónica en tu correo</div>
                            :
                            <div
                                style={{ fontFamily: "Lucida Console", fontSize: 11, display: 'flex', paddingLeft: 15, flexDirection: 'column' }}>
                                <span>Cliente: CONSUMIDOR FINAL</span>
                                <span>RUC:     9999999999999</span>

                            </div>
                    }
                    {
                        item.tipo_venta === 'factura' &&
                        <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lucida Sans Typewriter", marginLeft: 15, fontSize: 10 }}>
                            <div style={{ fontSize: 11, display: 'flex' }}>{`Nombre: ${item.nombreCliente}`}</div>
                            <div style={{ fontSize: 11, display: 'flex' }}>{`Correo: ${item.emailCliente}`}</div>
                            <div style={{ fontSize: 11, display: 'flex' }}>{`Cédula: ${item.identificacionCliente}`}</div>
                            <div style={{ fontSize: 11, display: 'flex' }}>{`Dirección: ${item.direccionCliente}`}</div>
                        </div>
                    }
                    {
                        item.tipo_pago === 'efectivo' ?
                            <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lucida Sans Typewriter", marginLeft: 15, fontSize: 10 }}>
                                {/*  <div style={{ fontSize: 8, display: 'flex' }}>{`Tipo Pago: ${item.tipo_pago}`}</div> */}
                            </div>
                            : item.tipo_pago === 'credito' ?
                                <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lucida Sans Typewriter", marginLeft: 15, fontSize: 8 }}>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Tipo Pago: A Credito`}</div>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Valor Acreditado: $ ${item.valor_acreditado}`}</div>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Saldo Pendiente: $ ${item.total - item.valor_acreditado}`}</div>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Fecha a Pagar: ${item.fecha_a_pagar}`}</div>

                                </div>
                                :
                                <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 10, fontFamily: "Lucida Sans Typewriter", marginLeft: 15, fontSize: 8 }}>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Tipo Pago:  ${item.tipo_pago}`}</div>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Numero: ${item.numero_tarjeta}`}</div>
                                    <div style={{ fontSize: 11, display: 'flex' }}>{`Nombre Banco: ${item.nombre_banco}`}</div>
                                </div>
                    }
                    <div style={{ fontFamily: "Lucida Console", fontSize: 11, marginTop: 8 }}>
                        <div style={{ borderBottom: '.5px solid rgba(0,0,0,0.3)', borderTop: '.5px solid rgba(0,0,0,0.3)', textAlign: 'center', padding: 8 }}>FACTURADO POR SERVIFAC - GLOBALTECH</div>

                    </div>


                    {/* Barcode
                     <svg id="barcode" style={{ width: 200 }}></svg> */}

                </div>
            );
        } else {
            return (
                <div>ndad</div>
            )
        }
    }
}

export default ResivoVenta;