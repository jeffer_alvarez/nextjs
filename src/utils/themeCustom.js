const themes = {
  lightest: "#F9F9FB",
  light: "#E4E7EB",
  base: "#425A70",
  dark: "#234361"
};

export default themes;
