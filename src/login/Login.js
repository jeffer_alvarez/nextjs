import "firebase/auth";

import React, { useEffect } from "react";

import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from "@material-ui/core/FormControl";
import { Grid } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import firebase from "firebase/app";
import setSnackBars from "../utils/setSnackBars";

// firebase

function Login(props) {
  const [password, setPassword] = React.useState("");
  const [usuario, setUsuario] = React.useState("");
  const [statePassword, setStatePassword] = React.useState("normal");
  const [stateUsuario, setStateUsuario] = React.useState("normal");
  const [showPassword, setShowPassword] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [timeSlepp, setTimeSleep] = React.useState(false);

  useEffect(() => {
    
    setTimeout(() => {
      setTimeSleep(true);
    }, 300);
  });

  const handleChangePassword = event => {
    if (event.target.value.length === 0) {
      setStatePassword("error");
    } else {
      setStatePassword("normal");
    }
    setPassword(event.target.value);
  };

  const handleChangeUsuario = event => {
    if (event.target.value.length === 0) {
      setStateUsuario("error");
    } else {
      setStateUsuario("normal");
    }
    setUsuario(event.target.value);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleSetInitSession = () => {      
    if (usuario.length === 0) {
      setStateUsuario("error");
    }
    if (password.length === 0) {
      setStatePassword("error");
    }
    if (usuario.length > 0 && password.length > 0) {
      setSuccess(false);
      setLoading(true);
      firebase
        .auth()
        .signInWithEmailAndPassword(usuario, password)
        .then(() => {
       
        })
        .catch(error => {
         
          var errorCode = error.code;
       
          if (errorCode === "auth/invalid-email") {
            setSnackBars.openSnack(
              "error",
              "rootSnackBar",
              "Formato de correo electrónico erroneo",
              2000
            );
          }

          if (errorCode === "auth/user-not-found") {
            setSnackBars.openSnack(
              "error",
              "rootSnackBar",
              "Usuario no registrado",
              2000
            );
          }

          if (errorCode === "auth/wrong-password") {
            setSnackBars.openSnack(
              "error",
              "rootSnackBar",
              "Contraseña erronea",
              2000
            );
          }

          setLoading(false);
          setSuccess(true);
        });
    }
  };

  const _onKeyPress = event => {
    if (event.charCode === 13) {
      // enter key pressed
      event.preventDefault();
      handleSetInitSession();
    }
  };

  return (
    <>
      {timeSlepp === true ? (
        <Grid container>
          <Grid item xs={12} sm={6} style={{ height: "100vh", backgroundImage: "url(/static/fondo_inicio_secion.png)" }}>
            <div style={{ padding: '10% 10%', display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "column" }}>
              <div id="rootSnackBar" />
              <img style={{ width: 300 }} src="/static/logo-bienvenida-administracion.png" />
              <Typography variant="headline" component="h1" style={{
                textAlign: 'center',
                color: 'gray',
                fontSize: 18,
                marginBottom: 20,
                marginTop: 18
              }}>
                Punto de venta
                </Typography>
              <form
                noValidate
                autoComplete="off"
                style={{
                  display: "flex",
                  flexDirection: "column"
                }}
              >
                <TextField
                  error={stateUsuario === "normal" ? false : true}
                  id="user"
                  type="text"
                  value={usuario}
                  onChange={handleChangeUsuario}
                  label="Correo Electronico"
                  margin="normal"
                  onFocus={() => {
                    //usuario.length === 0 &&
                    //setStateUsuario('error')
                  }}
                  style={{
                    marginTop: 10,
                    marginBottom: 20
                  }}
                />
                <FormControl
                  style={{
                    marginBottom: 20
                  }}
                >
                  <InputLabel
                    error={statePassword === "normal" ? false : true}
                    htmlFor="adornment-password"
                  >
                    Contraseña
                </InputLabel>
                  <Input
                    id="adornment-password"
                    label="Nombre de usuario"
                    error={statePassword === "normal" ? false : true}
                    type={showPassword ? "text" : "password"}
                    value={password}
                    onChange={handleChangePassword}
                    onFocus={() => {
                      password.length === 0 && setStatePassword("error");
                    }}
                    onKeyPress={_onKeyPress}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={handleClickShowPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </form>

              <div
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  position: "relative"
                }}
              >
                <Button
                  variant="contained"
                  color="primary"
                  style={{
                    marginTop: 20,
                    marginBottom: 10,
                    width: "100%"
                  }}
                  disabled={
                    stateUsuario === "normal" && statePassword === "normal"
                      ? loading
                      : true
                  }
                  onClick={handleSetInitSession}
                >
                  Iniciar sesión
              </Button>
                {loading && (
                  <CircularProgress
                    size={24}
                    style={{
                      color: "primary",
                      position: "absolute",
                      top: "50%",
                      left: "50%",
                      marginTop: -8,
                      marginLeft: -12
                    }}
                  />
                )}
              </div>
            </div>
          </Grid>
          <Grid item xs={12} sm={6} style={{ padding: "10% 10%", backgroundImage: "url(/static/fondo_inicio_secion_2.png)", height: "100vh" }}>
            <Grid item xs={12} sm={12}>
              <Typography variant="headline" component="h1" style={{ color: "white" }} >
                Más Aplicaciones
                            </Typography>
            </Grid>
            <Grid item xs={6} sm={6}
              onClick={() => { window.open('https://servifac.com', '_blank'); }}
              style={{ cursor: "pointer", display: "flex", alignItems: "center", flexDirection: "column",marginTop:75 }}
            >
              <img src="/static/logo-punto-de-venta.png" style={{ width: 120 }} />
              <Typography variant="headline" component="h1" style={{
                textAlign: 'center',
                fontSize: 18,
                marginBottom: 20,
                marginTop: 18,
                color: "white"
              }}>
                Panel de administración
              </Typography>
            </Grid>
            <Grid item xs={6} sm={6} 
              onClick={() => { window.open('https://servifac.com', '_blank'); }}
            //style={{ cursor: "pointer", display: "flex", alignItems: "center", flexDirection: "column" }}
            >


            </Grid>
          </Grid>
        </Grid>
      ) : (
          <></>
        )}
    </>
  );
}

export default Login;
